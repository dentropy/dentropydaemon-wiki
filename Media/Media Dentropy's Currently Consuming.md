---
uuid: 78aa36ca-c4c6-40ed-873c-24099d5c2481
share: false
---
## Fiction Book

* Dune Prequels
* [[We by Yevgeny Zamyatin]] 
* [[The Light of Other Days]]

## Non Fiction Book

* Darwin and the Machines
* Human Forever
* The Eggs Benedict Option

## TV Show

* [[Foundation]]
* [[Silo]]
* [[Severence]]
* [[For All Mankind]]

## Anime

* [[The Eminence in Shadow]]

## Links

* [[Media Consumption Backlog]]