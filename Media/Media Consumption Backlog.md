---
share: true
uuid: 3d6300be-801b-4df3-93ad-ac452f22831d
---
* Media Consumption
	* [[Sorcerer's stone a beginner's guide to alchemy]]
	* [[Tower of God]]
	* [[Persona 4]]
	* [[Human Forever]]
	* [[The Rediscovery of Man]]
	* [[We by Yevgeny Zamyatin]] 
	* [[The Light of Other Days]]
	* [[An Ancient Magus Bride]]
	* [[My Hero Academia]] Season 6
	* [[Plunderer]]
	* [[Animatrix]]
	* [[Person of Interest]]
	* [[Travelers]]