---
share: true
uuid: d0a4afe3-56f4-454c-a116-cbc90fcd356f
---
[[Website]]

## [[Who]]
* [Nicole van der Hoeven](https://nicolevanderhoeven.com/)

## [[What are their interests]]

* Tech
* Knowledge Management
* Creativity, Producing Content, Writing
* TTRPG's
* Courses
* Conferences
* Media
	* Music
	* Podcasts
	* Books
	* Papers
	* Show (Black Mirror)
* Readwise data