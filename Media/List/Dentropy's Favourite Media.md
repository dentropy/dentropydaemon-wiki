---
uuid: cf6a4db5-dcac-48ae-97ec-cf40f28e2b20
share: false
---
## Non Fiction Books

* [[Homo Deus]]
* [[The Singularity Is Near]]
* [[Human Forever]]
* [[dentropydaemon-wiki/Media/List/12 Rules For Life|12 Rules For Life]]

## Fiction Books

* [[Altered Carbon]]
* [[The Culture Series]]
* [[Hyperion]]
* [[Daemon by Daniel Suarez]]

## [[Movies]]

* [[Blade Runner]]
* [[Fight Club]]

## [[TV Shows]]

* [[Person of Interest]]
* [[Westworld]]

## [[Video Games]]

* [[Factorio]]
* [[Satisfactory]]
* [[Starcraft 2]]

## [[Anime]]

* [[Ghost in The Shell]]
* [[Neon Genesis Evangelion]]
* [[Madoka Magica]]
* [[Kino's Journey]]
* [[Psycho Pass]]
* [[Violet Evergarden]]
* [[Re Creators]]
* [[Ergo Proxy]]