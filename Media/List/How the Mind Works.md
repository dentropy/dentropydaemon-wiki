---
share: true
uuid: a984fc49-2539-456b-962b-e801c3379983
---
---
id: VH6gKAf3bR6PUAwVX7xWp
title: How the Mind Works
desc: ''
updated: 1640223365934
created: 1640223328714
---

[[Steven Pinker|Relationships.People.Steven Pinker]]

## Sources

* [How the Mind Works by Steven Pinker](https://www.goodreads.com/book/show/835623.How_the_Mind_Works)
