---
share: true
uuid: 072dd632-729b-4e18-9a48-5df256881d68
---
---
id: hc3bnNkRU1bag6J5S0UIU
title: 'Daniel Dennett: How Does the Brain Store Beliefs?'
desc: ''
updated: 1634420888490
created: 1634419529207
---

* You can't have an isolated belief
* Belief's come in systems, they  in large clumps
* [[holism|wiki.concepts.list.holism]]

## Similar Media

* [[1984|wiki.media.list.1984]]
  * In 1984 Big Brother instills a system of beliefs that is not entirely coherent
* [[Inception|wiki.media.list.inception]]
  * [[Leonardo Dicaprio|Relationships.People.Leonardo Dicaprio]] manipulate's his wife's beliefs  leading to him getting framed for her murder.
  * When you mess with someone's belief's you are fucking with a highly connected system. One's system of belief's has a low [[Average path length|wiki.concepts.list.Average path length]] connecting the separate belief's just like how society is all connected through the [[Six degrees of separation|wiki.concepts.list.Six degrees of separation]].
* [[CBC Casper|wiki.concepts.list.CBC Casper]]
  * Sorta functions as a belief system. The idea of slashing stake and state from a network is like removing a belief.
