---
share: true
uuid: 56c7fe87-4454-4f4b-8f0d-809c4224ad30
---
---
id: akusxff8ru907plyt1ltxor
title: Daniel Dennett - What is the Mind-Body Problem? - YouTube
desc: ''
updated: 1639791962353
created: 1639791908491
---

## [Daniel Dennett - What is the Mind-Body Problem? - YouTube](https://www.youtube.com/watch?v=zDUVCcknlJY)

Only human beings have minds animals are just fancy clock work. Humans are mostly fancy clockwork. There is a conspicuous link to an immaterial mind that can not be explained mechanically. The capacity to have an intelligent conversation is the only thing that can not be explained mechanically.

How can we unify our every day introspective first person sense of ourselves with the world science says is there. There is a neutral way to do this by passing it through the third person [[Sieve|wiki.products.Sieve]].

* [[Heterophenomenology|wiki.concepts.list.Heterophenomenology]]
* [[Autophenomenology|wiki.concepts.list.Autophenomenology]]
* [[Dualisim|wiki.concepts.list.Dualisim]]
* [[René Descartes|Relationships.People.René Descartes]]
* [[Daniel Dennett|Relationships.People.Daniel Dennett]]
* [[Digital Skin|MyProjects.Digital Skin]]
