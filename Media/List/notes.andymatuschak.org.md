---
share: true
uuid: 5bdbf3bd-a18b-4f04-830e-6d33a82b5c4b
---
[[Website]]

## [[What are their interests]]?

* knowledge management
* [[zettelkasten]]
* 

## #Phrase 
“Better note-taking” misses the point; what matters is “better thinking”

