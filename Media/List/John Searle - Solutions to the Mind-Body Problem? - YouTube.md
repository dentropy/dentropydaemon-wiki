---
share: true
uuid: db92339b-8421-42fd-8e03-f230deaa0d95
---
---
id: 23g97ahiwsi6o7dn3ohytcv
title: John Searle - Solutions to the Mind-Body Problem? - YouTube
desc: ''
updated: 1639793157601
created: 1639791840096
---

## [John Searle - Solutions to the Mind-Body Problem? - YouTube](https://www.youtube.com/watch?v=IgWbExnceHE)

* [[John Searle|Relationships.People.John Searle]]
* [[Dualisim|wiki.concepts.list.Dualisim]]
  * We live in the ream of the mental and the realm of the physical
  * Substance Dualisim, we are separate entities
  * Property dualisim, there are a set of properties that have to do with the brain
* How does the mind effect the brain and how does the brain effect the mind
* [[Materialisim|wiki.concepts.list.Materialisim]]
* [[Knowledge argument|wiki.concepts.list.Knowledge argument]]
* [[What is it like to be a bat|wiki.concepts.list.What is it like to be a bat]]
* [[Zombie Argument|wiki.concepts.list.Zombie Argument]]
* [[Qualia|wiki.concepts.list.Qualia]]
* [[Chinese Room|wiki.concepts.list.Chinese Room]]
* We know conscious states are real
* What does it mean to show the difference between a reality and an illusion?
* All conscious states are caused by brain processes, [[René Descartes|Relationships.People.René Descartes]] did not know that
* Conscious states are features of the brain
