---
share: true
uuid: 84eb6ae3-694c-4739-a0d9-563a1367e5d0
---
[[Website]]
[One year of TILs](https://simonwillison.net/2021/May/2/one-year-of-tils/)

* Tech from this list, [Simon Willison: all TILs](https://til.simonwillison.net/all)
	* Machine Learning
	* Python and PIP
	* GIS in SQLite
	* Bash Scripting
	* Docker
	* HTML