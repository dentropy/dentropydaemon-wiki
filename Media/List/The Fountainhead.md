---
uuid: b19daa7e-c32d-42ab-8cbb-dca0dd7848e8
share: false
---
=======
---
share: true
---
[[books]] 
By [[Ayn Rand]]
>>>>>>> fcccbfe16e1a29d5f8ce9471854d2daf9792c59d
Book Quotes
-----------

*   But neither politics nor ethics nor philosophy is an end in itself, neither in life nor in literature. Only Man is an end in himself.
*   God as the beneficiary of man's self-immolation.
*   The man-worshipers, in my sense of the term, are those who see man's highest potential and strive to actualize it.
*   he made people feel as if they did not exist.
*   if one suffers in this world, it's on account of error.
*   Some people enjoy seeing others break
*   It's not a question of who will let you but who will stop you
*   No two materials are alike. No two sites on earth are alike. No two buildings have the same purpose. The purpose, the site, the material determine the shape.
    *   Similar to how we structure information technology.
*   I intend to have clients in order to build

Ch2
---

*   An equal among equals
*   Peters eyes glow for everyone
*   Peter forgets his youthful ambition forever
*   Peter cares how people look at him
*   Peter wants Howard's opinion over the dean's
*   Don't you know what you want?
*   How can you let others decide for you
*   Peter has self doubt
*   He wonder if he liked his mother
*   Howard like a cat?

Ch 3.
-----

*   Peter wonders why he saw himself as a potential architecture
*   Choose the builder of a home as diligently as you choose the bride to inhabit it
*   You look an insufferable egotist
*   it's not what you do that matters really. It's only you.
    *   Said by Peter's girlfriend
*   Men hate passion, any great passion. Henry Cameron made a mistake : he loved his work. That was why he fought. That was why he lost.
*   The sounds you make are not a language in that room any longer
*   why had he always feared that mysterious entity of consciousness within others?
*   People were his protection against people. Roark had no sense of people. Others gave Keating a feeling of his own value. Roark gave him nothing. He thought that he should seize his drawings and run. The danger was not Roark. The danger was that he, Keating, remained.

Ch 9
----

*   Less than he wanted but more than he could expected
    *   Roark's freedom to design but not see his designs erected
*   "We have always stood," said the Wynand editorials, "for the rights of the common man against the yellow sharks of privilege, but we cannot give our support to the destruction of law and order."
*   Everyone else is so unfinished, broken up into so many different pieces that don't fit together.
*   Your life does not belong to you if you are aiming really high
    *   People fuck based on power relationships
*   Wayne Wilmot; there was only a shell containing the opinions of her friends, the picture postcards she had seen, the novels of country squires she had read; it was this that he had to address, this immateriality which could not hear him or answer, deaf and impersonal like a wad of cotton.
*   I never thought one way or the other whether a building was beautiful
*   And against you you just have a vague fat blind inertia
*   People want to appear as if they've always had power

CH 14
-----

*   he forgot his own address or its existence.
*   We must develop a professional Spirit of unity and cooperation
*   The strange untouchable healthiness of his body
*   I can't feel any difference
*   You'll learn to love me
*   If I want to punish myself I'll marry you
*   Then he grasped suddenly that it was not a vague threat, but a practical danger; and he lost all fear of it. He could deal with a practical danger, he could dispose of it quite simply. He chuckled with relief, he telephoned Roark's office, and made an appointment to see him.
*   Just drop that fool delusion that you're better than everybody else---and go to work. In a year, you'll have an office that'll make you blush to think of this dump. You'll have people running after you, you'll have clients, you'll have friends, you'll have an army of draftsmen to order around! ... Hell, Howard, it's nothing to me---what can it mean to me?---but this time I'm not fishing for anything for myself, in fact I know that you'd make a dangerous competitor, but I've got to say this to you. Just think, Howard, think of it! you'll be rich, you'll be famous, you'll be respected, you'll be praised, you'll be admired ---you'll be one of us! ... Well? ... Say something! Why don't you say something?"
    *   One of us says peter Keating
*   Howard says why betray so much
*   He Hated Roark. It was only necessary to hate, hate blindly
*   Only the inexplicable frightened Keating
*   "A thing is not high if one can reach it; it is not great if one can reason about it; it is not deep if one can see its bottom"
*   venturing into the field of intellectual experimentation with a client such as Lois Cook." Toohey referred to the house as "a cosmic joke."
*   "That's good for you, Peter. One must never allow oneself to acquire an exaggerated sense of one's own importance. There's no necessity to burden oneself with absolutes."
*   "Oh," she said and winked. "One of us?"
*   It was you I could make myself want at one time
*   "Only in thinking how little we know about ourselves. Some day you'll know the truth about yourself too, Peter, and it will be worse for you than for most of us. But you don't have to think about it. It won't come for a long time."

Dominique says Peter you are everything I despise in this world

A furnace fed on their bodies

Land developed brains when they have failed everything else says Mr Toohey

Cause greater than yourself

Who are you and I to oppose the course of history

*   There were moments he forgot he was Peter Keating

There are so many things more important than life than happiness says Mr tewie

Dominique face shows "no meaning"

He managed to convey the charming impression that nothing of importance had happened because nothing of importance ever happened on earth.

A thing is not high if one can reach it if it's not great someone can reason about it it is not deep if one can see its bottom

There is no need to burden oneself with absolutes

It was you I could make myself want at one time

You will know the truth about yourself soon one day Peter and I will be worse for you than the most of us

Dominique says Peter you are everything I despise in this world

A furnace fed on their bodies

Land developed brains when they have failed everything else says Mr chewy

Cause greater than yourself

There were moments he forgot he was Peter keating

*   There are so many things more important than life than happiness says Mr Toohey
    
*   Leave it to the men of brains. Brains, of course, are a dangerous confession of weakness. It had been said that men develop brains when they have failed in everything else.
    
*   And it is said that but for the spirit of a dozen men, here and there down the ages, but for a dozen men---less, perhaps---none of this would have been possible. And that might be true. If so, there are---again---two possible attitudes to take. We can say that these twelve were great benefactors, that we are all fed by the overflow of the magnificent wealth of their spirit, and that we are glad to accept it in gratitude and brotherhood. Or, we can say that by the splendor of their achievement which we can neither equal nor keep, these twelve have shown us what we are, that we do not want the free gifts of their grandeur, that a cave by an oozing swamp and a fire of sticks rubbed together are preferable to skyscrapers and neon lights---if the cave and the sticks are the limit of our own creative capacities. Of the two attitudes, Dominique, which would you call the truly humanitarian one? Because, you see, I'm a humanitarian." says Toohey
    
*   It was an act of violence
    
    *   Dominique getting dominated
*   Nothing is superior so something
    
    *   Blah blah blah

Kindness is the first law says Mr Toohey

It was not loyalty to him but to the best within themselves

The causes of illusions are not pretty to discover they are either vicious or tragic

A board of directors is just one or two vicious men and a group of ballast

Integredy is the ability to stand by an idea

They are all against me but they all don't know what they want but I do

You want a man who believes in his work as you believe in God

One must mistrust their most personal impulses

It is a law of survival to seek the best

How did they ever let you survive

What is this war?

Your friends love all the things about you except the things that count

You're so healthy you can't conceive of disease

I don't know it's nature. Description of tire horror

The principal behind the dean?

A person that goes to a temple seeks release from himself

You must atone to your fellow men before you can atone to God

Listen, what's the most horrible experience you can imagine? To me---it's being left, unarmed, in a sealed cell with a drooling beast of prey or a maniac who's had some disease that's eaten his brain out. You'd have nothing then but your voice---your voice and your thought. You'd scream to that creature why it should not touch you, you'd have the most eloquent words, the unanswerable words, you'd become the vessel of the absolute truth. And you'd see living eyes watching you and you'd know that the thing can't hear you, that it can't be reached, not reached, not in any way, yet it's breathing and moving there before you with a purpose of its own. That's horror. Well, that's what's hanging over the world, prowling somewhere through mankind, that same thing, something closed, mindless, utterly wanton, but something with an aim and a cunning of its own. I don't think I'm a coward, but I'm afraid of it. And that's all I know---only that it exists. I don't know its purpose, I don't know its nature."

"The principle behind the Dean," said Roark.

Particulate really when you have no weapons except your genius

It is difficult enough to acquire Fame

Make senseless your ally

That's the trouble with victims they don't even know theyre victims

It is one man's ego defying the most sacred impulses of all mankind

To glorify man said Elsworth Toohey was to glorify the pleasure of the man

Don't ask them to achieve self-respect they will hate your soul

Let us say we are moles and we object to mountain peaks

Religion is a divisive class exploitation

What I am afraid of most is being myself because I am vicious

I know unhappiness comes from selfishness

I go for days and able to think unable to look at myself

As if there was nobody there to feel anymore

I think I hate the poor now I think all the other women do too

I don't have a single selfless person in the world who's Happy except you

To hell with everybody as long as I'm virtuous

Read entire conversation with Elsworth and Catherine.

We are poisoned by the superstition of the ego 1m

Book Quotes
-----------

Kindness is the first law says Mr Toohey

It was not loyalty to him but to the best within themselves

The causes of illusions are not pretty to discover they are either vicious or tragic

A board of directors is just one or two vicious men and a group of ballast

Integredy is the ability to stand by an idea

They are all against me but they all don't know what they want but I do

You want a man who believes in his work as you believe in God

One must mistrust their most personal impulses

It is a law of survival to seek the best

How did they ever let you survive

What is this war?

Your friends love all the things about you except the things that count

You're so healthy you can't conceive of disease

I don't know it's nature. Description of tire horror

The principal behind the dean?

A person that goes to a temple seeks release from himself

You must atone to your fellow men before you can atone to God

Mallory shot at Toohey because he knows everything about that beast

Particulate really when you have no weapons except your genius

It is difficult enough to acquire Fame

Make senseless your ally

That's the trouble with victims they don't even know theyre victims

It is one man's ego defying the most sacred impulses of all mankind

To glorify man said Elsworth Toohey was to glorify the pleasure of the man

Don't ask them to achieve self-respect they will hate your soul

Let us say we are moles and we object to mountain peaks

Religion is a divisive class exploitation

What I am afraid of most is being myself because I am vicious

I know unhappiness comes from selfishness

I go for days and able to think unable to look at myself

As if there was nobody there to feel anymore

I think I hate the poor now I think all the other women do too

I don't have a single selfless person in the world who's Happy except you

To hell with everybody as long as I'm virtuous

Read entire conversation with Elsworth and Catherine.

A board of directors is just one or two vicious men and a group of ballast

We are poisoned by the superstition of the ego

Believe Catie don't think believe

I want you to be proud of me Peter , says guy

The weapons are not up to your standard

Poor kids come to look at the temple for token broken people

Most people are concerned with the suffering of others I'm not

Gale, he was too correct

The desire to desire

I don't feel sorry for anyone

Men differ in their virtues if any but they are alike in their vices

It is not my function to help preserve a sense of self respect they haven't got

He drove himself like a slave

Weiland lost interest in breaking industrialists and financiers

As long as he still feared something he had a foothold on living

Peter keeting describes Dominique as a dead person

Where's your I Where's yours Peter

No happy person can be so impervious to pain

One can not love man without hating most the creatures that bear its name

You walking advertisement for contraceptives

He did not know if he should feel pain insult or confidence

Noted I think of a song nothing really matters

Accidents are unreliable ph

In unity there is strength

Noted Howard Roark and Dominique never ask when they know the other person will tell them what they need

And what makes the whining papers possible

Dominique tests ro ark when she says tell me in detail

Until you stop hating all this

Because his life wasn't worth living let alone recording

Suffering that went down to a certain point

Interpretation
--------------

Interpretation
--------------

One liner: There are two types of people those who are self determined and those who are group determined.

Which character represents how I want to live my life more Peter or Howard? I like to see myself like howard seeing through people towards my goals not caring what they think because my mission is pure but at the same time I really appreciate Peter's machiavellian attitude. Howard is an Egotist meaning he will likely turn into a machiavellian in the coming chapters. Peter removes all his obstacles in his way by acting two faced. How does howard remove his obstacles? Howard stares into the eye of god, asks a questions, then uses his body to create it.

One is not supposed to see themselves in these characters they are supposed to be their own individuals. Good artists copy great artists steal.

The two phrases that stick out so far with my reading is "One of us". I guess there are the prime movers and the establishment. Howard is a prime mover stomped on by the establishment. Peter Keating is a freshly minted member of the establishment.

There is this theme of seeing through people. Ellsworth Toohey forces people to see through him when Rand describes, "He managed to convey the charming impression that nothing of importance had happened because nothing of importance ever happened on earth." When Roark sees people he basically sees strait through them to their soul. There is a parallel theme about self determinism. Roark sees what he wants and strives towards it. Peter gets off to the idea of people thinking he is important. Rand Says, "The man-worshipers, in my sense of the term, are those who see man's highest potential and strive to actualize it." The science moves forward allowing the architect's to build higher and higher but when they do they just seem to recreate the past.
