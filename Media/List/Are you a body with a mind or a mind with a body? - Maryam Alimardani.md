---
share: true
uuid: 5d03a3de-fd28-4f41-905b-f39790e99b55
---
---
id: 00omd37wyj0g69jjtlif5z5
title: Are you a body with a mind or a mind with a body? - Maryam Alimardani
desc: ''
updated: 1639794788790
created: 1639794152242
---

## Summary

* [[Rene Descartes|Relationships.People.Rene Descartes]] believed in a non physical consciousness or immaterial soul.
* When we think about picking up a fork it is actually God who moves our hand.
  * Seriously WTF, this is an amazing idea
* Is the world a delusion only existing as mental perceptions
* Having a body is something we can not jus imagine away
* Are we a mind equipt with a physical body or a complex organism that's gained consciousness over missions of years of evolution

## Links

* [[Rubber Hand Illusion|wiki.concepts.list.Rubber Hand Illusion]]
* [[Extended Mind Thesis|wiki.concepts.list.Extended Mind Thesis]]

## Sources

* [Are you a body with a mind or a mind with a body? - Maryam Alimardani - YouTube](https://www.youtube.com/watch?v=ILDy6kYU-xQ)
