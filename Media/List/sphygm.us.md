---
share: true
uuid: 53f0db9f-79ac-4afb-b488-b389ee93812e
---
[[Website]]

## [[What are their interests]]
* [[Tiddly Wiki]]
* [[Question]]'s on this site I should index
* Life Logging
* Typography
* Creativity (hyper/dreams)
* Web Design
* Javascript
* Cicada 3301
* Their Wardrobe
* How To / Tutorials
	* [[https://sphygm.us/#How To%3A Archive All Website Subpages On Wayback]]
* Books
	* [[Terry Pratchett]]
* [[dentropydaemon-wiki/Heuristics/Conversation/36 Questions To Fall In Love]]
* Video Games
	* [[Satisfactory]]
	* [[No Mans Sky]]
	* [[Breath of the Wild]]
* [[Hyper Reading]]
* [[The Mazeway]]
* [[Placeness]]
## #Phrase 
* Offline vs. IRL

## #Quotes

I often feel unable to approach the infinite information. Instead of approaching it, I resort to dumping links into tiddlers so they pile up into a giant, teetering, rotting tower. As the stack grows higher, I feel even more unable to meet certain self-inflicted expectations of "keeping up" and "reading them all".

