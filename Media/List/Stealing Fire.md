---
share: true
uuid: 0d402b52-6f03-4eb7-9069-925d7b717688
---

[[books]]


[What is the story of Prometheus?](https://youtu.be/U_u91SjrEOE)
[[Steven Kotler]]
Zeus kill all the Titan's except two of them Prometheus and his Brother Epimetheus. Prometheus and Epimetheus are tasked with creating all life on earth. Prometheus tried to recreate Zeus and spent too long therefore there was nothing left go give humans. Prometheus steals fire from Zeus.

Zeus does not like power challenged. Zeus creates Pandora who marries Epimetheus and she get curious and opens her box up. Everything bad leaves the box except one important thing Hope. 

Frankenstein was subtitled the Modern Prometheus

Finished Chapter 1

flow: Defined as an “optimal state of consciousness where we feel our best and perform our best,” flow refers to those “in the zone” moments where focus gets so intense that everything else disappears. Action and awareness start to merge. Our sense of self vanishes. Our sense of time as well. And all aspects of performance, both mental and physical, go through the roof.

* [[Chapter 3 Why We Missed It]]
* [[Chapter 4 Psychology]]
* [[Chapter 5 Neurobiology]]
* [[Chapter 6 Pharmacology]]
* 