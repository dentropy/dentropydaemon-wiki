---
uuid: 232e2702-309b-4820-a5e8-30e9254b92f2
share: false
---
*   List of key loggers I was able to get to work
	*   [Bad display name "" when running via ssh · Issue #6 · moses-palmer/pynput](https://github.com/moses-palmer/pynput/issues/6)
	*   [How to Make a Keylogger in Python - Python Code](https://www.thepythoncode.com/article/write-a-keylogger-python)
	*   [keylogger · PyPI](https://pypi.org/project/keylogger/)
	*   [raspbian - How to start a python script at boot - Raspberry Pi Stack Exchange](https://raspberrypi.stackexchange.com/questions/108694/how-to-start-a-python-script-at-boot)
*   Links to Key loggers I could not get to work
	*   [gsingh93/keylogger: A keylogger written in Rust](https://github.com/gsingh93/keylogger)
	*   [GiacomoLaw/Keylogger: A simple keylogger for Windows, Linux and Mac](https://github.com/GiacomoLaw/Keylogger)
	*   [deadblackclover/keylogger: Register various user actions - keystrokes on the computer keyboard, movements and mouse keystrokes](https://github.com/deadblackclover/keylogger)
* [[LogKeys]]



## Previous Tries

``` python

# keylogger.py
import keyboard # for keylogs
from datetime import datetime

# https://www.thepythoncode.com/article/write-a-keylogger-python
# https://github.com/boppreh/keyboard#keyboard.hook

class Keylogger:
    def callback_pressed(self, event):
        # print(event.name)
        # print(event.event_type)
        with open(f"./test-keylog.txt", "a") as f:
            event_string = str(datetime.now().isoformat()) + " " + event.event_type + " "+  event.name
            print(event_string, file=f)
    def start(self):
        keyboard.hook(callback=self.callback_pressed)
        keyboard.wait()
if __name__ == "__main__":
    keylogger = Keylogger()
    keylogger.start()
```

``` bash
#!/bin/bash
sudo cp keylogger.py /opt
nano keylogger.service
sudo cp keylogger.service /etc/systemd/system/keylogger.service
sudo systemctl enable keylogger.service
sudo systemctl start keylogger.service
sudo systemctl status keylogger.service

```
