---
share: true
uuid: cc5cc49d-f554-4f29-b31a-b8789688e6a3
---

**A protocol for thinking via conversation**

* [[Roadmap - Question Engine]]
* [Source Code: question-engine](https://gitlab.com/ddaemon/question-engine)
* [[Design - Question Engine]]
* [[User Stories - Quest(ion) Engine]]
* [[User Research - Question Engine]]
* [[Development - Question Engine]]
* [[Filters - Question Engine]]
* [[Brain Goop - Question Engine]]
* [[Initial Questions for Question Engine]]
* [[Queries for DDaemon]]

![[Why am I building Question Engine?]]

## [[Mission Statement]]

Provide people a tool to not only speak through but think through. A tool that forces people to become who they are.

## Description

The general goal of Quest(ion) Engine to have a RPG quest engine for real life just like in [[Daemon by Daniel Suarez]]. Quests are arrived at by asking questions, Quest(ion) Engine provides the medium for this cognition. The mediums we use to consume data on our computers today don't capture the metadata context behind the decisions people make. Why does someone choose to consume one video over another? How much information does an emoji reaction to a message really have? Are people self censoring because they do not have easy to use fine grained permissions on the information they share? The medium is the message, Quest(ion) Engine hopes to define the message built into the mediums we use to communicate by first creating a system for RBAC Socratic dialog.

Quest(ion) Engine uses **[[Persona 5|Personas]]** rather than *accounts*. To "wield a persona" you use "[[metamask]]", a interface to the the many masks one can wield and display to the world. To wield a mask you must first "state that your life has value" by minting your own token and turning on a faucet so others mint your token to interact with you. Every post you or someone makes has to have a token attached to it. Your faucet has settings so you can control how people manipulate your attention for example, a faucet can only work every 20 minutes and give out only 4 tokens each time. When you interact with someone you must first use their faucet. Once you have their token you can spend it to reveal their answers to questions, reveal questions that are not publicly displayed on their profile, or send them a direct message. When interacting with yourself and others communication is contextualised by meme types such as questions and statements, this medium encourages people to structure their thoughts. This is useful because when you are now having a conversation you can attach tokens to different parts of the meme graph to signal where people should regulate their attention. This also allows someone create a persona, mint their own token, allocate it throughout the meme graph adding their own edges (links) and stuff. The goal of Question Engine is to provide people not only a tool that they can speak through but a tool they can think through.

## First Goalpost

I personally want a way of easily generating and tracking OODA (Observe, Orient, Decide, Act) loops in a recursive contextualised manner. I want to be able to dump each of my observations into separate memes, then link them together using a graph structure that can display the same data using different visualisation techniques. Some of these visualisation techniques could be a [[react-digraph|dag]], [[react-force-graph|force graphs]], tables, bubble plots, etc. etc. Examples of how to visualise memes graphs can be found [[Visualization Brainstorming - Question Engine|Visualization Brainstorming]] page. 

## Proof of Concept

The main reason I am working on Quest(ion) Engine is so I can easily come up with insightful and actionable queries I can run on the datasets I have access to. Think about it, if you had all the data you ever generated across your entire life indexed and easily queriable, what would you do with it? What would all that data be good for? Well maybe you should articulate your **Goals** and **Values** before you try to create a digital homunculus of yourself. Use Quest(ion) Engine to articulate the [[Heuristics of the Self]]. The heuristics of the self all exist to answer.... The First Question

## The First Question

"What are we to do with our lives" is a very interesting question we should each try and answer. Everyone has a subconscious reason for existing but none of us really articulate it. Understanding and articulating one's life purpose is difficult. We all want multiple things and whatever action we make towards one want places the other wants in a different location. Many of us don't even know what we want and need some help from others figuring that out. Through Quest(ion) Engine one can wield a Persona and have deep permissioned dialog with other Personas. Hopefully after enough structured dialog everyone will find meaningful questions specific enough so that they turn into quests.

## Links

* [[Quest]]
* [[Question]]
* [[Serial Experiments Lain]]
