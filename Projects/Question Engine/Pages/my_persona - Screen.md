---
share: true
uuid: aaf21e6e-d448-49f2-8a7e-1ce05e0d2342
---
What do you want the world to know about you?
What quest(ion)s make up your identity?
See what token balances you have sent out to people and modify them.

## Features
* Components
	* [[dentropydaemon-wiki/Projects/Quest(ion|Your Persona Pseudonym - Component]]%20Engine/Components/Your%20Persona%20Pseudonym%20-%20Component.md)
	* [[dentropydaemon-wiki/Projects/Quest(ion|Your Persona Description - Component]]%20Engine/Components/Your%20Persona%20Description%20-%20Component.md)
	* Public Key
	* [[ion|Your Quest(ion|[ion|[ion|[ion|[ion|[ion|[ion|[ion|[ion|[ion)s - Component]]s - Component]]s - Component](ion)s - Component](ion)s - Component]]s%20-%20Component)s%20-%20Component.md)

