---
share: true
uuid: b3533689-04f1-4549-9dee-6def145cfcc2
---
## Features

* Detect if Metamask is installed
	* If Metamask is not installed
		* [[No Metamask  - Component]]
	* If Metamask is installed
		* [[Share Identity - Component]]
		* Once user shares identity, [[Wield Persona - Component]]

## No [[MetaMask]] Message

You need to install "Meta Mask" before you can wield a persona. Please go to "metamask.io", install it, create your mask, and come back

## Share Identity Message

Before you can wield your person you must share your mask with us so we can know who you are. Please select the "Get Mask" button below and select "Connect" within metamask once it shows up.

**Get Mask Button**

## Wield Persona Message

You are about to weird your persona. We have taken note of your resolve. Please input your Pseudonym below and select ***Next***

Input for Persona

Next Button

Second Screen

[I am thou... Thou art I... ](https://megamitensei.fandom.com/wiki/I_am_thou)
Thou hast acquired a new vow...  

It shall become the wings of rebellion  
That breaketh thy chains of captivity.  
With the birth of ___Your Pseudonym___ Persona   
I have obtained the winds of blessing that  
shall lead to freedom and new power..."    **Next**

