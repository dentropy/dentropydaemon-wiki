---
uuid: 8c3a8ec8-121c-4356-b13f-f9b9085d76a0
share: false
---
## Survey Questions

* [[Where do you spend your time online?]]
	* Wikipedia
	* Reddit
		* SuperStonk
		* 3D Printing
* [[What do you find engaging on the internet?]]
	* Tutorials
	* How To's
	* History Articles
	* Obscure but interesting shit
	* Infographics and explanation gifs
* [[Where do you assert your will online?]]
	* Lurker
* [[What do you get out of lurking, consuming, the internet?]]
	* "Not A Lot Honestly, Procrasting"
	* Useless Facts
	* Some Useful Research when not procrastinating
* [[What do you like about group chats?]]
	* Not really in them
	* Variety of People
* [[What do you dislike about group chats?]]
	* Lot's of random shit
	* Stuff I do not care about
	* End up silencing them anyways
* [[What does the internet need more of?a]]
	* Tutorials and How Too Guides that are actually vetted
	* Free and Open Articles
	* 
* [[What does the internet need less of?]]
	* People Tracking You
	* People trying to make money off your data
* What online community do you wish existed?
	* Everything is on reddit already
* [[Do you want more control over your Social Media feed?]]
	* Don't really use it
* [[How would you query your friends public social media?]]
	* I don't
	* DM's
* [[How would you query your friends private daemon data given they need to approve your queries?]]
	* Why can't I just ask them

## Reflection

Rana is someone who does not interact a lot online choosing to interact with his actual friends in real life. He is a lurker. I need to ask more questions to understand more about why people choose to lurk and be programmed rather than perform real multiplayer cognition.

The questions I have asked are not specific enough. It would be useful if I could get their usernames and stuff for different platforms.