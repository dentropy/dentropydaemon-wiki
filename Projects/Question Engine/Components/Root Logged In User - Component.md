---
share: true
uuid: 34aca245-6ca0-4599-ad2b-11f8e7156c87
---
* Contains Tabs for each of the following screens
	* [[dentropydaemon-wiki/Projects/Quest(ion|context_feed - Screen]]%20Engine/Pages/context_feed%20-%20Screen.md)
	* [[dentropydaemon-wiki/Projects/Quest(ion|discovery - Screen]]%20Engine/Pages/discovery%20-%20Screen.md)
	* [[dentropydaemon-wiki/Projects/Quest(ion|question_log - Screen]]%20Engine/Pages/question_log%20-%20Screen.md)
	* [[dentropydaemon-wiki/Projects/Quest(ion|my_persona - Screen]]%20Engine/Pages/my_persona%20-%20Screen.md)
	* [[dentropydaemon-wiki/Projects/Quest(ion|view_persona - Screen]]%20Engine/Pages/view_persona%20-%20Screen.md)
