---
uuid: 3973524b-168d-4be5-8b97-c5104400c6dc
share: false
---
* ICO Yourself
* Question DAG
* Cognition DAG
* Oh that is why tiddlywiki and fedwiki are set up like that
* WAIT, this is just a DAG data engineering tool
* Your life can be represented as a DAG

Well what if we created a game where we had a question protocol and you gave any of your verified followers X tokens they could use to see your answers to different publicly viewable questions. There could also be follow up responses that also consume these tokens. And tagged questions that also cost tokens. In order to get more tokens the user has to give them to you. The tokens are soul bound and attached to a NFT.

## From [[Question Engine]]

But there is no value in a system like this if everything is public like with twitter. The real world is a "dark forest" where you can see strangers, acquaintances, coworkers, friends and enemies but you don't know them deeply. Each user can have a series of public questions and statements on their profile that people can unlock via spending their token or possibly more complex transactions. For example the question "What is my favourite movie?" can be public but the users answer will require you use their faucet, get their token, and spend it to find out.

We develop a game of Q&A similar to what [[OK Cupid]] has. The question and answers in OK Cupid worked by requiring you yourself answer a question before you were allowed to see the answers of other people. This core concept is valuable but needs to be meddled with because [a Math Genius hacked this system ti get tones of 100% matches](https://www.wired.com/2014/01/how-to-hack-okcupid/). Dating apps don't really support multiplayer, you don't necessarily have three people meeting up to go on a date. The core value of the share your answer to see mine play mechanic works because "work" has to be done to answer the question.

## Thoughts that belong here

I need to commit absolutely to something. What is that something. Treating people as API's. Getting their data out and into a system that they can query. An API for the human soul. The first soul we interrorgate is Mine. What questions can I ask of my digital soul?

Where is the hidden core of crypto competence. We will find the prime movers inside every community putting the time in. We will measure the github issues.

Fully Annotated Joe Rogan Experience, The Portal With Eric Weinstein, Charles Hoskinson Chats.

I need a reading list to hand other people, contextualised media.

[[What is the point of gathering all my data in one place with a nice API and interface, what am I even going to do with it?]]

## It is basically impossible for someone to be absolutely honest with another person.

## TODO

* Get a RPG geek to write a review on quest engines. Pay someone to do the research.
* Google Circles
* Quest Engine / Self Authoring Program / Persona Game
* Question Engine
	* Poking people with questions.
* Tupla / Egregores Persona Creator
* [[Ultimate Tagging Machine]]
* [[Queue Management]]
* Memex
* You can request data from the System D for Humans
* Remember your card/board game engine you wanted to build!?!?
	* That engine can be used to make DAO's
* I need to tool that reflects the commitments I made in the past
* There is also the tool to manage human memory and help you define things and stuff
	* Write summary of what you read, and enhance them, force the end user to have recall
* What courses or skills am I currently working on, where can others find this information about me
* Remember query your friend amazon for a coffee maker
* Like those stupid quizz websites that harvest your data but questions you actually care about and you can share your answers with your friends
	* Who doesn't like getting asked questions about themselves 
* I want a knowledge graph backing my [[dentropydaemon-wiki/Software/List/Raindropio]]
* Let's reverse engineer the sources of power. Measure Prestige itself.


There is the self aware social graph based more on conversation and metadata tagging

THIS IS AN OSINT TOOL, THE PERSONA AND DATA I CAPTURE ABOUT THEM ARE PEOPLE I DO INVESTIGATIONS ON

Question Engine is supposed to be the backbone of my tutorials, question engine can be the live stream reddit thing I have always thought about.

I want Question Engine to allow me to become my own best friend, a framework that allows me to wield the personas I need to get to the bottom of the questions I like to define myself by

What are the symbols, tags that go on the map.

Question Templates, sorta OKCupid style

You know recursive tags can replace folders?

The data self if object storage. The computational self can spin up containers and keep data hot. The masked self is a series of server instances that are federated.

Measure the spend of attention and "question" it

Imagine if there was a competition to see who can annotate and link the same data better.

We need a file system where the same people can look at the same DID and see different info. This is how intelligence operates. Different levels get the same documents, files, and see different things.

We can use any message log as the root, twitter, group chats, discord, ceramic.network, nostr, farcaster. 

We can use whatever file system we want, perkeep, IPFS, ZFS, XFS, FAT, Object Storage etc. etc.

How can these file systems all be connected. We need object storage after. Well we can just adopt whatever perkeep does and name all those files. Perkeep is a requirement, fuck anything else.

But we can't just index discord, twitter, and NOSTR JSON to perkeep as individual messages?

Why not? We can DID it up with JSON bro. Then tag everything with that DID.

NO we do not tag everything with that did, we can store all these messages and crap, even do basic labels and stuff, but the relations need to be in a database of some kind.

So what order do we write the bindings in? Where does the NLP come in? Can we use the ChatGPT API?

Let's ask the dreams

User story, every one of these double enters should be a meme within the meme file system. We need to bring back the ultimate tagging machine.

Wait if I scrape all this data I can can tag it, I can tag it using objects. Labels to labels. Then I can start reverse engineering Discord Communities. The ultimate OSINT toolset. I can zip up entire servers and send them to people who can dump them to their perkeep.

How do we store the annotations? If they are not in perkeep?

Meme log bro. File systems are not databases, but databases run on file systems.

We need a data transformation langauge. No we just need to log into perkeep or something better. Prometheus, wielding fire anyone? The software is going to need logging anyways to understand oneself and it is not like we are running on urbit.

**SCHEMA FOR HUMAN BEHAVIOUR**

Understand and shape the world using your curiosity
Google Circles
Encourage
It is your job to find the objective


## Reminders


* Media Relationship Manager
* A schema for human experience
* A catalog of the human experience
* Proof of Meme
* OSINT research
* Meta annotation graph for raindrop, hypothesis and stuff
* Egregore Manager
* We gotta have a way to delegate authority of memes as well as group permissions
* Data Structure for metadata within a conversation
* Can we pay people on fiver to do research for us?
* I need a public version controlled quest log
* [[dentropydaemon-wiki/Wiki/Concepts/List/Areas of Improvement]]
* Fandom Binding
* Media Wiki Binding
* Isn't the idea that we can have the same persona used by different people pretty cool
* Token Curated Registries
* Discord Binding
* Likes are spending your tokens on other people's memes
* Fix the symbolisim of perform_transaction, send_transaction!?!?
* Comparison to [[Basic Attention Token  BAT]] and [[dentropydaemon-wiki/Software/List/Brave Browser]] integration
* OSINT operating system Egregore attached to ENS names with probabilities
* Input my social consciousness, bookmarks, history, ddaemon style
* The client side object will be called QuestJournal
* A protocol for [[dentropydaemon-wiki/Wiki/Concepts/List/Socratic Dialog]]
* People find their own answers and label them that way via edges to statements
* NO TAGS ON MEME's WE USE EDGES
* I don't want to be like a [[MyDendronExistence/Relationships/People/Ward Cunningham]] and let people like Jim whales run away with my idea
	* Seeing the adoption of [[dentropydaemon-wiki/Software/List/fed.wiki]] is depressing
* If you have not minted anything you can not receive transaction, you must declare you have worth before you can receive anyone's tokens
* What if there was a MEME file system, or MEME Database
* "Send me a meme and I'll add in the edges"
* Do we want to have a signature chain!?!?
	* YES it is the time to do this now
* Imagine having context tokens that can be minted to showcase how attention should be allocated to different topics. This is how Corporations can be managed
* Context feed needs "instruments" that people can use to speak through. Rather than corporations we have instruments and you need to be proficient to play them or get permission from the creator. You wield an interment rather than speaking through a corporate account. whahahaha. I wonder if I can get to the point where I get the Magic symbolism in [[dentropydaemon-wiki/Media/List/Daemon by Daniel Suarez]]
* This is a killer app, you get to say how important your speech is by minting and spending your own token
- How are you supposed to wield multiple persona's at once?
	- That is something the frontend and API can be updated for later, after NFT and Graph Database integrations
- So up votes are tokens personally given from someone to someone  So this in final application would be like taking a NFT and saying I use this NFT as my identity and attach a name to it. That would be on chain.
- What are we doing for RBAC on questions answers and being discoverable 
- Questions need UUID's
- Answer someone's question or respond to them get an additional token
- What about spending someone's token to get them to answer this question
- Root Questions as a concept.
- When you answer someone's questions you should have to spend more tokens to see who else has answered.
- How should people be rewarded for spending tokens, the issuer has to acknowledge and reissue.
- Questions are blue, Statements are Red.
- How many times can the faucet be used?
- Just once for now.
- Have questions that also require a note to unlock and have to be reviewed by the owner.
- We want to have the user of the root question be able to take the questions and statements and form context with it.
- Wouldn't it be cool if these other Persona's showed up along the top like Tabs
	- Actually showing up along the left side like discord would be ideal.
- What if we tokenized raw human attention.
- Maybe we can rebrand this Meme graph for the nerds.
- What are the goals of this system.
- Wouldn't it be cool if we know who was posting under people's social media handles for like political parties, candidates, and corperations. Or we had the choice to reveal this information?
- Think about [[dentropydaemon-wiki/Software/List/GunDB]] [[dentropydaemon-wiki/Software/List/Tiddly Wiki]] crossover for Quest(ion) Engine
- How do we want our Persona's to query each other
- There are no friends unless you label yourselves with a question and state, there is only value exchange between people
- What about blocking?
- MEME edges should flow through memes, aka be transformed
- You have to spend your own currency before other's will give you theirs.
- How are we doing friends and stuff? Like labelling people?
	- Umm there is a root statement like "Friend" then a user adds an edge to the User, which should be an NFT gut for now is a PSEUDONYM#PUBLIC_KEY
- So we should have a default transaction for registering users
	* Ummmmm YES it should technically be on chain. You don't say your name unless there are other people to hear it,
* What is the user story for breaking apart meme's. the same way you annotate an article.
* A protocol the culture from Ian m banks can awaken from
* Features
	* We may also want to be able to send hash of data anonymously 

## Notes

## Notes
What is this Quest(ion) Engine thing?

A protocol for thinking via conversation

Why can't our technology encourage us to have fruitful conversations with others rather than interact with mobs. The problem with the internet is that we removed the author from their works let's reconnect that with personas.

Life is made up of quests. Quests for things such as food, status, knowledge, and meaning, quests are everywhere. The concept of quests is so embedded within our society that we forget that it is the root word for question. Questions are just quests for information. Well I think quests are super important, they are our best tool to judge people and provide a framework for people to navigate the world and relate to one another. Next time your meet someone ask them what quests are they working on. Now ask yourself what quests are you personally working on. I like like to think a large part of my identity is the quests I am working on and previously accomplished. 

All media is quest driven. It suggests questions in the consumers mind that it and only it can answer.

Where do quests come from, how are they assigned, how are they evaluated, tracked, and rewarded.

I firmly believe that if you ask a person the right question you can force them into a level of self awareness that they did not know they were capable of? 

How are we to organise society and our lives using quests? Well we all have a set of skills, connections, information, and knowledge. And if we have a community to play with others, a real community where you can relate to others that are doing the same quests you are. A 24/7 conversation to contextualize the worlds knowledge while we all self actualize. 
