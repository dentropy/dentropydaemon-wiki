---
uuid: 10580093-aa12-4939-8f56-c7525a1bbeca
share: false
---
## Survey Questions

* [[Where do you spend your time online?]]
	* Instagram
	* Facebook, Organizing Photos
	* Tik Tok
	* Streaming
	* Reading, AO3
	* Pintrest
* [[What do you find engaging on the internet?]]
	* Learning how to do things
		* Food and Makeup
		* General Knowledge
		* Obscure Factoids
		* How to be a better person, self help
	* Inspiration for creative stuff
	* Community
* [[Where do you assert your will online?]]
	* Local friend group on Instagram and Tik Tok
	* Queer Community, Tik Tok, Instagram, Facebook
	* What about twitter
		* I don't
	* Have friends on twitter
	* The people from the queer community always have an instagram if they already have a twitter
* [[What do you get out of lurking, consuming, the internet?]]
	* Learning new things
	* Good Feelings
	* ASMR
* [[What do you like about group chats?]]
	* Easier to plan things rather than texting everyone individually
	* Sharing memes
	* Do you ever have to mute chats you no longer engage in?
		* Ya
		* Why do you mute them
			* I don't really want to leave
			* I mute chats when they are over whelming, like texting every two seconds, and I am not engaged
		* Where do these chats come from?
			* They are from Discord, I ended up deleting discord on my phone
* [[What do you dislike about group chats?]]
	* When they get spammy
	* Too many group chats are a problem
	* One person has a falling out and you have to create a new group chat to pretend you do not have have the new group chat
* [[What does the internet need more of?]]
	* Privacy, not like data tracking, but like posting on instagram and restricting which followers can see it
	* Restrictions for children
* [[What does the internet need less of?]]
	* Karens and shitty people but not shit posters more like bullys
	* Less Children, why do 8 year old's have tik tok accounts
* [[Do you want more control over your Social Media feed?]]
	* Yes
	* What shape might that control take?
		* I want to block by content type, like sounds and filters, on tik tok
		* Block specific trends and dances rather than users
		* I have to change my algorithm to see more recipies, if I want to see something else reccomended I need to go back, why can't I mix them rather than have it biased one way or the other
* [[How would you query your friends public social media?]]
	* I don't know
* [[How would you query your friends private daemon data given they need to approve your queries?]]
	* This matters more if someone died
	* Paul needs a better explanation
* [[Can you go through each of my personal questions and mark which ones you find interesting?]]
	* 