---
uuid: 09ca0ef4-eeb9-428d-9d2c-d501e0267bac
share: false
---
## Know who views my wiki

I [[Paul Mullins]] want people to get to know me through my public wiki. The problem I have is that I don't know when people go to browse my wiki, what they browse, if what they browsed resonated with them, or get feedback within the context of my wiki. I can have a tokenomics model for people that browse my site where people need to mint my token and spend it to view answers to my questions. That's it.

