---
share: true
uuid: 18993f72-82e2-4297-af4f-1d07a7e220ae
---
Randy sees four tabs "Context Feed", "Quest(ion) Log", "Discovery", and "Your Persona"
* Randy sees that the "Quest(ion) Log" tab is selected
* Randy reads, 
	* ![[question_log - Screen#Quest(ion) Log No Logs Description]]
* Randy selects the "Discovery" tab but does not see any users.
	* ![[discovery - Screen#No Public Users Message]]
* Randy selects the "Your Persona" tab. 
* Randy sees the tab change and sees a popup.
	* ![[Empty Personal Wallet Popup]]
	* Randy the one button, metamask pops up, he sign's, randy ends up back in Quest(ion) Engine
* Randy sees three separate areas within the Profile Tab
	* **Pseudonym**
	* **Public Key**
	* **Description**
	* **Your Quest(ion)s**
* **[[Your Persona Pseudonym - Component]]
	* ![[Your Persona Pseudonym - Component#Description]]
* Public Key
	* This is for re-keying, will redo later and probably will use NFT's or some other kind of [[dentropydaemon-wiki/Wiki/Concepts/List/DID]] in the end
* **[[dentropydaemon-wiki/Projects/Quest(ion|Your Persona Description]]%20Engine/Components/Your%20Persona%20Description%20-%20Component.md)**
	* The description area randy sees the following text, "You have not added a description to your profile yet." under this Randy sees a ***Text Box*** titled **Input New Description** with a ***button*** below that says **Update New Description**.
	* Randy fills in the his profile **Input New Description** text area with the following. 
		* ![[ion|dentropydaemon-wiki/Projects/Quest(ion]] Engine/Peronas/Randy.md#Description]]
	* Randy selects the update button and sees a popup saying "Profile Description Updated", he refreshes the page and sees his profile has been updated.
* **[[ion|Your Quest(ion|[ion|[ion|[ion|[ion|[ion|[ion|[ion|[ion|[ion)s - Component]]s - Component]]s - Component](ion)s - Component](ion)s - Component]]s%20-%20Component)s%20-%20Component.md)**
	* Below the Description component that is there is a separate are titled "Questions" under which it says "Post a Q&A for yourself" and "Add a Question for Others to Answer"
	* Randy then selects the "Post a Q&A for yourself" button in the **Your Quest(ion)s area**.
		* Within the same page a "Text Field" labelled "Question", a "Text Area" labelled "Answer", and a "Submit Q&A" button popped up
		* Randy fills in the question "Text Field" ,
			* ![[ion|dentropydaemon-wiki/Projects/Quest(ion]] Engine/Peronas/Randy.md#Randy's First Question]]
		* After asking his first question Randy sees a [[dentropydaemon-wiki/Projects/Quest(ion|Turn on your faucet]]%20Engine/Components/Turn%20on%20your%20faucet.md) popup.
			* ![[ion|dentropydaemon-wiki/Projects/Quest(ion]] Engine/Components/Turn on your faucet.md#Popup]]
		* Randy fills in the "Answer" "Text Area" with the following
			* ![[ion|dentropydaemon-wiki/Projects/Quest(ion]] Engine/Peronas/Randy.md#Randy's First Answer]]
	* Randy then selects "Submit Q&A"
		* Randy see a popup saying his "Q&A added to Quest(ion) Engine successfully" as well as a list of Q&A within the UI added
		* Randy then selects the "Add a Question for Others to Answer" button and sees a "Text Field" labelled "Question" with a "Submit Question" button popup
		* Randy fills in the "Text Field" with 
			* ![[ion|dentropydaemon-wiki/Projects/Quest(ion]] Engine/Peronas/Randy.md#Randy's Second Question]]
	* Randy clicks the "Submit Question" button
	* Randy sees a quick popup saying "Question added to Quest(ion) Engine successfully"
	* Randy logs off while he awaits for other people to join Quest(ion) Engine or respond to his questions from the shadows