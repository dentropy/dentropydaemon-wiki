---
uuid: 661fa275-19ee-42a8-b547-7b3713e51fa3
share: false
---
In [[Questions for Question Engine]] I have a list of Survey Questions. I want to have users able to answer these questions through Question Engine?

* There are a series of problems with this? I will need a custom persona.
* There are a series of problems with this, for example I need a survey persona that has a minting module that allows users to mint at least 3 tokens for each question they answer
* I also need to be able to group these questions, I haven't got a data structure for that
* Well I can have a custom persona for each set of questions, that way we can easily query everything from the backend

