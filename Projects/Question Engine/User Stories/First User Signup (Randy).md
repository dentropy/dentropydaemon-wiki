---
share: true
uuid: bd5c091b-6af3-48b9-bc4d-f17fb60961a7
---
* Randy goes to QuestionEngine.TLD
* The ***Home Page*** is simply a title that says *Quest(ion) Engine*
	* A subtitle that says 
	* ![[home - Screen#Home Page Subtitle]]
	* Followed by a description
		* ![[home - Screen#Home Page Description]]
* Randy sees a "Wield Persona" button below and selects it
* Randy get's brought to the [[wield_persona - Screen|Wield Persona]] screen where it says, 
	* ![[wield_persona - Screen#No MetaMask Message]]
* Randy goes to [[dentropydaemon-wiki/Software/List/MetaMask]].io and generates his first EVM keys to be used to manipulate their Persona
* Randy goes back to QuestionEngine.TLD tab and the [[wield_persona - Screen]] updated
	* ![[wield_persona - Screen#Share Identity Message]]
* Randy selects the **Get Mask** Button, he sees a popup in metamask asking to share his account, he selects "Account 1" then selects "next" then finally selects "connect" all within the metamask popup.
* Randy comes back to the login page and sees a different message,
	* ![[wield_persona - Screen#Wield Persona Message]]
* Randy inputs his Pseudonym, Randy
	* *This is a user story why make things complicated, there are many Randy's out there*
* Randy selects next
* Randy sees metamask popup that asks for a signature, he selects sign
* Randy is now brought into the "Quest(ion) Engine" application on the "Quest(ion) Log" screen.