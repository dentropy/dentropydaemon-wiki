---
uuid: a421f02e-264c-4885-acdc-18b08cdc678e
share: true
---
## First Perfect my Obsidian Publisher

![[Logs - dentropys-obsidian-publisher#Feature Backlog]]

## Question Engine Roadmap

* PKM to SQLITE
* Render PKM to website from Database
* Add metamask auth to website PKM
	* Groups based Auth for PKM
	* Auth Gui
* Publish my Personal Questions
	* Make sure User Flow Diagrams are done at this point
* Custom Meme Collections
	* Priority within the hierarchy


## Extended Roadmap

* Graph Based RBAC, share documents with one degree of separation from X note