---
share: true
uuid: f137b314-579f-42ab-8be5-1c72bf9ebcd9
---
## Story List

* [[First User Signup (Randy)]]
* [[User posts first questions and answers (Randy)]]
* [[Second user joins and responds to questions and answers (Stacy)]]
* [[Randy Signs Back in After Stacy Interacts with Him]]
* [[The Fight for Context]]
* [[Learn and Teach to Code (James)]]
* [[Archetypal Narratives (Addison)]]
* [[Mapping Knowledge Maps (Randy)]]
* [[The Archivest (Gwen)]]
* [[The History Buff (Dan)]]
* [[The Conspiracy Buff (John)]]
* [[Example Conversation]]
* Future Stories
	* [[Meme Permissions]]
	* [[Quest Engine (Paul)]]

## Stories of Features

* [[Conduct user surveys through Question Engine]]
* [[Know who views my wiki]]
* [[Have context overlaid on textual conversations]]
* [[Share access to my bookmarks]]
* [[Embeddings on every meme]]

## User Flow Diagram

## [[List Personas - Question Engine]]

* [[Randy]] - First User
* [[Stacy]] - Second User
* [[Dan]] - History Buff
* [[Gwen]] - Archivist
* [[James]] - Learn and Teach to Code
* [[John]] - Conspiracy Buff
* [[Addison]] - Archetypal Narratives


