---
share: true
uuid: 5fd78387-a5c4-4163-a464-861316fe4ba1
---
* GET /get_cookie
	* Input
		* NOTHING
	* Returns STATUS_CODE
		* GET_TOKEN
			* BEARER_TOKEN
* POST /wield_persona
	* ![[ion|dentropydaemon-wiki/Projects/Quest(ion]] Engine/API/POST wield_persona.md#Input]]
	* ![[ion|dentropydaemon-wiki/Projects/Quest(ion]] Engine/API/POST wield_persona.md#Returns]]
* POST /perform_transaction
	* Input
		* data
			* pseudonym
			* unix_time_ms
			* transaction_code
			* token_pseudonym
			* token_public_key
			* from_pseudonym
			* from_public_key
			* to_pseudonym
			* to_public_key
			* value
			* meme_link (optional)
			* meme_edge_link (optional)
		* hash
		* signature_of_hash
	* Returns STATUS_CODE
		* PERFORM_TRANSACTION
* POST /submit_meme
	* Input
		* data:
			* pseudonym
			* unix_time_ms
			* signing_key
			* body
			* permissions
			* type
			* status_code
			* pseudonym
			* signing_key
			* edges (optional)
		* hash
		* signature_of_hash
		* transactions []
	* Returns STATUS_CODE:
		* SUBMIT_MEME
			* MEME_CONTENTS: {}
* POST /SUBMIT_MEME_EDGE
	* Input
		* data
			* pseudonym
			* unix_time_ms
			* signing_key
			* source_meme (required)
			* tag_type (required)
			* context_data (optional)
			* reference_meme (required)
		* hash
		* signature of hash
		* transactions []
	* Returns
		* STATUS_CODE: True
		* MEME_EDGE_CONTENTS: {}
* [[dentropydaemon-wiki/Projects/Quest(ion|POST query_memes]]%20Engine/API/POST%20query_memes.md)
	* Get all public memes from a Persona
	* Get all protected memes from a user
		* Should these require an edge? Yes
	* 
	* RETURNS
		* [[dentropydaemon-wiki/Projects/Quest(ion|View Full Profile Component]]%20Engine/Components/View%20Full%20Profile%20Component.md)
* POST /DISCOVERY
	* Returns List of
		* Pseudonym (With Eth Public Key)
		* Summary from /REQUEST_PERSONA

