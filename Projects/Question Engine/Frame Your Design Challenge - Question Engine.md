---
uuid: 64315c84-e213-4ded-ac3a-54206135879d
share: false
---

## First Take

1. Take a stab at framing it as a design question.
	1. What am I supposed to do with all my data if it was easily querriable via an API
2. Now state the ultimate impact you're trying to have.
	1. Give people a medium where they can easily understand themselves and others around them via their digital data
3. What are some possible solutions to your problem?
	1. Think Broadly, it's fine to start a project with a hunch or two, but make sure to allow for surprising outcomes.
		1. There is none, everything is held within gated communities online
		2. Each platform does have their own search though
		3. [[Google Desktop]]
		4. [[Elasticsearch]] if you have all your data already
4. Finally, write down some of the context and constraints that you are facing.
	1. They could be geographic, technological, time-based, or have to do with the population you're trying to reach.
		1. I want a prototype to demo people in 3 weeks from [[2023-06-07]]
		2. I want federation, but I don't want it in the POC
		3. I want to build on top of an existing messaging protocol but I don't want to learn it
5. Does your original question need a tweak? Try it again.
	1. Yes, I want to use question engine to scope out Dentropy Daemon but that is not what it is really for. Question Engine is supposed to be a tool for cognitive computing. [[What is cognitive computing?]] So what is Question Engine. 

## Second Take

1. Take a stab at framing it as a design question.
	1. [[How can we capture the context and thoughts of people when they are interacting with a computer?]]
		1. How can we capture the context inside the minds of people as they navigate information on their computer.
	3. [[Can the interface we use to interact with our technology be changed to encourage better question asking?]]
		1. Intelligence is mostly about asking the right questions, can the interface we use to interact with our technology be changed to encourage better question asking?
	4. [[Can we turn social media into a RBAC file system?]]
	5. [[How can we teach people to help themselves?]] 
2. Now state the ultimate impact you're trying to have.
	1. Have an AI companion that can help me live my life
	2. Have a system understand me better than I understand myself
	3. Give people a tool that shapes their attention in a way so they can solve their own problems
	4. Provide the best social media experience
	5. **Provide a system that can bootstrap people's digital daemon companions**
3. What are some possible solutions to your problem?
	1. Think Broadly, it's fine to start a project with a hunch or two, but make sure to allow for surprising outcomes.
		1. [[ChatGPT]] with their Plugins
		2. Google going full search on everything
		3. [[Umbrel]] if it had the right apps
		4. A software set that can query people's Data Self via API search rather than indexing it all
4. Finally, write down some of the context and constraints that you are facing.
	1. They could be geographic, technological, time-based, or have to do with the population you're trying to reach.
		1. I need to schedule interviews and post on forums to get the interviews I need for the product, but I need a working product in order to help prioritise which user groups, forums and stuff get prioritised
5. Does your original question need a tweak? Try it again.
	1. Well maybe we are in a good spot if we decide on one.
	2. Can't try again

## Third Take

1. Take a stab at framing it as a design question.
	1. Can we provide people a tool that makes getting to know oneself as fun as playing an addictive video game
		1. Check this twitter thread
			1. [Paul mullins (h/ist) on Twitter: "Give people a tool they can not only speak through but think through" / Twitter](https://twitter.com/PaulWMullins/status/1632183202813992960)
				1. What if there was a social media protocol that actually encouraged people to know thyself rather than just wear a mask for others to see.
				2. What would it take for someone to wake up and be excited to live the life they are already living?
				3. Imagine there was an explicit competition to design the human experience
				4. How do you make getting to know oneself addicting?
	2. How do we model the meaning behind what people are actually saying?
2. Now state the ultimate impact you're trying to have.
	1. Code the premisies that will then be used to build a government for the digital age
3. What are some possible solutions to your problem?
	1. Think Broadly, it's fine to start a project with a hunch or two, but make sure to allow for surprising outcomes.
		1. [[Gnosis Safe]] and other DAO frameworks
		2. Social Media, [the parliament of man](https://graymirror.substack.com/p/1-a-general-theory-of-collaboration)
		3. Whatever Software Blackrock Uses
		4. [[Palentir]], of Palentir is definitely part of the solution
4. Finally, write down some of the context and constraints that you are facing.
	1. They could be geographic, technological, time-based, or have to do with the population you're trying to reach.
		1. We have three weeks
		2. My friends and family have to be able to use the application and see the utility in it
5. Does your original question need a tweak? Try it again.
	1. I have lots of questions, is there one I can choose over the others? 
	2. I think we can take these two questions and rework them at a later point
		1.  What if there was a social media protocol that actually encouraged people to know thyself rather than just wear a mask for others to see.
		2. What would it take for someone to wake up and be excited to live the life they are already living?
	3. These questions are what I am generally working towards, but are they the best fit for Question Engine?
	4. Yes the first one is, Question Engine has masks built into the root product


## Links

* [[IDEO - Frame Your Design Challenge]]