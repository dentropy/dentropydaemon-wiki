---
uuid: 344c2005-e043-44c9-82a2-1ae504cdee33
share: true
---
- [[What book are you reading?]]
- [[What anime are you watching?]]
- [[What do my friends think about X media?]]
- [[What movies have you watched recently?]]
- [[What does one's like on Twitter say about them?]]
- [[What is my relationship with different social media sites?]]
