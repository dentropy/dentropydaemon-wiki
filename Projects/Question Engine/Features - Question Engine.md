---
uuid: 40ff6387-1d12-4891-909d-9fa4e526ba9b
share: false
---
* Single interface to manage many personas
* Have your social media conversations logged via recursive signing of your private key
* Limit and encourage social interaction with modular token system
* RBAC for each meme generated from a persona
