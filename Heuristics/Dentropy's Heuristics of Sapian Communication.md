---
share: true
uuid: 383c9e4d-d96b-45c5-83f0-5bde0d9d6d29
---
You can not tell people what or how to think you can only provide them a path to follow.

Why do people say they are not okay when they are okay and what does that have to do with cognitive load.

[[Thinking Fast and Slow|wiki.media.list.Thinking Fast and Slow]]

Bias for how people try to fit what they learn into their own schema rather than creating a parallel schema.

I am the universe but I got trapped in this body.
