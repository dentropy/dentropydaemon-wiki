---
share: true
uuid: 8b1c2ce8-4de9-4d22-9d5b-d542b6cc8657
---
---
id: uMZZkRlqVN0yCIb4s09Vj
title: Sobol s
desc: ''
updated: 1641744236451
created: 1641744208845
---

Sobol’s Law: Organizations are far more complex than estimated

## Links

[[Sobol.io|swarmio.Swarmio Research.DAOs.list.Sobol]]

## Sources

[Sobol Blog: What is Sobol's Law?](https://sobol.io/blog/what-is-sobols-law)
