---
share: true
uuid: 2781f257-2980-4661-bc02-635e6cc59112
---
---
id: k9U6sTOB1iW25tItQ5f6K
title: Cosmic Sociology
desc: ''
updated: 1635209940174
created: 1635209883772
---

The axioms of cosmic sociology are: “First: Survival is the primary need of civilization. Second: Civilization continuously grows and expands, but the total matter in the universe remains constant” (13). These axioms present a negative answer to the above question: the universe does not have room for morality; it is a dark forest ruled mercilessly by the laws of jungle.

## Links

* [[The Three Body Problem|media.list.The Three Body Problem]]

## Sources

* [The Three-Body Trilogy: The Three-Body Problem, The Dark Forest, Death’s End | MCLC Resource Center](https://u.osu.edu/mclc/book-reviews/mingweisong/)
