---
share: true
uuid: 4b4921f5-3d0d-4698-ad55-434e6c117943
---
[36 questions designed to help you fall in love with anyone - Big Think](https://bigthink.com/surprising-science/how-to-fall-in-love-36-questions-and-deep-eye-contact/)

* [[What is the best way to get to know you as a person?|Questions.list.what.is the best way to get to know you as a person?]]
* [[What is your life story?|Questions.list.what.is your life story?]]
* [[What traits do you envy and or value in those around you?|Questions.list.what.traits do you envy and or value in those around you?]]
* [[What do you feel insecure about?|Questions.list.what.do you feel insecure about?]]
  * This one is higher variance - I don’t recommend leading with it!
* Friends questions
  * [[What do you value in friendships?|Questions.list.what.do you value in friendships?]]
  * [[What are the best ways friends add to your life?|Questions.list.what.are the best ways friends add to your life?]]
* [[How, historically, have you become close to people?|Questions.list.how.historically, have you become close to people?]]
* [[If you could design a personal set of social norms for how your friends interact with * you, what would they be?|Questions.list.If you could design a personal set of social norms for how your friends interact with * you, what would they be?]]
* Perception Questions
  * How would other people describe you? 
  * How does how people describe you compare to how you want to be * perceived?
* [[What in life do you get truly excited about?|Questions.list.what.in life do you get truly excited about?]]
