---
share: true
uuid: 46a9da70-94da-4916-9840-5e23f20eda26
---
**Just check out the link in the Sources**

* Explore / Exploit 
  * The latest verses the greatest
  * Minize Regret
  * A/B Testing
  * Randomized A/B Testing towards a threshhold
  * Everything depends on Time Investment
  * Gittens Index
  * Win Stay, Lose Shift
  * Confidence Bound?



## Links

[[.md|media.list.Algorithms To Live By]]

## Sources

* [Algorithms To Live By](https://koraytugay.github.io/content/literature/algorithms-to-live-by.html)