---
share: true
uuid: 550c359a-b6b9-45f7-87f0-3325be73cf59
---
---
id: 6Sd4B9P7WpQl6kojCKcq3
title: 12 Rules For Life
desc: ''
updated: 1628449250301
created: 1628449250301
---
# 12 Rules For Life
12 Rules For Life
-----------------

1.  Stand up strait with your shoulders back
2.  Treat yourself like you are someone you are responsible for helping
3.  Make friends with people that want the best for you
4.  Compare yourself to who you were yesterday, not who someone else is today
5.  Do not let you child do anything that makes you dislike them
6.  Set your house in perfect order before you criticize the world
7.  Pursue what is meaningful (not what is expedient)
8.  Tell the truth, or at least don't lie
9.  Assume that the person you are listening to might know something you don't
10.  Be precise in your speech
11.  Do not bother children when they are skate boarding
12.  Pet a cat when you encounter one on the street

*   [Link](https://en.wikipedia.org/wiki/12_Rules_for_Life)

1.  "Do not carelessly denigrate social institutions or creative achievement."
2.  "Imagine who you could be and then aim single-mindedly at that."
3.  "Do not hide unwanted things in the fog."
4.  "Notice that opportunity lurks where responsibility has been abdicated."
5.  "Do not do what you hate."
6.  "Abandon ideology."
7.  "Work as hard as you possibly can on at least one thing and see what happens."
8.  "Try to make one room in your home as beautiful as possible."
9.  "If old memories still upset you, write them down carefully and completely."
10.  "Plan and work diligently to maintain the romance in your relationship."
11.  "Do not allow yourself to become resentful, deceitful, or arrogant."
12.  "Be grateful in spite of your suffering."

*   [Link](https://en.wikipedia.org/wiki/Beyond_Order)
