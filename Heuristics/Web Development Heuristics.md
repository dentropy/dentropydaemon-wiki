---
share: true
uuid: d589dc2f-d6be-4a57-9204-ab7f5dc1da09
---
[[WebDev]]
Full Stack Meteror Angular

Back End nodejs apache nginx

Front End React jquerry D3.js Babylon.js Three.js

Testing code Mocha& Chai Karma

Other PhantomJS ternjs PDF.js

Video video.js jplayer playerjs.io

Audio audio.js mb.miniAudioPlayer

Images galleria.io

Unknown Backbone Ember vue.js MobX

Links: [https://github.com/showcases/front-end-javascript-frameworks](https://github.com/showcases/front-end-javascript-frameworks) [https://www.javascripting.com/](https://www.javascripting.com/)
