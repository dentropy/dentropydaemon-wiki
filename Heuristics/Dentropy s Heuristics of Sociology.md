---
share: true
uuid: 2831accf-38a0-4cd6-a2bc-a0a6a0f5de0f
---
---
id: 1kmrdhjud6mp9qpysthuix0
title: Dentropy s Heuristics of Sociology
desc: ''
updated: 1641583846404
created: 1635210481597
---

Just spit balling here, don't take this too serious

When I hired a dating coach I was learning how to be empathetic when I realized that "Don't rape" is an Axiom for society. Don't rape is something that can be communicated and understood with little to no language.

Another axiom is "One can barter with the future by sacrificing in the present". In the seventh rule of [[dentropydaemon-wiki/Heuristics/Rules/12 Rules For Life|media.list.12 Rules For Life]] is "Pursue what is meaningful not what is expedient". In this chapter [[Relationships.People.Jordan Peterson]] starts of simple. For example, sharing one's Mammoth meat in the present with other hunters will share with you when you can't catch something to eat was a concept built into us though natural selection.
  
## Heuristics of Sociology List

1. Don't [[rape|wiki.concepts.list.rape]]
1. One can barter with the future by sacrificing in the present
1. The [[Pareto principle|wiki.concepts.list.Pareto principle]] (80/20 Rule) applies to [competence / power]
   a. There is no one that is good at absolutely everything
1. You can't get everything you want, even billionaires live unfulfilled lives
1. Suffering is part of life no matter what
   * [[Stoicisim|wiki.concepts.list.Stoicisim]]
1. Everyone is unique
1. The delusions of individuals can function as a force just as real as physics
   * Money, Ideology, Empire
1. There are known known's, known unknown's, and unknown unknown's
1. Each person has a unique tolerance for chaos and order
   * Something regarding the [[Big Five Personality Traits|wiki.concepts.list.Big Five Personality Traits]]
1. Cognitive dissonance is required make decisions (Operate within the world)
1. One person has a limited capacity for what they can do, can't do everything at once

## Links

* [[dentropydaemon-wiki/Heuristics/Axioms/20 Axioms of Sociology|wiki.heuristics.Axioms.20 Axioms of Sociology]]
