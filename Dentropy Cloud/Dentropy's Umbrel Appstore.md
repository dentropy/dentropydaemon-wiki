---
share: true
uuid: fcc49407-81d6-4576-8eeb-9a3b3c843f75
---
* [dentropy/dentropys-umbrel-appstore](https://github.com/dentropy/dentropys-umbrel-appstore)
* Apps Supported
	* [[Tiddly Wiki]]
	* [[Monica CRM]]
	* [[Wallabag]]
	* [[Wireguard]]
	* [[mediagoblin]]
	* [[minio]]

## Logs

* [[2023-04-02]]
	* 2023-04-02T16:36:33-04:00
		* So how do we create a wireguard client?
		* That also forwards traffic to another host?
		* Wait this needs to be at an OS level, I can't run it inside Umbrel that might be stupid
		* Yes we just need to connect to wireguard on boot
		* Alright are we going to try and migrate everything to my umbrel downstairs?
		* YES
		* Alright we backing the shit up and stuff 
		* Sure
		* We should test the apps I have
		* Oh yes
	* 2023-04-02T17:18:41-04:00
		* Alright what is next?
		* Hmmm why we should ummm
		* Test migrating apps between systems
		* We should also publish all the shit I should want to publish
		* So what app are we migrating?
		* Media Goblin
		* We need to write a tutorial
		* Do we also want to sync mega here
		* No definitely not
		* Alright so what is next?
		* What app manages state by default?
		* Nextcloud of course
		* Alright let's do that
		* We still need wireguard with pihole
		* What domain do we want to use?
		* Shit good point
	* 2023-04-02T17:29:43-04:00
		* [[Does linuxserver.io wireguard instance update domain of certs when env variable changes?]]
		* 