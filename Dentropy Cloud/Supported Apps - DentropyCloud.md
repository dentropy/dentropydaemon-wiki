---
share: true
uuid: 41c5d192-2968-43a1-88bf-4f913ebd58e0
---
## No Additional Configuration

* [[Audiobookshelf]]
* [[filebrowser]]
* [[mediagoblin]]
* [[Misskey]]
* [[Trilium Notes]]

## With Additional Configuration

* [[Monica CRM]]
* [[Miniflux RSS]]
* [[PeerTube]]
* [[qbittorrent]] routed through a VPN