---
share: true
uuid: a6364f21-fc05-4aad-bd59-797aa2a70af5
---
* apps
	* [[dentropydaemon-wiki/Software/List/Audiobookshelf]]
		* audiobooks.${MY_DOMAIN}
	* [[dentropydaemon-wiki/Software/List/filebrowser]]
		* filebrowser.${MY_DOMAIN}
	* [[dentropydaemon-wiki/Software/List/Jellyfin]] 
		* jellyfin.${MY_DOMAIN}
	* [[dentropydaemon-wiki/Software/List/mediagoblin]]
		* media.${MY_DOMAIN}
	* [[dentropydaemon-wiki/Software/List/Misskey]]
		* misskey.${MY_DOMAIN}
	* [[dentropydaemon-wiki/Software/List/Monica CRM]]
		* monica.${MY_DOMAIN}
	* [[dentropydaemon-wiki/Software/List/PeerTube]]
		* peertube.${MY_DOMAIN}
	* [[dentropydaemon-wiki/Software/List/Syncthing]]
		* syncthing.${MY_DOMAIN}
	* [[dentropydaemon-wiki/Software/List/Trilium Notes]] [[dentropydaemon-wiki/Software/List/Trilium Notes]]
		* trilium.${MY_DOMAIN}
	* [[dentropydaemon-wiki/Software/List/wikijs]]
		* wiki.${MY_DOMAIN}
	* [[dentropydaemon-wiki/Software/List/zincsearch]]
		* zincsearch.${MY_DOMAIN}
* apps-manual-deployment
	* [[dentropydaemon-wiki/Software/List/audioserve]]
	* [[dentropydaemon-wiki/Software/List/Elasticsearch]]
	* [[dentropydaemon-wiki/Software/List/funkwhale]]
	* [[dentropydaemon-wiki/Software/List/Miniflux RSS]]
	* [[dentropydaemon-wiki/Software/List/qbittorrent]]
* apps-networking
	* [[dentropydaemon-wiki/Software/List/nginx-proxy-manager]] 
	* [[dentropydaemon-wiki/Software/List/Portainer]]
	* [[dentropydaemon-wiki/Software/List/Traefik]]
	* [[dentropydaemon-wiki/Software/List/Wireguard]]
	* [[dentropydaemon-wiki/Software/List/yacht]]
		* yacht.${MY_DOMAIN}