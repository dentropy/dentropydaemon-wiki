---
share: true
uuid: f738f680-95a2-46e5-bb4c-57b67687e36a
---
* apps
	* [[Audiobookshelf]]
	* [[filebrowser]]
	* [[mediagoblin]]
	* [[Syncthing]]
	* [[Trilium Notes]]
	* [[wikijs]]
* apps-manual-deployment
	* [[Jellyfin]]
	* [[Monica CRM]]
	* [[qbittorrent]]
	* [[Miniflux RSS]]
	* [[Misskey]]
	* [[PeerTube]]
	* [[zincsearch]]
* apps-networking
	* [[nginx-proxy-manager]] 
	* [[Portainer]]
	* [[Traefik]]
	* [[Wireguard]]
	* [[yacht]]
* Apps to add (In Order of Priority)
	* [[ddclient]]
	* [[Searx]]
	* [[Nextcloud]] 
	* [[Photoprism]]
	* [[urbit]]
	* [[paperless-ngx]]
	* [[fed.wiki]]
	* [[Mastodon]]
	* [[Pleroma]]
	* [[Pixlefed]]
	* [[Elasticsearch]]
	* [[openobserve]]
	* [[funkwhale]]
	* [[matrix-docker-ansible-deploy]]
		* [[Element For Matrix Protocol]]
		* [[Synapse for Matrix Protocol]]
	* [[Meilisearch]]
	* [[languagetool]]
	* Something to use as Authenticaion Middleware
		* [[Authelia]] 
		* [[goauthentik]]
		* [[keycloak]]
		* Custom [[HTTP Basic Auth]] Script 
	* Something for Internal DNS
* Services to add
    *   Wallabag
    *   IPFS
        *   Put files on there
        *   Get fils from there on another computer
        *   Rehost files on another computer
        *   What libraries are fun to use
        *   Image/Picture/File Hosting Service aka IPFS server
    *   Public DNS
    *   Samba Server
    *   Torrent Client
    *   Booksonic
    *   Mastadon
    *   [Project Send](https://fleet.linuxserver.io/image?name=linuxserver/projectsend)
    *   URL shorten
    *   Selfhosted alternative to ifconfig.co and ipchicken.com
    *   Email Server
    *   Ghost Blog, that is backed up
    *   Service Dashboard / Nginx Config Manager
    *   [GitHub - mozilla-services/syncserver: Run-Your-Own Firefox Sync Server](https://github.com/mozilla-services/syncserver)
    *   [Install apache guacamole somewhere](https://guacamole.apache.org/)