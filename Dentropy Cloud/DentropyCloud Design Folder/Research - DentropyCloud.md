---
share: true
uuid: e3b6a277-00dd-43cb-ad0d-c7694e7ebd7f
---

* [[What is a Homelab|wiki.concepts.list.homelab]] Software
  * [[dentropydaemon-wiki/Software/List/Umbrel|wiki.software.List.umbrel]]
  * [[dentropydaemon-wiki/Software/List/HomelabOS|wiki.software.List.HomelabOS]]
  * [[dentropydaemon-wiki/Software/List/Unraid|wiki.software.List.Unraid]]
  * [[dentropydaemon-wiki/Software/List/TrueNAS|wiki.software.List.TrueNAS]]
    * [[dentropydaemon-wiki/Software/List/True Charts|wiki.software.List.True Charts]]
  * [[dentropydaemon-wiki/Software/List/DappNode|wiki.software.List.DappNode]]
  * [[dentropydaemon-wiki/Software/List/Portainer|wiki.software.List.Portainer]]
* Example [[What is a Homelab|wiki.concepts.list.homelab]] Projects
  * [personal-server/README.md at master · erebe/personal-server](https://github.com/erebe/personal-server/blob/master/README.md)
  * [[2020 Edition|Building a Homelab VM Server (2020 Edition|[2020 Edition|[2020 Edition|[2020 Edition|[2020 Edition|[2020 Edition|[2020 Edition|[2020 Edition|[2020 Edition|[2020 Edition) · mtlynch.io]] · mtlynch.io]] · mtlynch.io](2020%20Edition) · mtlynch.io](2020%20Edition) · mtlynch.io]]%20%C2%B7%20mtlynch.io)
  * [TerraPi modular case system for Raspberry Pi supports multiple SSD's, DIN rail, horizontal & vertical mounts - CNX Software](https://www.cnx-software.com/2020/12/19/terrapi-modular-case-for-raspberry-pi-supports-multiple-ssds-din-rail-horizontal-vertical-mounts/)
  * [Screw it, I’ll host it myself | Hacker News](https://news.ycombinator.com/item?id=26725185)
  * [My self-hosting infrastructure, fully automated | Hacker News](https://news.ycombinator.com/item?id=30030991)
  * [FunkyPenguin's geek-cookbook](https://github.com/geek-cookbook/geek-cookbook)
* Example Homelab sources
  * [awesome-selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted)
  * [r/Self-Hosted](https://old.reddit.com/r/selfhosted/)
  * [r/homelab](https://old.reddit.com/r/homelab/)
  * [Self-Hosted Show](https://selfhosted.show/) (Check the Descriptions for stuff)
  * [What are your Most Used Self Hosted Applications?](https://noted.lol/what-are-your-most-used-self-hosted-applications/)
* Other useful things to know
  * [[dentropydaemon-wiki/Wiki/Concepts/List/Bridge Networking|wiki.concepts.list.Bridge Networking]]
  * [[dentropydaemon-wiki/Software/List/vim|wiki.software.List.vim]]
  * [[dentropydaemon-wiki/Software/List/Git Research|wiki.software.List.git]]
* Homelab Guides
  * [Running Nextcloud With Docker and Traefik 2 - Chris Wiegman](https://chriswiegman.com/2020/01/running-nextcloud-with-docker-and-traefik-2/)
  * [Using Wireguard and a VPS to Bypass ISP Port Blocking and Hide Your Public IP // Selfhosted Pro](https://selfhosted.pro/hl/wireguard_vps/)
  * [Authelia is an open-source authentication/authorization server with 2FA/SSO | Hacker News](https://news.ycombinator.com/item?id=26409820)
  * [ Hacker News](https://news.ycombinator.com/item?id=27249096|Choosing a Self-Hosted Git Service (2020) | Hacker News](2020)%20)
  * [[part 1|Using Ansible & Nomad for a homelab (part 1|[part 1|[part 1|[part 1|[part 1|[part 1|[part 1|[part 1|[part 1|[part 1)]]]]](part%201)](part%201)]])
* Implemented Software
  * File Management
    * [[dentropydaemon-wiki/Software/List/Syncthing|wiki.software.list.Syncthing]]
    * [[dentropydaemon-wiki/Software/List/mediagoblin|Software.list.mediagoblin]]
    * [[dentropydaemon-wiki/Software/List/Code Server|wiki.software.List.Code Server]] (Installed separately)
  * Media Management
    * [[dentropydaemon-wiki/Software/List/Jellyfin|wiki.software.list.Jellyfin]]
    * [[dentropydaemon-wiki/Software/List/Audiobookshelf|wiki.software.list.EVM.Audiobookshelf]]
  * Knowledge Management
    * [[dentropydaemon-wiki/Software/List/wikijs|wiki.software.list.wikijs]]
    * [[dentropydaemon-wiki/Software/List/Trilium Notes|wiki.software.List.trilium]]
    * [[dentropydaemon-wiki/Software/List/Elasticsearch|wiki.software.List.Elasticsearch]]
    * [[dentropydaemon-wiki/Software/List/Monica CRM|wiki.software.List.Monica CRM]]
  * Fediverse
    * [[dentropydaemon-wiki/Software/List/Misskey|wiki.software.List.misskey]]
    * [[dentropydaemon-wiki/Software/List/funkwhale|wiki.software.List.funkwhale]]
    * [[dentropydaemon-wiki/Software/List/PeerTube|wiki.software.list.PeerTube]]
* Software to add list (In Order of Priority)
  * [fedwiki/wiki: Federated Wiki - node server as npm package](https://github.com/fedwiki/wiki)
  * [zelon88/HRConvert2: A self-hosted, drag-and-drop, & nosql file conversion server & share tool that supports 75 file formats in 13 languages.](https://github.com/zelon88/HRConvert2)
  * [[dentropydaemon-wiki/Software/List/gitea|wiki.software.List.gitea]]
  * [[dentropydaemon-wiki/Software/List/urbit|wiki.software.List.urbit]]
  * [[dentropydaemon-wiki/Software/List/keycloak|wiki.software.List.keycloak]]
  * [[Synapse / Matrix|wiki.software.List.Synapse  Matrix]]