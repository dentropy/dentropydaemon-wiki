---
share: true
uuid: b6611f4f-b019-4676-902e-8ea82840d740
---

## Videos in Chronological Order

* [[dentLog 001]]
* [[dentLog 002 The Mind Body Problem]]
* [[dentLog 003 Mapmakers]]
* [[dentLog 004 Personas]]
* [[dentLog 005 Routers, Agency, and Performing Human]]
* [[dentLog 006 What makes us Human]]
* [[dentLog 007 Setting into the Territory]]
* [[dentLog 008 The Act of Reflection]]
* [[dentLog 009 Waking Up From Denial]]
* [[dentLog 010 Provokation verses Truth]]
* [[dentLog 011 Reality The Role Playing Game]]
* [[dentLog 012 Thinking Out Loud]]
* [[dentLog 013 Engineering a Persona]]


