---
share: true
uuid: 6d0bbf21-e1ea-4a09-9597-ec479b998235
---

* [[Cringe meets theory of mind]]
* [[Learning to sail the memes]]
* [[Mapping The Human Heart]]
* [[What Humans Value]]
* [[The Human Social Interface]]
* [[Virtualizing The Self]]
* [[Guide Posts for the Human Condition]]
* [[For Manifesting Destiny]]
