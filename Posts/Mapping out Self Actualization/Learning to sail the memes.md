---
share: true
uuid: e3ed979d-7207-4dfa-806c-03aab973a4c9
---

I often find myself asking what is the self? Does the self really exist or is it an illusion? What functions transform the self against axis of time? Can you transfer the self into a computer? What does it mean that individuals can form a shared self? I ask a lot of loaded questions.

I like to think of the self as a set of heuristics animals, namely [[Homo Sapians]] use in order to survive in their environment. The self is just like a massive set of [[Peafowl]] a sexual ornament brought to us by natural selection. This sexual ornament has a bunch of interesting features such as how we have to treat delusions as fact. [[money]], [[Corporations]], [[Government]], and [[Purposes/God]] don't exist in any meaningful form outside the human mind. Homo Sapians has to treat these things as real because their interaction with other Homo Sapians depends on them. If Homo Sapians can't cooperate they die, without these heuristics in the form of delusions humanity would not be able to cooperate. It's funny how these delusions operate in their own ecosystem of natural selective pressures. [[Theory of Mind|wiki.concepts.list.Theory of Mind]] is built into the biology of Homo Sapians in order for these delusions to scale across the species.

That's cute; many people are familiar with [[sapians|wiki.media.list.sapians]] and [[the selfish gene|wiki.media.list.the selfish gene]], why does this matter in terms of the self? The self has to exist within a complex environment. The self has to listen to its environment in order to navigate it. In order to navigate its environment the self has to have wants and desires. When someone has an effective value structure in place they can listen to their environment, tune their sails to the memes, and sail in the direction of their wants. To sail the meme's one needs a map in order to know what to set their sails to. How does one generate their map? How does one identify what whey want? How is one supposed to wield the sails once they have a map? With enough heuristics for these questions one can set their sails and will the wind into existence.

Natural selection is a tinkerer not an engineer. If we could engineer ourselves we would probably function a little like the [[Obin|wiki.concepts.list.Obin]] from the [[Old Man s War|wiki.media.list.Old Man s War]] book series. Imagine having a switch to turns our conscious selves on and off. The need for suicide, drugs, and suffering would no longer exist. Just turn off the conscious self and have the mind direct what one does in the service of the state, as long as the body doesn't deteriorate too much I guess. Sadly or hopefully we don't have the capacity to engineer ourselves to that level... yet. Just imagine a chip in people's heads that would allow their bodies to be controlled via [[google, facebook, and amazon...|wiki.concepts.list.FAANG]] creepy. Funny thing is renting out one's body to the state in return for money is something many people would do voluntary. Okay next concept.

The last paragraph exists to contextualize that [[emotions|wiki.concepts.list.emotions]] are an [[axiomatic|wiki.heuristics.Axioms]] piece of the [[Human Experience|wiki.concepts.list.Human Experience]]. Our emotions give meaning to our lives. Can we empathize with someone who does not feel emotions? Imagine just executing the events of your life one after another just like a robot. It would be beautiful. Terrorists would be an actual threat. Then there are Psychopaths, people who have among other things their [[dissociation|wiki.concepts.list.dissociation]] control knob messed up sometimes go to extreme lengths in order to feel something. Hmmm seeing how people love their true crime and murder mysteries it seems like some of us may actually have a capacity to empathize with these individuals that can't feel. Hmm that's interesting, we have the capacity to empathize with psychopaths. Imagine looking at people the same way we look at cars with that sense of detachment. Emotions provide us with a value structure hierarchy for us to navigate the world.

The memetic sailboat needs a direction. It's direction is derived from our wants, desires, emotions, and values. The memetic sailboat has to navigate the waves produced by the delusions of human culture. It seems like we are missing a [[map of the human heart.|posts.Self Actualization.Mapping The Human Heart]]

## Other References

* [[Feeling is the secret|wiki.media.list.feeling is the secret]]

<!-- 
[[Personal Legend|Concepts.list.Personal Legend]]

Autistic Genuises in academia and the programmer are different

* What type of disclaimer should we put on stuff
* Just Organizers, or Participants
* How do you inform the participants
* Guppy interface is a markov blanket
* Post it myself

Do sociopaths just have their emotion's offline because their wiring is messed up causing them to constantly disassociate. 
-->
