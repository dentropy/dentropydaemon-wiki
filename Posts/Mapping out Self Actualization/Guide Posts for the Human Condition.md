---
share: true
uuid: 746f7dae-6c6d-416f-b98c-4c5e63351f71
---

---
id: 2tsCNgEJVsKAmVzDIs5Bq
title: Guide Posts for the Human Condition
desc: ''
updated: 1639982673907
created: 1639982330113
---

<!-- 
* Purpose of this post?

How to slap yourself in the face.

-->

My Zenu [[Charles Hoskinson|Relationships.People.Charles Hoskinson]] likes to make a point about guide posts. Charles of course wanted our governments to choose some guide posts in order to have some coherent [[covid policy|wiki.concepts.list.Covid.Policy]] but Guide Posts can apply to almost any thing. The guide posts I am interested in deconstructing how people expect to be treated verse how they want to be treated.