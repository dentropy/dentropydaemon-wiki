---
share: true
uuid: b8ba3c64-63f3-47bc-bf00-da6a612f7b55
---

<!-- 

The purpose of this work;

Meta psychology I can read like a concise prayer so my greatest fears do not manifest. 

-->
These days when I stare in the [[ideological mirror|wiki.concepts.list.Ideological Reflection]] I do not perceive a constructive interference pattern. I am consciously aware of my countless flaws yet I cower in fear not wanting to expose them to the conscious perception of others capable of judging.

The ego horseshoe.

List what you do not want and fear rather than what you desire.

Freedom requires restrictions.

Producing media does not matter when it is not consumed by others.

OODA

[[dentropydaemon-wiki/Heuristics/Laws/48 Laws Of Power/29/Plan All the Way to the End|wiki.heuristics.Laws.48 Laws Of Power.29.Plan All the Way to the End]]

You use your body to think, eat something, go on a walk, stretch. Your body is like the star ship enterprise.