---
share: true
uuid: c4747e80-98b8-4dca-93d9-14d4e6425e70
---

* [[Becoming A Dataist In Training]]
* [[Privacy Should You Care?]]
* [[How Does One Go About Wielding Their Own Plot Armor?]]
* [[The Daemon is Real, Now What?]]
* [[What is My Vision]]
* [[Consciousness and Parasites]]
* [[You took the Transhumanist Wager Now What?]]
* [[Blockchain as the Operating System for the Technological Singularity]]
