---
share: true
uuid: 406a13ea-5f64-440a-b454-6b43afe9e0d5
---
This is just a list of technology I think is cool and I hope to learn and use one day.

* [[Continuous Integration and Continuous Development]]
	* Gitlab CI
	* Argo CD
	* CircleCI
* [[VPN]]
	* [[Wireguard]], self hosted
	* [[Tailscale]], SAAS
	* [[sshuttle]]
	* [[Nebula]], Federated and Peer to Peer
	* [[Netmaker]]
* Security
	* [[Fail2Ban]]
* Website Analytics
	* [[matomo]]
* Self Hosted Certificate Authority
	* #TODO
* DNS Server / Proxy
	* [[Pi Hole]]
	* [[AdGuard]]
	* [[Bind9]]
* DNS Server
	* OpenDNS
* Public DNS NameServer
	* Cloud Flare
	* Vultr / Linode
* Self Hosted Zapier
* Self Hosted Airtable
* Loadbalancer, Web Server, and Reverse Proxy
	* [[Traefik]]
	* [[nginx]] and [[nginx*proxy*manager]]
* SQL Database
	* [[Postgres]]
* Graph Database
	* [[SurrealDB]]
	* [[ArangoDB]]
	* [[neo4j]]
* Semantic Search Database
	* [[Meilisearch]]
	* [[Elasticsearch]]
* Pub Sub + Job Queue
	* [[NATS]]
	* [[RabbitMQ]]
* Key Value
	* [[NATS]]
	* [[Redis]]
* Object Storage
	* Minio
	* OpenIO Object Storage
	* [[ceph]]
* Data Engineering
	* [[Apache Zeppelin]] * #TODO Research Alternative
* Data Streaming
	* [[Apache Kafka]] * #TODO Research Alternative
	* [[NATS]]
* Metrics / Status Monitoring
	* Prometheus
* Logging / Log Consolidation
	* Logstash * #TODO Find Alternative
		* [5 Awesome Logstash Alternatives: Pros & Cons [2023] * Sematext](https://sematext.com/blog/logstash*alternatives/)
	* Prometheus
	* DataDog SAAS
* Status Dashboard
	* Grafana
* Email
	* SAAS email providers
		* Sendgrid
		* Mailgun
* Authentication Middleware
	* [[goauthentik]]
	* [[keycloak]]
* Authentication Server / Identity Service / [[AAA]]
	* [[OpenLDAP]]
	* [[Active Directory]]
	* [SME Server](https://wiki.koozali.org/Main_Page)
* Testing
	* Javascript, Mocha
	* Kubernetes, #TODO
* DarkWeb (TOR/I2P) Site Proxy
	* #TODO

## Reminder for Future Me

* Reminder, Research Kubernetes Solutions for these Categories