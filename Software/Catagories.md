---
share: true
uuid: cdf9f58f-9d28-4d6e-8668-4a7f6c0b99c5
---
There are a lot of different Software Categories listed here, here is a list of some of the interesting ones to get you started

* [[dentropydaemon-wiki/Software/Catagories/KMS - Knowledge Management Systems]]
* [[dentropydaemon-wiki/Software/Catagories/Annotation Software]]
* [[dentropydaemon-wiki/Software/Catagories/Identity|wiki.software.Catagories.Identity]]
