---
share: true
uuid: 44f3c8f7-ce5d-4a19-a5a0-b6343c6ef137
---
## Disable Wayland and Enable [[x11]]

``` bash
sudo vim /etc/gdm/custom.conf    
```

``` config

# Uncomment the following
#WaylandEnable=false

```

## Sources

* [arch linux - Gnome defaults to wayland; how can I go to back to X11? - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/336219/gnome-defaults-to-wayland-how-can-i-go-to-back-to-x11)
