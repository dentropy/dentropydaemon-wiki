---
share: true
uuid: ed5a16ab-a774-4aa4-a8b9-599e49123019
---
## Install

``` bash
cd  $VAULT
cd .obsidian/plugins
git clone https://github.com/LordGrimmauld/aw-watcher-obsidian.git
cd aw-watcher-obsidian
npm install
npm run build
```

Go to `settings` -> `community plugins` and enable ActivityWatch

## Links

* [[Obsidian]]

## Source

* [LordGrimmauld/aw-watcher-obsidian: Obsidian plugin to track user activity with ActivityWatch](https://github.com/LordGrimmauld/aw-watcher-obsidian)
* [Create your first plugin | Obsidian Plugin Developer Docs](https://marcus.se.net/obsidian-plugin-docs/getting-started/create-your-first-plugin)