---
uuid: f3ba6bff-ad26-4243-9950-9249d9426446
share: false
---
* [Netmaker 0.7 - UDP Hole Punching and Kubernetes with WireGuard : WireGuard](https://old.reddit.com/r/WireGuard/comments/p2d70d/netmaker_07_udp_hole_punching_and_kubernetes_with/)
* [gravitl/netmaker: Netmaker makes networks with WireGuard. Netmaker automates fast, secure, and distributed virtual networks.](https://github.com/gravitl/netmaker)
