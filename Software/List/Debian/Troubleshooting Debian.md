---
share: true
uuid: ebb51eba-3d84-4a89-9566-72b1963e6c4a
---


## [[Keybase]]

``` bash
sudo apt purge keybase && sudo apt-get autoclean
rm -rf ~/.local/share/keybase ~/.config/keybase
```
