---
uuid: 03e5fa8e-39f5-481b-a040-178350596d13
share: false
---
[mdn/django-locallibrary-tutorial: Local Library website written in Django; example for the MDN server-side development Django module: https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django.](https://github.com/mdn/django-locallibrary-tutorial)


## Setup **RUN ONE LINE AT A TIME**
``` bash

# Create and activate Virtual Envrionment
# This helps control dependencies and with dockerization
virtualenv learning_django
cd learning_django
source bin/activate
# Install and validate dependencies within virtual environment
pip3 install django~=4.0
python3 -m django --version
# Create Django Project
django-admin startproject mysite
cd  mysite

# Test Running Server
python3 manage.py runserver
# Exit Server

# Create django module
python manage.py startapp polls

# Do database migrations so app actually works
python manage.py migrate

# Create a user for use in the /admin page
python manage.py  createsuperuser

# Run the server
python3 manage.py runserver 8000
```


## Models

* Models are your database schema and they are defined with

``` bash

# Assume polls is the name of the new model
python manage.py sqlmigrate polls 0001
# Check the migrations folder
python manage.py migrate
# Run the server
python3 manage.py runserver 8000

```

It is interesting how the models can be operated on via the shell.

It is interesting how the models can be managed from the admin interface.

## Questions

* [[How do I seed data into django models?]]
* [[How do I log all users out using django?]]
* [[How do I do a JSON REST API with django?]]
* [[How do views work in django?]]
* [[How do I access the model in django?]]
* [[How do I backup the database in django]]
* [[How do forms work in django?]]
* [[How do I upload files to django?]]
	* [[How do I add a progress bar to a file upload in django?]]

## Sources

* [Writing your first Django app, part 2 | Django documentation | Django](https://docs.djangoproject.com/en/2.1/intro/tutorial02/)