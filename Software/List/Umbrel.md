---
share: true
uuid: 60722662-eccc-443d-af35-af0ee02d1c9c
---

- Tutorials
	- [[Umbrel - Secure Install]]
	- [[Umbrel - Backup and Restore]]
	- [[Umbrel - Migrate App]]
- Links
	- [Umbrel — A personal server OS for self-hosting](https://umbrel.com/)
	- [Getting started - UmbrelInfo](https://umbrelinfo.gitlab.io/getting-started.html)
* Issues
	* Why does it not cache the images, like ever