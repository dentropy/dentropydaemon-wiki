---
share: true
uuid: d327da7e-0881-4517-8a8f-c20190efeaa4
---

[Keybase website](https://keybase.io/)  [[Keybase Binding]]

``` bash
keybase chat api -m '{"method": "send", "params": {"options": {"channel": {"name": "dentropydaemon", "members_type": "team", "topic_name": "bot-testing"}, "message": {"body": "CLI TEST?"}}}}'

keybase chat api -m '{"method": "send", "params": {"options": {"channel": {"name": "dentropydaemon", "members_type": "team", "topic_name": "bot-testing"}, "message": {"body": "Wow it worked"}}}}'
```


``` json
{
  "content":{
    "type":"text",
    "text":{
      "body":"Awesome, are you still on the education/textbook analysis trail as well?",
      "payments":null,
      "userMentions":null,
      "teamMentions":null,
      "emojis":null
    }
  }
}
```