---
uuid: afd34891-bd19-484b-a122-57b34254f6e6
share: false
---
## Logging Solutions

* [Syslog logging driver | Docker Documentation](https://docs.docker.com/config/containers/logging/syslog/)
* [Fluentd logging driver | Docker Documentation](https://docs.docker.com/config/containers/logging/fluentd/)
* [What makes Fluentd better than syslog-ng? | Hacker News](https://news.ycombinator.com/item?id=8978737)

## Links

* [Quickstart - OpenObserve Documentation](https://openobserve.ai/docs/quickstart/)
