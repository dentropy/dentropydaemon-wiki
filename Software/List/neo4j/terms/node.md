---
share: true
uuid: 7b547d68-8df1-4fd1-b67b-28c134270a62
---

Nodes describe entities (discrete objects) of a domain.
Nodes can have zero or more labels to define (classify) what kind of nodes they are.

## Links

* [[Graph]]
