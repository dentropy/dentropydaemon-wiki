---
share: true
uuid: a73228c1-fa70-4ec0-9e0e-ee251c5ce2b8
---
---
id: AeBXQyhJtFC1wPA5WGd77
title: Properties
desc: ''
updated: 1639965687337
created: 1639965640296
---

[[Nodes|wiki.software.list.neo4j.terms.node]] and [[dentropydaemon-wiki/Software 1/List/neo4j/terms/Relationships|wiki.software.list.neo4j.terms.Relationships]] can have properties (key-value pairs), which further describe them.

## Sources

* [Graph database concepts - Getting Started](https://neo4j.com/docs/getting-started/current/graphdb-concepts/)