---
share: true
uuid: 0492f7e1-1174-46ba-82ea-f3ef4d7c5421
---
``` python
from glob import glob
import os
from pprint import pprint
import os
current_path = os.getcwd()
files = glob(f"{current_path}/**", recursive=True)
pprint(files)
```

## For [[Markdown Files]]

``` python

from glob import glob
import os
from pprint import pprint
import os
current_path = os.getcwd()
files = glob(f"{current_path}/**/*.md", recursive=True)
pprint(files)

```

## Sources

* [python - Getting a list of all subdirectories in the current directory - Stack Overflow](https://stackoverflow.com/questions/973473/getting-a-list-of-all-subdirectories-in-the-current-directory)
* [https://docs.python.org/3/library/glob.html](https://docs.python.org/3/library/glob.html)
