---
share: true
uuid: 64876391-b188-4f48-9df6-ce01aa68e4ce
---
---
id: 9IhbeZxKwEf78Ca6BItKP
title: time
desc: ''
updated: 1643214031301
created: 1628449250437
---


``` python
import tzlocal, datetime
datetime.datetime.utcfromtimestamp(1416668401)
```

## Links

* [[dentropydaemon-wiki/Software/List/pandas|wiki.software.List.pandas]]
  * Has its own datetime stuff
