---
share: true
uuid: 4cf4d838-1c65-43c5-8b4f-936c99b48350
---
---
id: mNuKl1aFgq0LChPdkHaiU
title: Opensearch
desc: ''
updated: 1634180683605
created: 1634173354409
---

## Description

Opensearch is a open souce fork of [[dentropydaemon-wiki/Software/List/Elasticsearch|wiki.software.List.Elasticsearch#elasticsearch]]


## Setup

* [Docker - OpenSearch documentation](https://opensearch.org/docs/latest/opensearch/install/docker/)
  * Has useful [[dentropydaemon-wiki/Software/List/Elasticsearch|wiki.software.list.Elasticsearch.Curl#links-and-sources]] commands
* [flavienbwk/opensearch-docker-compose: Dockerized cluster architecture for OpenSearch with compose.](https://github.com/flavienbwk/opensearch-docker-compose)
  * Works
* [opensearch/docker-compose.yaml at main · amulyas/opensearch](https://github.com/amulyas/opensearch/blob/main/docker-compose.yaml)
  * Not tried

## User Management

* [[https://opensearch.org/docs/latest/security-plugin/access-control/api/#change-password]]
  * Just change password with curl, I feel stupid after learning how easy this is
* [[https://opensearch.org/docs/latest/security-plugin/configuration/yaml/#internal_usersyml]]
