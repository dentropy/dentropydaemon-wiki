---
share: true
uuid: e03181d1-9425-4466-838b-b5ee9e50b3c2
---
[scottstanfield/markdown-to-json: 🕹️ Convert YAML front-matter in Markdown files to JSON. v0.5.1 is published on NPM. Current version is v0.5.3](https://github.com/scottstanfield/markdown-to-json)


## Links

* [[nodejs]]
* [[dentropydaemon-wiki/Software/List/npm]]