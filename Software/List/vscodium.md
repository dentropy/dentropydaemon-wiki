---
share: true
uuid: 70244cd1-0256-45c2-850f-352966e10904
---

An open source version of [[dentropydaemon-wiki/Software/List/vscode|wiki.software.List.vscode]]

## #Links

[VSCodium - Open Source Binaries of VSCode](https://vscodium.com/)