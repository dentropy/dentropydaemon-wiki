---
share: true
uuid: 4b930efc-f6f6-4397-baca-4280c473976a
---
---
id: h1y3JfWfrHgCX5Ic6ReMJ
title: Apache Cassandra
desc: ''
updated: 1642461723738
created: 1642461603192
---

[[dentropydaemon-wiki/Software/List/Netflix|wiki.ddaemon.monorepo.bindings.proposed.Netflix]] uses Apache Cassandra to serve all their videos to customers.

Not good for Aggregations and Joins.
