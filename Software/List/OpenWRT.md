---
share: true
uuid: aaa6f541-e5eb-4f8c-997b-2d185fcccf06
---
---
id: n787OELyTjciHwZjn8jtq
title: OpenWRT
desc: ''
updated: 1632875087996
created: 1630780105263
---

# OpenWRT Restart Networking
[\[OpenWrt Wiki\] Network basics /etc/config/network](https://openwrt.org/docs/guide-user/base-system/basic-networking)

# OpenWRT Static Route
[\[OpenWrt Wiki\] Static routes](https://openwrt.org/docs/guide-user/network/routes_configuration)


* [OpenMPTCProuter - Internet connection bonding - Home](https://www.openmptcprouter.com/)
* [[Load balancing/failover with multiple WAN interfaces|[OpenWrt Wiki]])
