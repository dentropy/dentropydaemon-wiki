---
share: true
uuid: c151db06-defd-4c97-85c1-1103a37ba29b
---
## [Spark Tutorial For Beginners | Big Data Spark Tutorial | Apache Spark Tutorial | Simplilearn - YouTube](https://www.youtube.com/watch?v=QaoJNXW6SQo)

* Components
  * Core
  * SQL
  * Streaming
  * MLlib
  * GraphX