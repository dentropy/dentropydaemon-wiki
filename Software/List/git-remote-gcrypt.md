---
uuid: 7f22c466-275b-4dcd-a65c-47c6602335fe
share: false
---
**[git-remote-gcrypt](https://github.com/spwhitton/git-remote-gcrypt)**


``` bash
git init
git remote add cryptoremote gcrypt::git@github.com:USERNAME/REPO_NAME.git
gpg --list-keys
git config remote.cryptoremote.gcrypt-participants $KEY
git config remote.cryptoremote.gcrypt-signingkey $KEY
git add index.md
git commit -m "Plz be encrypted"
git pull cryptoremote master
git branch --set-upstream-to=cryptoremote/master master
git pull
git push -u cryptoremote master
git push --force -u cryptoremote master

# Remember to check size of files whahahahah
```


## Sources

* [encryption - Setting up an encrypted git-repository - Super User](https://superuser.com/questions/1162907/setting-up-an-encrypted-git-repository)