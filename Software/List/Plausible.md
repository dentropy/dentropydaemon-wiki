---
share: true
uuid: 44e138f7-a286-44bf-9ada-04567baba8fa
---
[Plausible Analytics | Simple, privacy-friendly Google Analytics alternative](https://plausible.io/)