---
uuid: 3af71fee-8ad7-40ae-8efd-8d18dcda1ac4
share: false
---


## Links

* [[python glob]]

## Sources

* [glob - npm](https://www.npmjs.com/package/glob)
* [joplin/updateIgnoredTypeScriptBuild.js at 771acbe463854b4977a1ad560f4a28c65fdb2fd5 · laurent22/joplin](https://github.com/laurent22/joplin/blob/771acbe463854b4977a1ad560f4a28c65fdb2fd5/Tools/gulp/tasks/updateIgnoredTypeScriptBuild.js#L8)