---
share: true
uuid: 2d8ed80e-7a4f-42db-bebc-e105943cfe63
---
---
id: avTojuIG40nJw8LkUgm6G
title: Boilerplate
desc: ''
updated: 1637598731314
created: 1637598715666
---

[[dentropydaemon-wiki/Software 1/List/nodejs/boilerplate|wiki.concepts.list.Boilerplate]]

* [danielfsousa/express-rest-boilerplate: ⌛️ Express starter for building RESTful APIs](https://github.com/danielfsousa/express-rest-boilerplate)
