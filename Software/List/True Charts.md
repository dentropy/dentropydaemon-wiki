---
share: true
uuid: 951bac20-04ca-4d27-85e2-b59e15722a81
---
---
id: as2iwtretzzs95pbwtu0v5p
title: True Charts
desc: ''
updated: 1667826732087
created: 1667826719282
---

Addon for [[dentropydaemon-wiki/Software/List/TrueNAS|wiki.software.List.TrueNAS]]

[Meet TrueCharts - the First App Store for TrueNAS SCALE | TrueCharts](https://truecharts.org/blog/Meet%20TrueCharts%20-%20the%20First%20App%20Store%20for%20TrueNAS%20SCALE/)