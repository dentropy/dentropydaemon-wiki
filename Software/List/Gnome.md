---
share: true
uuid: eb67c211-8651-42cc-b512-1ff655f7a537
---


[[dentropydaemon-wiki/Software/List/x11]]   [[Wayland]]

## Extensions

[Tiling Assistant - GNOME Shell Extensions](https://extensions.gnome.org/extension/3733/tiling-assistant/)


## Arch Stuff

[Power management - ArchWiki](https://wiki.archlinux.org/title/Power_management)

``` bash
pacman -S gnome-power-manager
```
