---
uuid: 74a6363f-f10a-414e-a1d7-c4539ec4b5d2
share: false
---
## Sources

* [sandreas/m4b-tool: m4b-tool is a command line utility to merge, split and chapterize audiobook files such as mp3, ogg, flac, m4a or m4b](https://github.com/sandreas/m4b-tool)
