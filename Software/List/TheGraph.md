---
share: true
uuid: 8d77e5a9-dcd8-4812-80b7-798826dcf337
---
---
id: Yb1GwPF6Fzxb91rZ7cFYx
title: TheGraph
desc: ''
updated: 1632168139979
created: 1630436794289
---

* [Creating subgraph on the graph - step by step | EthDump.com](https://www.ethdump.com/creating-subgraph-graph-step-step)
* [Quick Start | Graph Docs](https://thegraph.com/docs/developer/quick-start)
* [OpenZeppelin/openzeppelin-subgraphs: Subgraph schema and templates to index the activity of OpenZeppelin Contracts.](https://github.com/OpenZeppelin/openzeppelin-subgraphs)
* [ensdomains/ens-subgraph: ENS data source for The Graph](https://github.com/ensdomains/ens-subgraph)
* [TheGraph: Fixing the Web3 data querying](https://soliditydeveloper.com/thegraph)

## Research

* [The Graph To Support Optimistic Ethereum at Mainnet Launch To Help Scale Ethereum](https://thegraph.com/blog/graph-optimistic-ethereum)
* [Graph Protocol Testnet Docker Compose - The Graph Academy](https://docs.thegraph.academy/technical-documentation/testnet/guide)
