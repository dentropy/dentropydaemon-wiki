---
uuid: 1875407f-4688-4c67-abc6-0fe751e9f34c
share: false
---
[sshuttle/sshuttle: Transparent proxy server that works as a poor man's VPN. Forwards over ssh. Doesn't require admin. Works with Linux and MacOS. Supports DNS tunneling.](https://github.com/sshuttle/sshuttle)