---
uuid: d0531e52-f4bb-4dd1-ac6f-6d188a4d3be6
share: false
---
So what is SQL and why should you learn it?

SQL stands for Structured Query Language. SQL is the language used for interacting with databases. Databases are used to run the internet and the apps that run on our phones and desktops. Using SQL you can basically do everything you do with spreadsheets but scale to data orders of magnitude in size and even updated in real time.
 

