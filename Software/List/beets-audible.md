---
uuid: 533a2f18-a660-4f10-912a-543a7c36bd05
share: false
---
I use [[BadaBoomBooks]] instead unless you already use beets.

## Sources

* [Neurrone/beets-audible: Organize Your Audiobook Collection With Beets](https://github.com/Neurrone/beets-audible)