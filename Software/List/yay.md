---
share: true
uuid: 827e0f6c-eaa7-4f4a-a768-b25a29ea186c
---
[[dentropydaemon-wiki/Software/List/Arch Linux]]

## Install

``` bash
pacman -S --needed git base-devel yay
```

## Sources
* [Jguer/yay: Yet another Yogurt - An AUR Helper written in Go](https://github.com/Jguer/yay)
