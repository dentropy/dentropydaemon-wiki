---
share: true
uuid: 444ff7c7-77b4-483c-b801-3955d2daeb0a
---

* [[Calibre]] for eBooks
* [[Zotero]] for reading PDF's and Annotation
* [[Protonmail]] for email
* [[Hypothes]] and [[memex.garden]] for social annotations
* [[Raindropio]] for Bookmarks
	* Used to use [[Pinboard]]
* [[Readwise]] managing annotations
* [[git-remote-gcrypt]] for end to end encrypted git
	* Used to use [[Keybase]] RIP
* [[Signal]]  and [[Matrix]] for messaging friends and family
* [[ActivityWatch]] and [[Hishtory]] for LifeLogging
* [[PopOS]] or [[Manjaro]]  as my go to Operating System
	* [[Gnome]] as my Window Manager
	* [[codium]] as my Text Editor and IDE (Open Source [[vscode]])
		* [[VSCode Extensions]]
	* [[Obsidian]] for my Notes
		* I have also tried [[Trilium Notes]], [[OrgMode]], and [[dendron]] in the past
	* [[Terminator]] for my Terminal Emulator
	* [[zsh]] with [[oh-my-zsh]] for my shell
	* [[mega-cmd]] for File Sync and Photo Backup
	* [[Brave Browser]] as my browser of choice
		* [[Browser Extensions]]
	* [[Debian Based Fresh Install]]
	* [[Arch Linux]] Fresh Install
* [[Tailscale]] for my [[homelab]] VPN
* [[Windscribe]] for my proxy VPN
* [[TrueNAS]] Scale for my Server Operating System
	* I will likely migrate to [[Proxmox]] soon because [[helm]] on TrueNAS Scale does not work as expected
	* I ran [[Unraid]] in the past successfully
	* [[Umbrel]] for [[bitcoin]] stuff

