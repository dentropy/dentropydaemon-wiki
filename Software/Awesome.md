---
share: true
uuid: 595558e9-ad0f-420a-bf21-8d87a41c68ba
---
## [[dentropydaemon-wiki/Software/Catagories/Database Software]]
* [GitHub - chentsulin/awesome-graphql: Awesome list of GraphQL](https://github.com/chentsulin/awesome-graphql)
* [GitHub - hasura/awesome-react-graphql: A curated collection of resources, clients and tools that make working with `GraphQL and React/React Native` awesome](https://github.com/hasura/awesome-react-graphql)

## [[dentropydaemon-wiki/Wiki/Research/Decentralized Storage]]

* [GitHub - ceramicnetwork/awesome: A list of awesome projects built on Ceramic](https://github.com/ceramicnetwork/awesome)
	* [[dentropydaemon-wiki/Software/List/Ceramic]]
* [GitHub - ipfs/awesome-ipfs: Useful resources for using IPFS and building things on top of it](https://github.com/ipfs/awesome-ipfs)
	* [[IPFS]]

## [[dentropydaemon-wiki/Software/Catagories/Blockchain Software]]

* [Ceramic Network « Polygon](https://awesomepolygon.com/ceramic-network/)
	* [[dentropydaemon-wiki/Software/List/Polygon]]
* [GitHub - yjjnls/awesome-blockchain: ⚡️Curated list of resources for the development and applications of blockchain.](https://github.com/yjjnls/awesome-blockchain)
	* [[dentropydaemon-wiki/Software/Catagories/Blockchain Software]]
* [GitHub - hitripod/awesome-blockchain: Curated list of blockchain, Awesome Awesomeness](https://github.com/hitripod/awesome-blockchain)
* [[dentropydaemon-wiki/Software/List/Ethereum]]
	* [GitHub - bekatom/awesome-ethereum: Awesome Ethereum Resources](https://github.com/bekatom/awesome-ethereum)
	* [GitHub - ttumiel/Awesome-Ethereum: A Curated List of Awesome Ethereum Resources](https://github.com/ttumiel/Awesome-Ethereum)
