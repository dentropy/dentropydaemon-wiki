---
share: true
uuid: aa3fa3a8-5c37-4401-9034-219ba4930d81
---
---
id: gyROraXRM79C1ZkWMdDDw
title: images
desc: ''
updated: 1634862064668
created: 1634861951880
---

## [[dentropydaemon-wiki/Software/Catagories/CLI|wiki.software.Catagories.CLI]] stuff

* [[dentropydaemon-wiki/Software/List/imagemagick|wiki.software.list.imagemagick]]
* [[dentropydaemon-wiki/Software/List/mogrify|wiki.software.list.mogrify]]
* [[dentropydaemon-wiki/Software/List/pngcrush|wiki.software.list.pngcrush]]
* [[dentropydaemon-wiki/Software/List/jpegoptim|wiki.software.list.jpegoptim]]

[Reduce File Size of Images in Linux - CLI and GUI methods - JournalDev](https://www.journaldev.com/43692/reduce-file-size-of-images-linux)
