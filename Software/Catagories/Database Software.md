---
share: true
uuid: 17168e54-0695-461f-bc35-524a305f529f
---
## Theory

* [[dentropydaemon-wiki/Wiki/Acronyms/ACID|wiki.concepts.list.Acronyms.ACID]]

## Sub Catagories
* [[SQL Database|wiki.software.Catagories.Database.SQL]]
* [[dentropydaemon-wiki/Software/Catagories/Database/GraphQL|wiki.software.Catagories.Database.GraphQL]]
* [[dentropydaemon-wiki/Software/Catagories/Database/Graph Database Software|wiki.software.Catagories.Database.Graph]]
* [[dentropydaemon-wiki/Software/Catagories/NoSQL|wiki.software.Catagories.NoSQL]]

