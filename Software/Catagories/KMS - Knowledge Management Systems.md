---
share: true
uuid: 6aef6fe9-4c4e-4f3a-850c-e163e2303f81
---
*Sorted by my familiarity and probability I would recommend with each system*
* Unsorted
* [[Obsidian]]
	* [[CodexOS]]
	* [[Capacities]]
	* [[Milanote]]
	* [[Anytype.io]]
	* [[Tettra]]
* Self Hosted
	* [[wikijs]]
	* [[BookStack]]
	* [[Tiddly Wiki]]
	* [[Mediawiki]]
	* [[fed.wiki]]
* Free / SaaS Only
	* [[Roam Research]]
	* [[Clickup]]
	* [[Coda]]
	* [[Notion]]
	* [[Sharepoint]] / Microsoft Suite
	* [[Google Docs]] / Google Suite
	* [[Confluence]]  / [[Atlassian Suite]]
	* [[Evernote]]
	* [[Asana]]
	* [[Nuclino]]
	* [[Heptabase]]
	* [[Scrintal]]
	* [[Taskade]]
	* [[Supernotes]]
	* [[Capacities]]
	* [[Milanote]]
* Personal Knowledge Management Systems
	* [[Trilium Notes]] - Can publish Publicly
	* [[Joplin]]
	* [[PerKeep]]
* Requires [[Git Research]] or shared file system backend 
	* [[logseq 1]]
	* [MkDocs](https://www.mkdocs.org/)
	* [[dendron]]
	* [[OrgMode]]
* Ontology / Semantic Web
	* [[protege]]


## Characteristics

