---
share: true
uuid: 40dba8ff-6a4d-4f04-9ef0-003e8ac9695f
---
---
id: wjBTMkxF3gJS2HkRegGn9
title: Bookmarks
desc: ''
updated: 1630805299077
created: 1628742445320
---

## Specific Software

* [[wiki.software.List.promnesia]]
* [[dentropydaemon-wiki/Software/List/Buku|wiki.software.List.Buku]]
* [[dentropydaemon-wiki/Software/List/histre|wiki.software.list.histre]]
* [[dentropydaemon-wiki/Software/List/Pinboard|wiki.software.List.Pinboard#pinboard]]
* [[dentropydaemon-wiki/Software/List/Raindropio|wiki.software.List.Raindropio]]

## Lists of Bookmark Software

* [Alternative to Bookmark Manager](https://alternativeto.net/software/bookmark-manager/?license=opensource)
* [Reddit: Help finding a bookmark manager that is self hosted, free and open source](https://www.reddit.com/r/opensource/comments/538hpo/help_finding_a_bookmark_manager_that_is_self/)
