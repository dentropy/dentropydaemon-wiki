---
share: true
uuid: 6e70b530-f84f-435d-a647-fe85065ba418
---
[7 Best Calendar Apps for Linux Desktop in 2020](https://www.tecmint.com/best-calendar-apps-linux-desktop/)

[[dentropydaemon-wiki/Software/List/Korganizer|Software.List.Korganizer]]

### Calendars

* Nextcloud
* [[dentropydaemon-wiki/Software/List/EteSync|Software.list.EteSync]]
* [[dentropydaemon-wiki/Software/List/radicale|Software.List.radicale]]