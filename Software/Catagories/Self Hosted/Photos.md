---
share: true
uuid: a06baa72-6906-48b7-8f3d-c8cbb54e5b48
---
---
id: DpIWm1FJ4B0lOFFVk7oaS
title: Photos
desc: ''
updated: 1629204280132
created: 1629073732029
---

## List of Software

* [[dentropydaemon-wiki/Software/List/Photonix|wiki.software.List.Photonix]]
* [[dentropydaemon-wiki/Software/List/Photoprism|wiki.software.List.Photoprism]]
* [[dentropydaemon-wiki/Software/List/photoview|wiki.software.List.photoview]]
* [[dentropydaemon-wiki/Software/List/Pixlefed|wiki.software.List.Pixlefed]]
* [[dentropydaemon-wiki/Software/List/Piwigo|wiki.software.List.Piwigo]]
* [[Nextcloud photos|wiki.software.list.Nextcloud.photos]]
* [[dentropydaemon-wiki/Software/List/Damselfly|wiki.software.List.Damselfly]]
* [[dentropydaemon-wiki/Software/List/pigallary2|wiki.software.List.pigallary2]]

## Ideal Features

* Organizing
  * Tagging
  * Sharable Albums
  * File System View
* View map of geo tagged photos
* AI Model that can search photos
* Sync photos from phone
* Script ability / API
* Migratable / Exportable
* Web clipper

## Tools

* [[dentropydaemon-wiki/Software/List/gphotos-sync|wiki.software.List.gphotos-sync]]
* [[dentropydaemon-wiki/Software/List/gphotos-cdp|wiki.software.List.gphotos-cdp]]

## Lists of self hosted apps

* [[https://github.com/awesome-selfhosted/awesome-selfhosted#photo-and-video-galleries]]
* [Ask HN: Alternatives to Google Photos? | Hacker News](https://news.ycombinator.com/item?id=27338008)

## Research on Photo Apps

* [Base64.ai – Extract text, data, photos and more from all types of docs | Hacker News](https://news.ycombinator.com/item?id=26085538)
* [13 Free Alternatives Image and Photo Organizer For Microsoft Windows 10](https://www.geckoandfly.com/2306/alternative-photo-organizer-acdsee-10-photo-manager/)
