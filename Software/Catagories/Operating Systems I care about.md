---
share: true
uuid: 0cfccaf0-dc64-4d53-9c8c-d7004b2d5814
---
* Operating Systems Compatible with [[dentropydaemon-wiki/Software/List/Ventoy]]
	* [[dentropydaemon-wiki/Software/List/Manjaro]] [[dentropydaemon-wiki/Software/List/Gnome]] - Default for Desktop
	* [[dentropydaemon-wiki/Software/List/Void Linux]]
	* [[dentropydaemon-wiki/Software/List/Debian]] - Default for Server
	* [[dentropydaemon-wiki/Software/List/PopOS]]
	* [[dentropydaemon-wiki/Software/List/Ubuntu]] #Server
	* [[dentropydaemon-wiki/Software/List/Fedora]] + #Server
	* [[dentropydaemon-wiki/Software/List/TrueNAS Scale]] - Have deployed on my actual NAS
	* [[dentropydaemon-wiki/Software/List/Kali Linux]]
	* [[dentropydaemon-wiki/Software/List/Windows 10]] - Default for Gaming
	* [[dentropydaemon-wiki/Software/List/Qubes OS]] 
	* [[dentropydaemon-wiki/Software/List/Parrot OS]]
* Operating Systems Not compatible with [[dentropydaemon-wiki/Software/List/Ventoy]]
	* [[dentropydaemon-wiki/Software/List/Whonix]] - Not available as ISO... yet, Runs in VM, Separate USB, use Qubes
	* [[dentropydaemon-wiki/Software/List/Unraid]] - Runs off the USB by default