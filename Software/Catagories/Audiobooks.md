---
share: true
uuid: cf293569-0322-486e-9649-6e1b4b4ce9fa
---
# Audiobooks Software

* [[audioserve]]
* [[Cozy]]
* [[Voice Audiobook Player]]
* [[BadaBoomBooks]]
* [[m4b-tool]]
* [[beets-audible]]


## Sources

* [audio - Audiobook player for Linux - Software Recommendations Stack Exchange](https://softwarerecs.stackexchange.com/questions/4057/audiobook-player-for-linux)
* [audiobooks · GitHub Topics](https://github.com/topics/audiobooks)
* [seanap/Plex-Audiobook-Guide: A walkthrough for optimal Audiobook experience using Plex](https://github.com/seanap/Plex-Audiobook-Guide)

## TODO

* [audio - Audiobook player for Linux - Software Recommendations Stack Exchange](https://softwarerecs.stackexchange.com/questions/4057/audiobook-player-for-linux)
* [izderadicka/audioserve: Simple personal server to serve audiofiles files from folders. Intended primarily for audio books, but anything with decent folder structure will do.](https://github.com/izderadicka/audioserve)
* [audiobooks · GitHub Topics](https://github.com/topics/audiobooks)
