---
share: true
uuid: 304bae5f-df49-479f-bd2b-04331b9843aa
---
## List All Programming Languages

```dataview
LIST
FROM [[.md|Language - Programming]]
WHERE file.path = this or "dentropydaemon-wiki/Software/List"
```

