---
share: true
uuid: b09af5f6-0f41-432f-a5eb-1eb909da4ade
---
---
id: Dl6vTqZxTezwnzCdqDFJF
title: KMS - Knowledge Management Systems
desc: ''
updated: 1636855498131
created: 1628770985915
---

Wiki's are tools used by groups of people to collaboratively edit publications. When reasonable people work together they usually come to common solutions. This does not seem to be the case when it comes to wiki's with there existing at least a dozen competing tools.

* [[Office 365|wiki.software.List.O365]]
* [[dentropydaemon-wiki/Software/List/GSuite|wiki.software.List.GSuite]]
* [[dentropydaemon-wiki/Software/List/Obsidian|wiki.software.List.Obsidian]]
* [[wiki|wiki.software.List.wiki.media.wiki]]
* [[dentropydaemon-wiki/Software/List/Roam Research|wiki.software.List.Roam Research]]
* [[dentropydaemon-wiki/Software/List/Tiddly Wiki|wiki.software.List.Tiddly Wiki]]
* [[dentropydaemon-wiki/Software/List/dendron|wiki.software.List.dendron]]
  * The one I currently use
* [[dentropydaemon-wiki/Software/List/Trilium Notes|wiki.software.List.trilium]]
  * Perfect for note taking but not annotating web pages and books
  * Third choice
* [[dentropydaemon-wiki/Software/List/Joplin|wiki.software.List.Joplin]]
  * Perfect for not taking but not annotating web pages and books
  * Not available from the browser
* [[dentropydaemon-wiki/Software/List/One Note|wiki.software.List.One Note]]
  * Worry about data export, not local first
* [[dentropydaemon-wiki/Software/List/Notion|wiki.software.List.Notion]]
  * Worry about data export, not local first
* [[dentropydaemon-wiki/Software/List/Evernote|wiki.software.list.Evernote]]
* [[dentropydaemon-wiki/Software/List/OrgMode|wiki.software.List.OrgMode#orgmode]]
* [[dentropydaemon-wiki/Software/List/wikijs|wiki.software.list.wikijs]]
* [[dentropydaemon-wiki/Software/List/Athens|wiki.software.list.Athens]]
* [[dentropydaemon-wiki/Software/List/neuracache|wiki.software.List.neuracache]]
* [[dentropydaemon-wiki/Software/List/SuperMemo|wiki.software.List.SuperMemo]]

## Other Tools

* [OntoWiki](http://ontowiki.net/)
* [dokuwiki](https://www.dokuwiki.org/dokuwiki)

## Lists of other tools

* [MediaWiki Alternativest](https://alternativeto.net/software/mediawiki/)
* [Evernote Alternatives](https://alternativeto.net/software/evernote/)

## Research on Knowledge Management Systems

* [[dentropydaemon-wiki/Media/List/Knowledge Management Archipelago|media.list.Knowledge Management Archipelago]]
* [[https://docs.google.com/document/d/1R5Kvbz_hu6fS15xSq4tGWpEAplJ9bRgtytqIiA_lz-g/edit#heading=h.18j5z1lok8wc]]