---
share: true
uuid: ba705a2b-cd36-41c4-afaa-4668998f17e5
---
## Reminders

* [[dentropydaemon-wiki/Software/Catagories/Database/SQL/SQL Examples]]

## List of SQL Databases

* [[dentropydaemon-wiki/Software/List/sqlite|wiki.software.List.sqlite]]
* [[dentropydaemon-wiki/Software/List/Postgres|wiki.software.List.postgres]]
* [[dentropydaemon-wiki/Software/List/mysql|wiki.software.List.mysql]]
* [[dentropydaemon-wiki/Software/List/MariaDB|wiki.software.List.MariaDB]]

## Links

* [[Data Types|wiki.software.List.postgres#data-types]]

## Questions

* What is a parameterized cursor
