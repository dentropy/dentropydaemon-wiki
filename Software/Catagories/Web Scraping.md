---
share: true
uuid: a4d5154b-6474-4bb6-8a82-ed04bfc722ab
---
## Web Scraping Tools
* Dynamic Sites
	* [[Selenium]]
	* [[nodejs]]
		* [[Puppeteer]]
* Scraping Static Sites
	* [[curl]]
	* [[wget]]
	* [[Python]]
		* [[dentropydaemon-wiki/Software/List/python/requests]]
		* [[pywebcopy]]
* Parsing content
	* [[Beautiful Soup]]
	* [pyquery · PyPI](https://pypi.org/project/pyquery/)
	* [[sqlite]] HTML extension
## Links

* [[Web Scraping Browser Extensions]]

## Sources

* [Top 5 Open Source Web Scraping Frameworks and Libraries - Datahut](https://www.blog.datahut.co/post/top-5-open-source-web-scraping-frameworks-and-libraries)