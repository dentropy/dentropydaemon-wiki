---
share: true
uuid: 3a6e70f4-6e28-4b3b-8bbc-c28afe14ed6e
---
---
id: G0sQIyleULB9gJ8Uh9aTT
title: PDF Annotation
desc: ''
updated: 1630804526685
created: 1630005212069
---

## General PDF Annotation Tools

* [[Okular|wiki.software.List.Okular PDF]]
* [[dentropydaemon-wiki/Software/List/Polar|wiki.software.List.Polar]]

## Only web PDF's

* [[dentropydaemon-wiki/Software/List/Hypothes|wiki.software.List.Hypothes]]
* [[memex.garden|wiki.software.List.Memex]]
* [[dentropydaemon-wiki/Software/List/NotesAlong|wiki.software.List.NotesAlong]]
