---
share: true
uuid: 512bd027-a589-4091-81c2-86d918b75bae
---
---
id: DrggAJejfHDJEr1LiL8uI
title: EPUB Annotation
desc: ''
updated: 1630006028626
created: 1630005398288
---

## Supported Software

* [[dentropydaemon-wiki/Software/List/Calibre|wiki.software.List.Calibre]]
* [[dentropydaemon-wiki/Software/List/Polar|wiki.software.List.Polar]]

# Semi Supported Software

* [[dentropydaemon-wiki/Software/List/Hypothes|wiki.software.List.Hypothes]] is semi supported because it has an API for annotating EPUB's on websites but not raw epubs themselves.
  * [A Powerful Partnership Brings Open Annotation to EPUBs : Hypothesis](https://web.hypothes.is/blog/epub-annotation/)
