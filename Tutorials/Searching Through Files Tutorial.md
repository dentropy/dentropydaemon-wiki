---
share: true
uuid: 21c0a45b-3f65-446a-bcae-89242cc63469
---

## Remember [[Logs - Searching Through Files Tutorial Research]]

Earlier today I realised that [[vscode]] will only search through the contents of files I have open not all the files I have in the workspace, so I set out to resolve this issue.

[[ripgrep]] is the best tool for the job and turns out [[grep#Searching Directories Recursively|grep]] with the right flags works pretty well too. 

I want to be able to search for the same phrases being used in different files. This requires some text processing. Time to learn some [NLP](https://www.nlpdemystified.org/course/introduction).

I want to be able to get all domain names mentioned in a folder of markdown files.

I also want to be able to parse markdown files.

I want to extract all names mentioned in markdown files.

I want to extract all questions mentioned in markdown files.

I want to be able to easily parse and search PDF,DOCX and HTML files.

To be Continued....

## Links
* [[Logs - Searching Through Files Tutorial Research]]