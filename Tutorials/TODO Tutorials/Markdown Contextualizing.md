---
share: true
uuid: 2f0c38e1-054c-42a8-bd2c-0cb1733af116
---
[[What is the best Markdown parser?]]

## Requirements

* [[nodejs]]
* [[npm]]

## Steps

``` js

import fs from 'node:fs/promises'
import {fromMarkdown} from 'mdast-util-from-markdown'
import {toMarkdown} from 'mdast-util-to-markdown'
import {frontmatter} from 'micromark-extension-frontmatter'
import {frontmatterFromMarkdown, frontmatterToMarkdown} from 'mdast-util-frontmatter'
import util from "util";


const doc = await fs.readFile('./dentropydaemon-wiki/Wiki/Software/List/docker.md')
console.log(doc.toString())
const tree = fromMarkdown(doc, {
  extensions: [frontmatter(['yaml', 'toml'])],
  mdastExtensions: [frontmatterFromMarkdown(['yaml', 'toml'])]
})

// console.log(tree)
console.log(util.inspect(tree, {showHidden: false, depth: null, colors: true}))

```

``` sql
SELECT * FROM pkm;

SELECT DISTINCT json_extract(markdown_syntax_tree , '$.type') AS key
FROM pkm;

SELECT DISTINCT json_extract(markdown_syntax_tree , '$.children') AS key
FROM pkm;


SELECT DISTINCT json_extract(markdown_syntax_tree , '$.children') AS key
FROM (SELECT * FROM pkm LIMIT 1);

SELECT DISTINCT json_extract(markdown_syntax_tree , '$.children') AS key
FROM (SELECT * FROM pkm LIMIT 1 OFFSET 1);


SELECT DISTINCT json_extract(markdown_syntax_tree , '$.children') AS key
FROM (SELECT * FROM pkm LIMIT 1 OFFSET 1);

SELECT DISTINCT json_extract(markdown_syntax_tree , '$.children[0].children') AS key
FROM (SELECT * FROM pkm LIMIT 1 OFFSET 1);

-- We need to take the first child element
-- Loop through it's list of child element 
-- If I can loop through all these and put them in a table that would be great


-- Here we have the list expanded out
SELECT * FROM
json_each(
	(json_extract((SELECT markdown_syntax_tree FROM pkm LIMIT 1 OFFSET 1) , '$.children[0].children'))
);


SELECT * FROM
json_each(
	(json_extract((SELECT markdown_syntax_tree FROM pkm LIMIT 1 OFFSET 1) , '$'))
);

-- So I need to check the key then make a list then check the key
-- The inside query checks the key, that is surrounded by the query that makes the list, then recursive
-- We already check the key then check the list

SELECT * FROM
json_each(
	(json_extract((SELECT markdown_syntax_tree FROM pkm LIMIT 1 OFFSET 1) , '$.children'))
);

SELECT * FROM
json_each(
	(json_extract((SELECT markdown_syntax_tree FROM pkm LIMIT 1 OFFSET 1) , '$.children[0].children'))
);

SELECT * FROM
json_each(
	(json_extract((SELECT markdown_syntax_tree FROM pkm LIMIT 1 OFFSET 1) , '$.children[0].children[0].children'))
);

SELECT * FROM
json_each(
	(json_extract((SELECT markdown_syntax_tree FROM pkm LIMIT 1 OFFSET 1) , '$.children[0].children[0].children[0].children'))
);

-- Now how do we put this together
-- Well remember we are transorming tables
-- What is our output table
-- We need to check if the children element is present, if it is we do something
-- But we do not have if and else
-- But we do have where
-- But how do we start off the query
-- That's a good point
-- Hmmm
-- We create a huge table then save all the objects that do not have the children key
-- So we are going to save the key's to all these objects recursively
-- Wait don't we have json_tree as a thing
-- json_tree needs to be loaded


SELECT * FROM
json_tree  ((SELECT markdown_syntax_tree FROM pkm LIMIT 1 OFFSET 1));


--------------------------

SELECT * FROM
pkm, json_tree(pkm.markdown_syntax_tree);

SELECT * FROM
pkm, json_tree(pkm.markdown_syntax_tree)
WHERE key = 'url';

SELECT file_name, value FROM
pkm, json_tree(pkm.markdown_syntax_tree)
WHERE key = 'url';

SELECT file_name, value FROM
pkm, json_tree(pkm.markdown_syntax_tree)
WHERE key = 'permalink';

```

## Links

* [[What is the best Markdown parser?]]

## Logs

* 2023-04-13T00:40:54-04:00
	* Cool we got it, we have all the links and the files they come from
	* Wow that was easier than expected in the end, no recursion required
	* We still need to parse the URL's
	* Also can we extract the double bracket obsidian links?
* 2023-04-12T23:47:18-04:00
	* I have been trying to extract all the URL's in native SQL by navigating the markdown_syntax_tree
	* Well I can just loop through the stuff in javascript
	* But this is more pure
	* This is real ETL
	* So how do we extract the list from JSON
	* How do we find the keys from that list then start again
	* Good you finally broke down the problem
	* FLATTEN LIST
	* SELECT KEY
	* We can already select key
	* Now flatten list
* 2023-04-12T22:18:48-04:00
	* So our new markdown parser basically create's a markdown rather than HTML DOM, and sqlite can not recursively parse it, or can it?
	* Well I can read the entire DOM into python, look through the object recursively, then extract all the links
	* I guess that is ETL
	* But I want to do it all in JSON, raw actual JSON
	* Find all objects with matching key value pair in JSON using sqlite
	* It looks like we can do this all in SQLITE
		* [Search JSON with SQLite - A ShareGPT conversation](https://sharegpt.com/c/DGAXiyy)
	* Alright so I guess let's read all the files and parse them
	* How do we want to parse the paths
	* No we have column that is, file_name, file_path, raw_file_contents, markdown_syntax_tree
	* Ya I guess that's all we need right
	* Ya I guess it is
	* We need glob!?!?
	* Yes that would be nice
	* Do we grab an ORM?
	* That will make things WAAAY more complicated, we have a single table
	* But postgres
	* Dude just use a converter at this point, THIS IS A SINGLE TABLE
	* 
* 2023-04-12T21:54:53-04:00
	* Alright with some help from ChatGPT we found a better markdown parser that is so dam powerful and important it has extensions
	* So what is our goal here again?
	* We want every URL from the entire PKM
	* We are going to need the link and frontmatter extensions
* 2023-04-12T21:38:30-04:00
	* I would like a python3 package instead that is maintained and can actually be installed
	* [Best Markdown to JSON Parser. - A ShareGPT conversation](https://sharegpt.com/c/VFfk0h8)
* 2023-04-12T21:28:46-04:00
	* What are the goals, objectives, and constraings of this project
	* I think it would be cool to parse all my markdown files into a sqlite database then query them
	* Alright that's the goal and should be pretty easy, but it is lacking a purpose
	* How do I log my work on all these projects differently?
	* Hmmm good question
	* I guess we can just have one huge log file, or have tones of different log files like we wanted to do with Tiddlywiki
	* Alright let's load everything into sqlite