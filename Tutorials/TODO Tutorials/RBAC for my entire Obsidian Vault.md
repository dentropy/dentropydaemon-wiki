---
share: true
uuid: 1f1f914c-8e6e-48b7-9068-bc7b290b6b64
---
* Then it might make more sense to use [[JuiceFS]] and mount the S3 Object storage
	* But then how are you supposed to do local editing
* Describe the problems with using git with Obsidian Vault
* Why can't you just use Dropbox, Mega, or other cloud file sync
* What 