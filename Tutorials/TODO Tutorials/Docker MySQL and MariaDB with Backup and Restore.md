---
share: true
uuid: 065d3a41-1745-4c38-8950-4939f0d0b606
---
* [[MySQL]] + [[MariaDB]]
	* [Backup and restore a mysql database from a running Docker mysql container](https://gist.github.com/spalladino/6d981f7b33f6e0afe6bb)
