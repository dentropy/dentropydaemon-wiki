---
share: true
uuid: 31f7e81a-967e-41f4-872e-91d1571df726
---
* [[Encrypted Git Backup]]
* [[Django]]
* Write Tutorials / Skills to Learn
	* [[SQL Tutorial]]
	* [[Mastering Docker]]
	* [[Markdown Contextualizing]]
	* [[Web Scraping]]
	* Data Engineering
		* [[Mastering SQL]]
		* Hamilton Dag
		* Airflow
		* Spark
	* Privacy
		* PGP
		* Local TOR Proxy
		* Tor Hidden Servies
	* DevOps
		* [[SSH]]
			* [[Fail2Ban]]
		* Object Storage
		* Teraform
		* Emails
			* [Postmark: Fast, Reliable Email Delivery Service | SMTP | API](https://postmarkapp.com/)
		* Cloud-Init + UserData
		* Prometheus
		* Grafana
		* Gitlab CI
	* [[dentropydaemon-wiki/Skills/Learning Pathways/Programming/Unit Testing]]
		* Javascript - ViteTest
		* Python
	* key logger
	* PubSub + Key Value Store + Data Streaming
		* Kafka / Nats / RabbitMQ / Spark Streaming
	* Come up with better way to store API keys's from stuff
* Robotics Project

## Tutorials

* Write Tutorials / Skills to Learn
	* Forward docker traffic through VPN
	* SQL Database Deployment, Cheat Sheet, Backup and Restore
	* PKM Enhancement
		* Parsing Markdown
	* Web Scraping
		* Python Requests
		* Selenium
	* Data Engineering
		* SQLAlchemy
		* Hamilton Dag
		* Airflow
		* Spark
	* Privacy
		* PGP
		* Local TOR Proxy
		* Tor Hidden Servies
	* DevOps
		* SSH
		* Deploy and secure Postgres
		* Object Storage
		* Teraform
		* Emails
			* [Postmark: Fast, Reliable Email Delivery Service | SMTP | API](https://postmarkapp.com/)
		* Docker
			* docker-compose
			* dockerfile
			* advanced networking
			* DNS and Docker
		* Cloud-Init + UserData
		* Prometheus
		* Grafana
		* Gitlab CI
	* [[dentropydaemon-wiki/Skills/Learning Pathways/Programming/Unit Testing]]
		* Javascript - ViteTest
		* Python
	* key logger
	* PubSub + Key Value Store + Data Streaming
		* Kafka / Nats / RabbitMQ / Spark Streaming
	* Come up with better way to store API keys's from stuff
* Get my ebike up and running.
* Robotics Project