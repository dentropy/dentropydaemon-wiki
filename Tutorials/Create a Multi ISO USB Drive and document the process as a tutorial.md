---
share: true
uuid: 69a15b0e-608e-4f59-ba71-a4b159ca12a0
---

![[dentropydaemon-wiki/Software/List/Ventoy#Tutorial for Linux]]

## Research

* [How To Create A Multi OS Bootable USB Drive - YouTube](https://www.youtube.com/watch?v=Xf2Qdpv6P20)
	* Written Tutorial, [√ How to create a all-in-one bootable USB drive - multiboot USB drive - kilObit](https://kil0bit.blogspot.com/2022/05/how-to-create-all-in-one-bootable-usb.html)
	* Software, [[dentropydaemon-wiki/Software/List/Ventoy]]
* [One Thumb Drive For Multiple ISOs - YouTube](https://www.youtube.com/watch?v=3isvLpUic-c)
	* Software [[dentropydaemon-wiki/Software/List/Ventoy]]