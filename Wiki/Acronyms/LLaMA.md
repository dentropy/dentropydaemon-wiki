---
uuid: a722edb6-d8e9-4b1c-9eed-6a8c84e1553e
share: false
---
**Large Language Model Meta AI**


## Sources

* [LLaMA - Wikipedia](https://en.wikipedia.org/wiki/LLaMA)
* [Introducing LLaMA: A foundational, 65-billion-parameter language model](https://ai.facebook.com/blog/large-language-model-llama-meta-ai/)
