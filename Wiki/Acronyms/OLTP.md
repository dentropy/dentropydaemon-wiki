---
share: true
uuid: bd1fc221-f8e8-40f0-b0f3-c3b5a3e145f3
---
---
id: OLHTdv3iYkO8YuRkmf7Ll
title: OLTP
desc: ''
updated: 1642601277106
created: 1642601135287
---

Online transaction processing

## Links

* [[dentropydaemon-wiki/Wiki/Concepts/List/OLAP vs OLTP|wiki.concepts.list.OLAP vs OLTP]]

## Sources

[Online transaction processing - Wikipedia](https://en.wikipedia.org/wiki/Online_transaction_processing)