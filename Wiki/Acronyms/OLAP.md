---
share: true
uuid: 515a019e-5333-4996-956c-d4661cd215c8
---
---
id: Jir5zARoveIwjPrUUBND3
title: OLAP
desc: ''
updated: 1644413859392
created: 1642601082534
---

Online analytical processing

## Links

* [[dentropydaemon-wiki/Wiki/Concepts/List/OLAP Cubes|Concepts.list.OLAP Cubes]]
* [[dentropydaemon-wiki/Wiki/Concepts/List/OLAP vs OLTP|wiki.concepts.list.OLAP vs OLTP]]

## Sources

* [Online analytical processing - Wikipedia](https://en.wikipedia.org/wiki/Online_analytical_processing)