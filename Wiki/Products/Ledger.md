---
share: true
uuid: f66a2618-116c-408b-80dc-8a5be57bf593
---

## Links

[[.md|swarmio.Swarmio Research.Swarmio Product Research.Ledger]]

## Sources

* [Hardware Wallet - State-of-the-art security for crypto assets | Ledger](https://www.ledger.com/)