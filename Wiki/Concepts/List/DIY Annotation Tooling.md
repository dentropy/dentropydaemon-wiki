---
share: true
uuid: 6cd3ff9d-b7f2-4292-a631-c07b7a9f5920
---
* HTML
	* [funktechno/texthighlighter: a no dependency typescript npm package for highlighting user selected text](https://github.com/funktechno/texthighlighter)
* PDF
	* [highkite/pdfAnnotate: Javascript library for creating annotations in PDF documents](https://github.com/highkite/pdfAnnotate)
* Videos + Youtube
	* [[memex.garden]]
* Images
	* [adityasuman2025/MNgoImageAnnotate: A JavaScript React Library (npm package) which provides an area over an image to annotation/markup/write. One can easily annotate over image in react.js by installing react-image-annotate-mngo package](https://github.com/adityasuman2025/MNgoImageAnnotate)

## Links

* [[Annotation Software]]