---
share: true
uuid: 98bd1412-f674-4a4b-bb84-5bfc1b26b3c3
---
---
id: m2iKUi0giPp0roug0GTX1
title: Big Five Personality Traits
desc: ''
updated: 1635126090416
created: 1634413548036
---

Personality is how one thinks, feels, and behaves. The schema for personality are listed below.

* [[dentropydaemon-wiki/Wiki/Concepts/List/Big Five Personality Traits/agreeableness|wiki.concepts.list.Big Five Personality Traits.agreeableness]]
* [[dentropydaemon-wiki/Wiki/Concepts/List/Big Five Personality Traits/conscientiousness|wiki.concepts.list.Big Five Personality Traits.conscientiousness]]
* [[dentropydaemon-wiki/Wiki/Concepts/List/Big Five Personality Traits/extraversion|wiki.concepts.list.Big Five Personality Traits.extraversion]]
* [[dentropydaemon-wiki/Wiki/Concepts/List/Big Five Personality Traits/neuroticism|wiki.concepts.list.Big Five Personality Traits.neuroticism]]
* [[dentropydaemon-wiki/Wiki/Concepts/List/Big Five Personality Traits/Personality Trait Openness|wiki.concepts.list.Big Five Personality Traits.openness]]

![[/assets/images/2021-10-16-16-03-09.png|Big 5 Examples]]

## [[dentropydaemon-wiki/Wiki/Concepts/List/mnemonic|wiki.concepts.list.mnemonic]]

OCEAN (openness, conscientiousness, extraversion, agreeableness, and neuroticism)
CANOE (for conscientiousness, agreeableness, neuroticism, openness, and extraversion)

## Sources

* [What Are the Big 5 Personality Traits?](https://www.verywellmind.com/the-big-five-personality-dimensions-2795422)
