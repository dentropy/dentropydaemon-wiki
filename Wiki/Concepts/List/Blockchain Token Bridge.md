---
share: true
uuid: fa7d08c2-5e1e-49fa-93c2-05449eb4ac4a
---
[[dentropydaemon-wiki/Software/List/Solidity]] #blockchain 

* [ERC721-based NFT Transfer Example - TokenBridge](https://docs.tokenbridge.net/eth-xdai-amb-bridge/nft-omnibridge-extension/nft-transfer-example)
  * [[dentropydaemon-wiki/Software/List/Solidity|wiki.software.Programming Language.Solidity]]
