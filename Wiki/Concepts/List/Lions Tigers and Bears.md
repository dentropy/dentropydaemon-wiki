---
share: true
uuid: a732396b-42e4-469d-9734-cec11a9c5949
---
---
id: k85rCxG2SPV4VDwqmDRPX
title: Lions Tigers and Bears
desc: ''
updated: 1642220151396
created: 1642219717043
---

This theme of the Lions Tigers and Bears and how it relates to Aliens is really fascinating.

## Links

* [[dentropydaemon-wiki/Media/List/Childhood's End|media.list.Childhood's End]]
* [[dentropydaemon-wiki/Media/List/Hyperion|wiki.media.list.Hyperion]]
* [[dentropydaemon-wiki/Media/List/The Wizard of Oz|media.list.The Wizard of Oz]]

## Sources

* [Lions and Tigers and Bears | Hyperion Cantos Wiki | Fandom](https://hyperioncantos.fandom.com/wiki/Lions_and_Tigers_and_Bears)