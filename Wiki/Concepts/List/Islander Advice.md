---
uuid: 2ea39fe9-1bc8-4f54-8592-51dfc1875691
share: false
---
* Befriend new members, moderators, and active members
* Document organization scope and project descriptions (max 500 characters)
* Setup personal account
* Setup organization account
* Test transaction and key creation and message
* Setup org hierarchy
* Publish org hierarchy
* Publish public keybase and pgp signed message