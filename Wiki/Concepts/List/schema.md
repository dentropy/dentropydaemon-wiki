---
share: true
uuid: 83aca571-097c-4635-9973-e0fea44806d5
---
---
id: mm7DZ6Cgbw5jyu1KNCvCW
title: Schema
desc: ''
updated: 1643820681972
created: 1639601518592
---

## SQL Examples

* [6 Database Schema Examples and How to Use Them | Integrate.io](https://www.integrate.io/blog/database-schema-examples/)

* [Creating Entity Relationship Diagrams using Draw.io - YouTube](https://www.youtube.com/watch?v=lAtCySGDD48)

![[/assets/images/2022-02-02-11-51-20.png]]
