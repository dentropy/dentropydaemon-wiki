---
uuid: 6a0d37b9-bf8d-44a1-b236-4c16af4e1924
share: false
---
In short, yield farming protocols incentive [[liquidity provider]] to stake or lock up their crypto assets in a smart contract-based liquidity pool. These incentives can be a percentage of transaction fees, interest from lenders or a governance token. These returns are expressed as an annual percentage yield (APY). As more investors add funds to the related liquidity pool, the value of the issued returns decrease accordingly. 

Liquidity mining occurs when a yield farming participant earns token rewards as additional compensation, and came to prominence after Compound started issuing the skyrocketing COMP, its governance token, to its platform users. 

Most yield farming protocols now reward liquidity providers with governance tokens, which can usually be traded on both centralised exchanges like Binance and decentralised exchanges such as Uniswap. 

## Sources

* [What Is Yield Farming? | Alexandria](https://coinmarketcap.com/alexandria/article/what-is-yield-farming)
