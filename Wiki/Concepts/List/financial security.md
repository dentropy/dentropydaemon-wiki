---
uuid: 9b88c3e6-a5cb-4d2d-8915-2360d8badb4b
share: false
---
## What Is a Security?

The term "security" refers to a fungible, negotiable financial instrument that holds some type of monetary value. It represents an ownership position in a publicly-traded corporation via stock; a creditor relationship with a governmental body or a corporation represented by owning that entity's bond; or rights to ownership as represented by an [[financial call option|swarmio.Swarmio Research.concepts.financial call option]].

## Sources:

* [Security Definition: How Securities Trading Works](https://www.investopedia.com/terms/s/security.asp)
