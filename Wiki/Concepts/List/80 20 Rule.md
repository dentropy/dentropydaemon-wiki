---
share: true
uuid: 4eb7dd15-c29a-4378-875e-c17b424c507a
---
---
id: LaEO9wQC8LXmprighVT9d
title: 80 20 Rule
desc: ''
updated: 1632769173279
created: 1632769173279
---

The [[dentropydaemon-wiki/Wiki/Concepts/List/Pareto principle|wiki.concepts.list.Pareto principle]] applied to raising funds: 20% of the donors contribute 80% of the total
