---
share: true
uuid: 4b5a5481-48b3-4d42-9bf8-39365ae1f4ef
---
---
id: W6ZVu3XE1HQdLPF7yyVcK
title: Normalization
desc: ''
updated: 1642602571032
created: 1642602298360
---


## Definition

To reduce data redundancy and increase data integrity.

Normalization organizes the columns and tables in a database to ensure that their dependencies are properly enforced by database integrity constraints.

## Links

* [[dentropydaemon-wiki/Wiki/Concepts/List/Database|wiki.concepts.list.Database]]