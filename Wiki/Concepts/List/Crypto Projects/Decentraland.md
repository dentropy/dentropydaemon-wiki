---
share: true
uuid: 3079829e-6163-4297-9701-ba03086bde7d
---
---
id: jchAEzjPxkhW7MzOZH7Zr
title: Decentraland
desc: ''
updated: 1635434171235
created: 1634232978489
---

## [[dentropydaemon-wiki/Wiki/Concepts/List/whitepaper|wiki.concepts.list.whitepaper]]

* [Decentraland Whitepaper](https://decentraland.org/whitepaper.pdf)

## [[dentropydaemon-wiki/Wiki/Concepts/List/Summary|wiki.concepts.list.Summary]]

* Read the whitepaper abstract, it is pretty good

## Links

## [[Tokenomics]]

* Land (NFT)
  * Token Features
  * Token Distribution Model
* MANA (Fungible)
  * Buy stuff in game
  * LAND is purchased by burning Decentraland’s native token MANA, making it deflationary (and indirectly redistributing value to tokenholders).
  * MANA has a max supply of 2.2T and a circulating supply of 1.3T. At a MCAP of ~$800M, the MANA token has two use cases:


## [[dentropydaemon-wiki/Wiki/Concepts/List/Mission Statement|wiki.concepts.list.Mission Statement]]

## [[dentropydaemon-wiki/Wiki/Concepts/List/Vision|wiki.concepts.list.Vision]]

## Notes

## Sources

* [Decoding the Metaverse: Under the Hood of Decentralized Gaming Tokenomics | by Clear Chain Capital | Coinmonks | Medium](https://medium.com/coinmonks/decoding-the-metaverse-under-the-hood-of-decentralized-gaming-tokenomics-b5cd9d907cfa)
