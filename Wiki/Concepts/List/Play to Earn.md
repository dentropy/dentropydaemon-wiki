---
uuid: a7f513df-63a1-48d3-9da4-d79c7ebd0cd2
share: false
---
## List of Play to Earn Games

* [[dentropydaemon-wiki/Wiki/Concepts/List/Crypto Projects/Axie Infinity]]
* [[Decentraland]]
* [[Holium/Research/Product Research/The Sandbox|The Sandbox]]
* [[MyNeighborAlice]]

## List of Play to Earn Platforms

* [[Firstblood]]

## Ubisoft 

* [Ubisoft Announces Plans To Develop Play-To-Earn, Blockchain And NFT Games](https://www.ibtimes.com/angelina-jolie-using-kids-publicity-unlike-ex-brad-pitt-report-claims-3328472)

## Reflection on PlayToEarn

Each PlayToEarn game takes an already existing gaming genre, or combination there of, and augments it with the Blockchain.

NFT in these games are relatively interesting because rather than in game auction houses we have global blockchain marketplaces for items.

The DAO integration with PlayToEarn games has got me confused. I can't tell what anyone is marketing about.

## Sources

* [Top Play to Earn Tokens by Market Capitalization | CoinMarketCap](https://coinmarketcap.com/view/play-to-earn/)
* [Play to Earn - NFT games, play-to-earn, GameFi and blockchain entertainment](https://www.playtoearn.online/)
