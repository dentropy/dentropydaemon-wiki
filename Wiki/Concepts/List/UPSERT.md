---
share: true
uuid: e694de9e-8159-4c8c-91cc-b7a5f90b0ef9
---
---
id: o4UMOKa9yUeHhUM1lqjcR
title: UPSERT
desc: ''
updated: 1642784043783
created: 1642783835453
---


## Links

* [[ON CONFLICT|wiki.software.Catagories.Database.SQL.Examples#on-conflict]]

## Sources

* [UPSERT - PostgreSQL wiki](https://wiki.postgresql.org/wiki/UPSERT)
