---
share: true
uuid: f9030f9f-cf72-4bf3-a683-8b8cdc7ac11e
---
---
id: J8MvzO7XoVg0cJhYzRcLI
title: Research
desc: ''
updated: 1639947419373
created: 1639945203031
---

## [Visualizing Movie Reviews in Kibana Graph with Elasticsearch backend - YouTube](https://www.youtube.com/watch?v=6dxQxRzLCB0)

* [freemansoft/examples: Home for Elasticsearch examples available to everyone. It's a great way to get started.](https://github.com/freemansoft/examples)

Turns out that elasticsearch can be used fine as a graph database somehow?

* [[dentropydaemon-wiki/Software/List/Elasticsearch|wiki.software.List.Elasticsearch#elasticsearch]]

## [Knowledge Graph Search with Elasticsearch — L. Misquitta and A. Negro, GraphAware - YouTube](https://www.youtube.com/watch?v=k8Gu6GMbBtQ)

* Use Cases
  * [[dentropydaemon-wiki/Wiki/Concepts/List/Panama Papers|wiki.concepts.list.Panama Papers]]
  * Disease Progression
  * Human Tracking
  * Traceability from source in a court of law
* COllaborative filtering
  * How reccomendation algorithms work
  * Requires tagging based systems or trust networks
* Relevance move around 4 different dimensions
  * User
  * Business Goal
  * Text
  * Context

![[/assets/images/2021-12-19-15-39-08.png]]

* ML (Machine Learning) Stuff
  * Co-occurrence
  * Latent Semantic Analysis
  * Latent Dirichlet Allocation
  * Work2Vec
* [TF/IDF Score](https://youtu.be/k8Gu6GMbBtQ?t=2086)
  * [[dentropydaemon-wiki/Wiki/Acronyms/TF-IDF|wiki.concepts.list.Acronyms.TF-IDF]]
* [Neo4j and ElasticSearch - Developer Guides](https://neo4j.com/developer/elastic-search/)
* [[MyDendronExistence/Possessions/Keys/neo4j keys|wiki.software.List.neo4j]]
* [[dentropydaemon-wiki/Software/List/Elasticsearch|wiki.software.List.Elasticsearch#elasticsearch]]