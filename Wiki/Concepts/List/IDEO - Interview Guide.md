---
uuid: 5a92390e-7e9a-4dd7-9586-8b9711082867
share: false
---
## Open General

[[What are some broad question you can ask to open the conversation and warm people up?]]

Then Go Deep

[[What are some questions that can help you start to understand this person's hope, fears, and ambitions?]]