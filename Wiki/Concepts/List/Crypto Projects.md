---
share: true
uuid: 409347b3-f25a-4bb9-8ffc-6f49ccf08961
---
## Places to look

* [State of the DApps — A list of 3,734 blockchain apps for Ethereum, Hive, EOS, and more](https://www.stateofthedapps.com/)
* [Cryptocurrency Prices, Charts And Market Capitalizations | CoinMarketCap](https://coinmarketcap.com/)

## Raw List of Crypto Token Projects

* Bitcoin
* Ethereum
* Stellar
* Cardano
* Polkadot
* Algorand
* Theta
* Livepeer
* Uniswap
* Terra
* Decentraland
* ENjin
* [0x: Powering the decentralized exchange of tokens on Ethereum](https://0x.org/)
* [YouNow](https://www.younow.com/login)
* [EEA Home - Enterprise Ethereum Alliance](https://entethalliance.org/)
* [Data Analysis | Data Profiling | Experian](https://www.experian.com/data-quality/experian-pandora)
* [Home - Abra](https://www.abra.com/)
* [ConsenSys Quorum | ConsenSys](https://consensys.net/quorum/)
* [Stacks](https://www.stacks.co/)
* [Tencent QQ - Wikipedia](https://en.wikipedia.org/wiki/Tencent_QQ)
* [iMPERIUM – Medium](https://medium.com/@iMPERIUMcoin)
* [Snapup - Disrupt the way you buy PREMIUM products!](https://snapup.biz/)
* [MetaXchain](https://github.com/metaxchain)
* [Mediachain : Documentation](http://docs.mediachain.io/)
* [[dentropydaemon-wiki/Wiki/Concepts/List/Defi|Identity Verification by Civic - compliance tools for decentralized finance (DeFi|[DeFi), public blockchains, NFTs, and businesses]], public blockchains, NFTs, and businesses]],%20public%20blockchains,%20NFTs,%20and%20businesses),%20public%20blockchains,%20NFTs,%20and%20businesses),%20public%20blockchains,%20NFTs,%20and%20businesses),%20public%20blockchains,%20NFTs,%20and%20businesses),%20public%20blockchains,%20NFTs,%20and%20businesses),%20public%20blockchains,%20NFTs,%20and%20businesses)
* MakerDAO
* AAVE
* Kyber Network
* [POAP - Homepage](https://poap.xyz/)

## Play to earn tokens

* [[dentropydaemon-wiki/Wiki/Concepts/List/Crypto Projects/Axie Infinity|wiki.concepts.list.Crypto Projects.Axie Infinity]]
* [[dentropydaemon-wiki/Wiki/Concepts/List/Crypto Projects/Decentraland|wiki.concepts.list.Crypto Projects.Decentraland]]
* [[dentropydaemon-wiki/Wiki/Concepts/List/Crypto Projects/The Sandbox|Concepts.list.The Sandbox]]
* [[dentropydaemon-wiki/Wiki/Concepts/List/Crypto Projects/Ethermon|wiki.concepts.list.Crypto Projects.Ethermon]]
* [[dentropydaemon-wiki/Wiki/Concepts/List/Crypto Projects/Ulti Arena|wiki.concepts.list.Crypto Projects.Ulti Arena]]

## Reminders

* [[dentropydaemon-wiki/Wiki/Research/Research Template|Concepts.list.Crypto Projects.Template]]
