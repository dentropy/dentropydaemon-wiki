---
share: true
uuid: f38f40e6-27b3-41ba-999e-901eef5c7287
---
---
id: MuXGZhq2gMYj5B76HxJ5Y
title: Normal Form
desc: ''
updated: 1642605789375
created: 1642602957224
---

Follow my existing intuition on how databases should be setup. We want things accesses by the primary key.

In order to get into second normal form you need to use some sort of matching key across tables.

## Links

* [[dentropydaemon-wiki/Wiki/Concepts/List/Database|wiki.concepts.list.Database]]

