---
uuid: 2191f52c-a547-4ff7-be3c-74ce920d5b1d
share: false
---
The different places one does to interact with other people, one's social API.