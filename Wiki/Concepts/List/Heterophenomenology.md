---
share: true
uuid: 3213d6c4-e618-4a42-847d-060edfcf6959
---
---
id: iIixKqSMlCKSCmVPM8BJW
title: Heterophenomenology
desc: ''
updated: 1639790829476
created: 1639790569009
---

Introspection of the other.

Heterophenomenology is a term coined by [[MyDendronExistence/Relationships/People/Daniel Dennett|Relationships.People.Daniel Dennett]] to describe an explicitly third-person, scientific approach to the study of consciousness and other mental phenomena. It consists of applying the scientific method with an anthropological bent, combining the subjects self-reports with all other available evidence to determine their mental state. The goal is to discover how the subject sees the world him- or herself, without taking the accuracy of the subjects view for granted.

## Links

* [[dentropydaemon-wiki/Wiki/Concepts/List/Autophenomenology|wiki.concepts.list.Autophenomenology]]

## Sources

* [Heterophenomenology - Wikipedia](https://en.wikipedia.org/wiki/Heterophenomenology)