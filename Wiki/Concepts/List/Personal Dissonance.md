---
share: true
uuid: b3bae7bb-d53b-40cb-8eeb-a0efc703cb75
---
---
id: BNeCei44Oann0fDSDn9c4
title: Personal Dissonance
desc: ''
updated: 1641368488611
created: 1641368465562
---


## Links

* [[dentropydaemon-wiki/Wiki/Concepts/List/Cognitive Dissonance|wiki.concepts.list.Cognitive Dissonance]]

## Sources

* [[https://mantracare.org/therapy/what-is/cognitive-dissonance/#The_Three_Types_of_Personal_Dissonance]]