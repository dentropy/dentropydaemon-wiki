---
share: true
uuid: 80b89261-900d-472f-9a4e-7a580ae51e99
---
---
id: bv6lYY1sStraQgfliFsQw
title: ELI5 Archatype
desc: ''
updated: 1628449250260
created: 1628449250260
---
# ELI5: Archatype
From [[../Media Consumption/Books/Book Notes/12 Rules For Life.md|12 Rules For Life]]

It is for this reason that the Christian sacrificial drama of Son and Self is archetypal. It’s a story at the limit, where nothing more extreme—nothing greater—can be imagined. That’s the very definition of “archetypal.” That’s the core of what constitutes “religious.”
