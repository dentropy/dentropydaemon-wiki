---
share: true
uuid: 5241197b-7146-46d5-9084-e10a1f56804f
---
---
id: DoxdbOqltk5NlsGMQBRug
title: Six degrees of separation
desc: ''
updated: 1634420702875
created: 1634420520806
---

* [Six degrees of separation - Wikipedia](https://en.wikipedia.org/wiki/Six_degrees_of_separation)
* What is the term for this concept of a highly connected network
  * Searched, "term average distance between nodes graph thery"
    * [[dentropydaemon-wiki/Wiki/Concepts/List/Average path length|wiki.concepts.list.Average path length]]
