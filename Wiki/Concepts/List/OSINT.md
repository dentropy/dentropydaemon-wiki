---
share: true
uuid: e5a5a594-f979-4aa3-bbd0-da97ff62c224
---
* Tools
	* [OSINT Framework](https://osintframework.com/)
	* [IntelTechniques OSINT Online Search Tool](https://inteltechniques.com/tools/index.html)
* Books
	* [[Open Source Inteligence]]
	* [[OSINT Handbook]]
* Videos
	* [[OSINT|Open-Source Intelligence (OSINT|[OSINT) in 5 Hours - Full Course - Learn OSINT! - YouTube]] in 5 Hours - Full Course - Learn OSINT! - YouTube]]%20in%205%20Hours%20-%20Full%20Course%20-%20Learn%20OSINT!%20-%20YouTube)%20in%205%20Hours%20-%20Full%20Course%20-%20Learn%20OSINT!%20-%20YouTube)%20in%205%20Hours%20-%20Full%20Course%20-%20Learn%20OSINT!%20-%20YouTube)%20in%205%20Hours%20-%20Full%20Course%20-%20Learn%20OSINT!%20-%20YouTube)%20in%205%20Hours%20-%20Full%20Course%20-%20Learn%20OSINT!%20-%20YouTube)%20in%205%20Hours%20-%20Full%20Course%20-%20Learn%20OSINT!%20-%20YouTube)
		* Basically goes through the Open Source Intelligence Book
	* [OSINT: You can't hide // Your privacy is dead // Best resources to get started - YouTube](https://www.youtube.com/watch?v=ImWJgDQ-_ek)
		* Podcast, listen later
	* [The Creepiest OSINT Tool to Date - YouTube](https://www.youtube.com/watch?v=uBynB50liTw)
		* Identify people by photos of someone's face using all photos across internet [PimEyes](https://pimeyes.com/en)
	* [Doing a Live OSINT Investigation on an Instagram Influencer - YouTube](https://www.youtube.com/watch?v=KTVHRdSFBJU)
* Communities
	* I joined a bunch on Discord Guilds
