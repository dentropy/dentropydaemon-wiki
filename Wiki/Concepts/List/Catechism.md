---
share: true
uuid: 474cf7d6-0c55-489d-90d0-cf4edce33b3a
---
* [Catechism - Wikipedia](https://en.wikipedia.org/wiki/Catechism)
	* "a summary or exposition of doctrine and serves as a learning introduction"
*  [Catechism DB](https://coda.io/d/Catechism-DB_dDI977il1RE/Catechisms_sumC5)
* Specific Catechism's
	* [[Heilmeier Catechism]]
	* [[Facilitator's Catechism]]
