---
uuid: cd7b3c57-af78-4cb1-9e5d-c95c95c433e3
share: false
---
## Succinctly, what is your project?

## Who do you need to pitch?

## What format(s) will your pitch take?

## What’s your short pitch? As you write it, think about how you’ll expand it into a larger one.


## Notes