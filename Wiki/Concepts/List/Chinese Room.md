---
share: true
uuid: e1aed0af-98ef-4514-9a7e-8e6eaaac2f1d
---
---
id: OvBUWyoCWlvkvp10kDDlt
title: Chinese Room
desc: ''
updated: 1639792875094
created: 1639792863517
---

## Sources

* [[Stanford Encyclopedia of Philosophy|The Chinese Room Argument (Stanford Encyclopedia of Philosophy|[Stanford Encyclopedia of Philosophy)]]]]))))))
* [Chinese room - Wikipedia](https://en.wikipedia.org/wiki/Chinese_room)
* [The Chinese Room | Philosophy](https://philosophy.tamucc.edu/notes/chinese-room)