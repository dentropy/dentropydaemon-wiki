---
share: true
uuid: a05cbbee-a18a-4a8a-9948-ee1fc05ad0d5
---
---
id: Y94MpMYioKDcjBZdfvXBT
title: Inmon s Corporate Information Factory
desc: ''
updated: 1644413666969
created: 1644413400354
---

* Inmon's Corporate Information Factory
  * 2 ETL Process
  * Source systems → 3NF database
  * 3NF database → Departmental Data Marts
  * The 3NF database acts an enterprise-wide data store.
  * Single integrated source of truth for data-marts
  * Could be accessed by end-users if needed
  * Data marts dimensionally modeled & unlike Kimball’s dimensional models, they are mostly aggregated

![[/assets/images/2022-02-09-08-30-47.png]]