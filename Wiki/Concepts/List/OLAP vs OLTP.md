---
share: true
uuid: 283d27f8-1257-438b-9159-5fc825d17eb1
---
---
id: pf6wU2pAtK8XMs3zrmrlb
title: OLAP vs OLTP
desc: ''
updated: 1642602008000
created: 1642601187305
---

* [[dentropydaemon-wiki/Wiki/Acronyms/OLAP|wiki.concepts.list.Acronyms.OLAP]]
  * Complex analytical and ad hoc queries
    * Heavy focus on aggregations
* [[dentropydaemon-wiki/Wiki/Acronyms/OLTP|wiki.concepts.list.Acronyms.OLTP]]
  * Less complex queries in large volumes
    * Low aggregations

## Sources

* [OLAP vs. OLTP: What’s the Difference? | IBM](https://www.ibm.com/cloud/blog/olap-vs-oltp)
