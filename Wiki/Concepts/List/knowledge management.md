---
share: true
uuid: 7decfa6a-d846-4d65-896c-cb2d2c315edc
---
---
id: p2kvhw1ygzzfj1smq3axc6m
title: Knowledge Management
desc: ''
updated: 1666119923469
created: 1666119689676
---

* [[memex.garden|wiki.software.List.Memex]]
* [[dentropydaemon-wiki/Software/List/Obsidian|wiki.software.List.Obsidian]]
* [[dentropydaemon-wiki/Software/List/logseq 1|wiki.software.list.logseq]]
* [[dentropydaemon-wiki/Software/List/Tiddly Wiki|wiki.software.List.Tiddly Wiki]]
* [[dentropydaemon-wiki/Software/List/Trilium Notes|wiki.software.List.trilium]]
* [[dentropydaemon-wiki/Software/List/org-mode|wiki.software.List.org-mode]]
* [[dentropydaemon-wiki/Software/List/wikijs|wiki.software.list.wikijs]]


[Federated knowledge graphs - Meta](https://meta.wikimedia.org/wiki/Federated_knowledge_graphs)