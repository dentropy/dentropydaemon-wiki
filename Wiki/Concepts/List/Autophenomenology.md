---
share: true
uuid: 3ebbb692-9961-40b2-a99b-256d7ffa1cb6
---
---
id: fNAtCVpcQYnZ13iUwUq3U
title: Autophenomenology
desc: ''
updated: 1639790812662
created: 1639790766601
---

Introspection from within.

## Links

* [[dentropydaemon-wiki/Wiki/Concepts/List/Heterophenomenology|wiki.concepts.list.Heterophenomenology]]