---
uuid: a0dad842-b574-4063-829a-2ca5d59970d8
share: false
---
* Searched "Awesome Knowledge Gardens"
	* [kyrose/awesome-digital-gardens: Inspirational digital gardens and resources for building your own 🍄🌻🌺🌿🌷🌼🌳🌻🌷🌼🌼🌻](https://github.com/kyrose/awesome-digital-gardens)
	* [CarlosGG's Knowledge Garden 🪴](https://carlos-gg.github.io/digitalgarden/)
* Searched "Digital Garden"
	* [MaggieAppleton/digital-gardeners: Resources, links, projects, and ideas for gardeners tending their digital notes on the public interwebs](https://github.com/MaggieAppleton/digital-gardeners)
	* [articles written by Joel Hooks](https://joelhooks.com/)
	* [Digital Gardens](https://old.reddit.com/r/DigitalGardens/)
	* [Examples of Digital Gardens](https://notes.alexkehayias.com/examples-of-digital-gardens/)
	* [What is a Digital Garden?. Digital gardens have become a popular… | by Esteban Thilliez | Medium](https://medium.com/@estebanthi/what-is-a-digital-garden-eeae89c7c483)
		* Search for Examples'
* List of Knowledge Gardens
	* [Home · Patrick Collison](https://patrickcollison.com/)
	* [Y Combinator](https://www.ycombinator.com/blog/author/paul-graham)