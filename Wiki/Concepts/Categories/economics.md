---
share: true
uuid: 08698f06-5b23-4072-97e5-86acad2031c0
---
---
id: clCGFpKhGIg8jLIlQfxe5
title: Economics
desc: ''
updated: 1633622130577
created: 1633620514478
---

* [[Holium Token Talking Points|Concepts.list.token]]
  * [[token bonding curve|swarmio.Swarmio Research.Tokenomics.token.bonding curve]]
* [[dentropydaemon-wiki/Wiki/Concepts/List/market maker|wiki.concepts.list.market maker]]
  * [[Automatic Market Maker|swarmio.Swarmio Research.concepts.automatic market maker]]
* [[dentropydaemon-wiki/Wiki/Concepts/List/liquidity|wiki.concepts.list.liquidity]]
