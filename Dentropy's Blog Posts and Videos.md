---
share: true
uuid: 3d59d5cc-de9f-42d3-96fd-e4bb02710a33
---

## Collections
* [[Inital Writings]]
* [[Mapping out Self Actualization]]
* [[Videos and Their Scripts]]

## Reverse Chronological, Probably Worth Reading
* [[Virtualizing The Self]]
* [[What Humans Value]]
* [[Learning to sail the memes]]
* [[Cringe meets theory of mind]]
* [[You took the Transhumanist Wager Now What?]]
* [[Consciousness and Parasites]]
* [[What is My Vision]]
* [[The Daemon is Real, Now What?]]
* [[How Does One Go About Wielding Their Own Plot Armor?]]

## Need to be expanded on

* [[Blockchain as the Operating System for the Technological Singularity]]
* [[The Human Social Interface]]
* [[Mapping The Human Heart]]

## Need to be rewritten

* [[Becoming A Dataist In Training]]
* [[Privacy Should You Care?]]

![[Videos and Their Scripts]]