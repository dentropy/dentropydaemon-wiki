---
uuid: fe8efadc-c3fc-40fc-8b5b-8ca0ad45abbb
share: false
---
---
title: ELI5 the Dentropy Daemon
description: 
published: true
date: 2021-06-02T01:37:11.051Z
tags: [[Social Media Singularity]]
editor: markdown
dateCreated: 2021-01-10T22:44:32.643Z
---

# ELI5 (Explain Like I am 5) The Dentropy Daemon

## Background

Human computer interaction today is not human centric, it is computer centicntric. We all have to contort our mental falculties in order to make a computer bend to our will. What if there was a program (daemon) on our computers that translated our will rather than us having to translate our will into computer speak. We should not have to think in terms of specific software and platforms, we should be thinking of what experiences and work we desire to accomplsih. We all want to be entertained, socialize, consume, and create.

People do not want to use facebook or twitter they want to socialize. People do not want to use iCloud or Google Drive they just want their files, photos, and videos backed up. People do not want to use GSuite or Office365 they want powerful information display formatting and editing systems. People do not want a netflix or disney+ subscriptions they want to be entertained with audio visual naratives.

At the end of the day computers exist to do three things, store, transform, and transfer data. The data individuals generate is stored as fragments across the cloud, and the multitude of devices one owns. If all the data a person generates were to be consolodated they would be able to perform analytics to generate an algorithmic reflection.

What if there was a tool that generated a interface on top of the platforms, protocols, and software of today. Examples include instant messaging, forum discussion, feed generation, and product shopping. The transformation of social media into a fluid design language and protocol can be refered to as the [Social Media Singularity](Social%20Media%20Singularity.md)

## Integration

The dentropy daemon is supposed to be an AI pal that helps you live the best life you can. One's AI pal should understand them like a parent except one's AI pal, understands one better, can be turned on and off, settings page, and has an app store. Why can't Google, Alexa, or Siri get me a job interview, tell me when to go to bed, or tell me when ones friends are free? Computer code can do all those things it is just a matter of design.

## Brainstorming

* Explain how a computer can copy the behaviors of your digital persona
* Explain why this metadata and self hosting is useful is useful
  * DIY Tiwtter, Facebook, Youtube algorithm
  	* Custom feeds, meta social media
  * See who views your social media
  * Query your history across all apps at once
  * Sythensize conversations with the same person, or gorup across social media
  * Migrate a community from facebook, keybase2.0

The Dentropy Daemon is a suite of software that is meant to evolve from the applications we use into an AI compantion. By collecting all data an individual produces algorithims can create models of a users behavior, an algorithmic reflection. This algorithnic reflection can be observed by the individual with options to change settings in the software suite to help optimize their life.

The Dentropy Daemon is a software suite that provides tools for generating an algoithmic reflection for the purpose of behavior modification and optimization.

The goal of Dentropy Cloud is to make running server applications as desirable, secure, and as easy as installing an app on a phone.

##