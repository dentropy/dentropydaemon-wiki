---
uuid: da0f6b58-e381-4bf4-87ea-15b155b26b60
share: false
---
* [[PKM]] + [[Social Annotation]] Integration
* [[Data Scraping]] + [[Data Annotation]]
* [[Social Media Singularity]], social messaging migration
* [[Question Engine]]
* Daemon Hosting Service
* Ingest all my data into a single time series database
* [[Skilltreeplatofrm]] + Tutorials
	* How TO DO RESEARCH
* Power of people I know
* Intent of people I know
* Psyop History and Course
* How much control do you have over your own life?
* CRM / Egregore Manager
* Data Migration Tooling
	* Confluence to Mediawiki for example
* SAAS Hosting and Service
	* Media wiki with LDAP
	* Misskey but only for emails from specific company
	* Lightning network stuff
	* LivePeer hosting
* Find comments associated with Articles