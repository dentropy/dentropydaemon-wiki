---
uuid: c5bd7ce4-89db-4473-8510-a633f7922e77
share: false
---
---
title: Social Media Binding Schemas
description: 
published: true
date: 2020-12-30T06:55:25.934Z
tags: 
editor: undefined
dateCreated: 2020-12-30T06:40:30.979Z
---

# Generalized Social Media Schemas

There are many common design patterns across all social media. If there were a generalized schema to repersent these design patterns a generalized API for social media would be possible. Every platform will simply be a constraint on the generalized schema, in theory....

There are many form that social media takes form such as threads chats, comments, and feeds. Let's take a look at the first form of social media to hit the masses email.

## email, the DM machine

Email is a bunch of messages linked together in chaotic ways. There are the bot emails, newsletters, email threads, announcements, and more. At its core email is like setting up a group chat with one or more people that can be added down the line. The design pattern is basically a linked list of messages between people.

## The Forum, threads for asynchronous comments on comments



## The Feed, algotainment

## The Platforms

### Facebook / Instagram

* Feed
* Groups
* Direct Messages

### Twitter

* Feed
  * Basically public email
* @ vs #
* Direct Messages

### Telegram

* Stickers

### Whatsapp

* End to End encrypted

### WeChat

* Groups are limited to 500 people.