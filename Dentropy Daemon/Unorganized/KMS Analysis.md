---
share: true
uuid: ea7bef36-42df-455b-8fb6-c8bdb458b6e5
---
* [SkillTree Docs](https://skilltreeplatform.dev/)

[[KMS - Knowledge Management Systems]]

## Characteristics

* [[Customization via Extensions]]
* Sharing
	* [[Live Sharing]]
	* [[Multi User Sharing]]
	* [[File System Sharing]]
	* [[Share as File]]
* [[Publishing]]
* [[Data Visualization]]
* [[Active Community]]
* [[Cross Platform]]
* [[RBAC - Rule Base Access Control]]
* [[Application Search]]
* [[E2EE - End To End Encryption]]
* [[Self Hostable]]
* [[SAAS - Software As A Service]]
* [[Has API]]
	* [[API - REST]]
	* [[API - GraphQL]]
	* [[Language - Query|Query Language]]
	* [[API - Internal]]
* [[Has Pub Sub]]
* [[File Formats Supported]]
* [[Data Export Functionality]]
* [[Open Source]]