---
uuid: 9da9a7c1-de23-4edc-b1a2-62e945207776
share: false
---

# Behavior Tracking / Time Management

<https://myactivity.google.com/myactivity>

* Use a calendar application and plan my time better
* Automatically catagorize what I am doing
  * Browsing web
  * Typing into my existance
  * Playing video games
  * Listening to X
  * Watching X
  * Reading X
