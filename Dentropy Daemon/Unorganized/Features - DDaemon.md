---
uuid: 9eb15095-279b-42eb-8331-84099700cdaa
share: false
---
---
title: Dentropy Daemon Features
description: 
published: true
date: 2021-04-29T16:35:41.272Z
tags: 
editor: markdown
dateCreated: 2021-01-14T22:15:29.824Z
---

# Dentropy Daemon Features

## Browser Daemon

*   Bookmarks are a curated database of URLs from the web
*   Bookmarks should be aware of your browsing history
*   I want to know when I put a bookmark into the database of URL's

When browsing the web we always have a goal in mind. Be it socialize, research, consume media, or a cure for boredom. The Browser Daemon tracks ones metadata while browsing the web. One can tag their activities so one can search their personal history better. The browser daemon also acts as the ultimate bookmarking Application a reasonable chunk of the user experience of social media sites such as youtube, reddit, and twitter can be replaced by a bookmarking application. The chunk of user experience I am refering to is the feed. One can have all their favorite youtube channels, sub reddits, and twitter users all bookmarked and tagged to generate custom feeds. One could have feeds for memes, data science, and world news. Not only would someone be able to generate custom feeds from other feeds on the internet. They would be able to save all their favorite posts to the same platform.

*   The Browser Daemon can
    *   Track browser state across time including context
        *   Number of browser windows open
        *   Size of browser windows
        *   State of browser window, is it active or hiding behind other windows
        *   Number and order of browser tabs
        *   State of browser tab, active or not
        *   Size of window
        *   Group and tag browsing behavior
            *   If one spent the last hour researching a topic for an essay they can tag all new tabs created in the last hour as part of that project. One can use the same tag as bookmarks for the project.
        *   Log / tag type of content
            *   Video, Article, Reddit
            *   People or organizations associated with content
    *   Manage bookmarks
        *   Tagging
        *   Higharchy
        *   The bookmark manager needs to log everything I ever do on it
            *   What bookmarks I click and when
            *   When bookmarks are added
            *   What device I am viewing the bookmark manager on
    *   Website annotations
        *   Setting to automatically back up said website
        *   Highlighting with many colors
        *   Tags
        *   Annotations
            *   Embed specific quotes from my [hypothes.is](https://hypothes.is/users/dentropy) into my notes.
    *   Platform augmentation
        *   RSS with more features
        *   The pinboard API can me amended to to support playlists and watch later for YouTube. Actually there can be a YouTube to pinboard converter. Also Twitter with twitter.follow and twitter.favorite also Reddit with reddit.subscribe and reddit.multireddit.name and reddit.collection.name and reddit.save and even messages actually pinboard and tags can abstract away the backend of many sites and even allow profiles.... Writing this gave me deja vu
    *   Search functionality
*   Stuff to research
    *   [Alternative to Bookmark Manager](https://alternativeto.net/software/bookmark-manager/?license=opensource)
    *   [Pocket](https://getpocket.com/)
    *   [Buku CLI](https://github.com/jarun/Buku)
    *   [Reddit: Help finding a bookmark manager that is self hosted, free and open source](https://www.reddit.com/r/opensource/comments/538hpo/help_finding_a_bookmark_manager_that_is_self/)
    *   [Promnesia | beepb00p](https://beepb00p.xyz/promnesia.html)
    *   [GitHub - karlicoss/promnesia: Another piece of your extended mind](https://github.com/karlicoss/promnesia)

## File Daemon

*   I want to know every file I have ever interacted with and have my interaction with them time stamped with as much metadata as reasonable
*   I want to manage my own data on a NAS and have it redundantly backed up
*   I have tiers of different data
    *   Random downloads like ISO's and tar files of open source projects
    *   Movies / Books / Games / Podcasts / Songs
    *   Images
        *   Personal / Web based / Meme's
*   I want to have a folder that is synced across all my devices with important files I always want backed up and files I want offline

## Activity Daemon

*   Where is the log file for human experience, it does not exist... yet
*   Explain how I want my logs to work ideally
*   DNS queries from computer
*   IP addresses connected to
*   Phone location
*   Bio metrics such as heart rate
*   Every search ever done
    *   Search Engines such as google, duckduckgo, and yahoo
*   Searches done on specific websites
    *   Facebook, Pornhub, Linkedin, Firefox Addon Store, Amazon, wikis
*   Whenever CTRL+f is used
    *   Web pages, documents, ebooks
*   File system searches
    *   Have an index of portions of the file system and log searches
*   Personal Journal
*   Activity Watch
    *   [ActivityWatch - Open-source time tracker](https://activitywatch.net/)
    *   Find a way to dump terminal logs to ActivityWatch
*   Key Logger
    *   I can log my keystrokes and mouse movements and I can analyze that data to better understand how I work
    *   I want this piece of software installed on all my computers
    *   I want to software configured on boot to automatically start keylogging what I do on all my machines
    *   I want one on my phone as well
    *   I want to store all this keylogged data from all my devices on a database distributed across my devices
    *   For the MyExistance 2.0 / Digital Skin dashboard I will be able to querry this data and even use visualization tools

## Social Media / Relationship Daemon

*   Generalized Social Graph
*   Call Logs across all social media apps
*   Time since last interaction for each person
*   All messages chronologically
*   Every interaction organized by person
*   Time spent on each social media platform by the second
*   Time spent generating each message sent
*   Explain analytics on all my relationships, time since last message and so forth
*   I want DentropyDaemon to be able to be able to remind me to call my girlfriend but that is a complicated condition to program in.
*   Stuff to Research
    *   [](https://github.com/monicahq/monica#purpose)

## Media Consumption Daemon

*   Awareness of the music you listen to, skipping songs is like a down vote
*   Ebook and audiobook in same format
*   Every video ever watched
    *   What seconds watched
    *   Speed was watching
    *   Subtitles dumped into database
    *   At exactly what second what was being consumed
        *   So basically track when pause, play buttons are clicked
*   Books
    *   Explain how I want to read books with every word from each character tagged and plugged into wikipedia also memes and tropes
    *   What page was being viewed by the second
        *   Even do what part of the page with eye tracking
    *   When highlights were made
    *   By the word when the audiobook was read to me
*   Music
    *   Exactly from what second to what second a song was played for
    *   Need tagging
*   Articles
*   Papers
*   Integrated Media Consumption
    *   Use websites like TMDB, GoodReads, MyAnimeList, and others to as a back end for the media I consume and want to consume.
    *   I should be able to organize and tag these pieces of media and have them automatically integrated into MyExistance.

## KMS (Knowledge Management System) Daemon

* Journal / Wiki Tooling
* Tagging
  * Date
  * Key Value
  * Datatypes
* Import and Export
* Multiuser
  * Permissions
* Version Control
* Extensibility
* API
	* Query Language
    * Final all mispelled words
    * Find all names of a person
    * Find all software names
  * Read Documents
  * Update Documents
* Backlinking
* Make drawings with Draw.io
* Mapping
* Integrations
  * Portions of wikipedia
  * Wiki.js
  * Archwiki
* Self Aware Recommendation Engine aka entailment Every name should be tagged and linked to a custom object. When I type Tim Yano the system should recommend I tag it and link it so social media and describe specific characteristics of our relationship
* The ultimate Todo app telling you to live your life you wake up brush your teeth it has that built in then has priorities for everything