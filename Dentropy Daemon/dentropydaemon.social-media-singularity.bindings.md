---
uuid: a1a24ac0-7258-49bc-9135-3d097939de0c
share: false
---
---
title: Social Media Bindings
description: 
published: true
date: 2021-01-11T04:17:07.001Z
tags: 
editor: undefined
dateCreated: 2020-11-05T02:33:12.139Z
---

# Social Media Bindings

The goal of the Social Media Singularity is to create a single data structure for all social media a individual person consumes and produces. Each binding must also be paird with tools for analysis otherwise what is the point.

**NOTE THAT ONLY KEYBASE BINDING IS UNDER DEVELOPMENT**

* [Generalized Social Media Schemas](DDaemon%20Bindings.md)
* [Keybase Binding](/dentropydaemon/social-media-singularity/bindings/keybase)
* [Discord Binding](/dentropydaemon/social-media-singularity/bindings/discord)
* [Facebook Binding](/dentropydaemon/social-media-singularity/bindings/Facebook)
* [Telegram Binding](/dentropydaemon/social-media-singularity/bindings/Telegram)
* [Email Binding](/dentropydaemon/social-media-singularity/bindings/Email)

## Existing tools

* [Matterbridge](https://github.com/42wim/matterbridge)
  * Tool for deploying syncronized chat channels across multiple social media applications
* [All in one messenger](https://allinone.im/)
  * An application one can download to recieve and send messages from a vast list of social media platforms
  * [List of similar applications](https://alternativeto.net/software/all-in-one-messenger/)

## Brainstorming other Bindings

* Signal
* Slack
* Gaming Platforms
  * Steam
  * Origin
  * Epic
  * BattleNet
* Dating Apps
  * Tinder
  * OKCupid
  
## Identity / Messaging Solutions

* OpenID
* Matrix Protocol
* Solid Protocol
* Scuttlebut Protocol
* Fediverse Platforms
* Urbit
* Keybase

## TODO 

Befriend new members, moderators, and active members
Document organization scope and project descriptions (max 500 characters)
Setup personal account
Setup organization account
Test transaction and key creation and message
Setup org hierarchy
Publish org hierarchy
Publish public keybase and pgp signed message
