---
uuid: 0d0529da-b080-41ee-b1d8-0366c60c9ac6
share: false
---
---
title: GeneratedAnalytics Class
description: Python3 class to organize different kinds of data from Keybase export. Lives in generate_analytics.py.
published: true
date: 2020-11-07T00:55:03.167Z
tags: 
editor: undefined
dateCreated: 2020-11-07T00:19:26.862Z
---

# `GeneratedAnalytics` Class #

Python3 class to organize different kinds of data from `Keybase` export.
Lives in `generate_analytics.py`.

## Basic Use ##
_**Import syntax**_
```python
from generate_analytics import GeneratedAnalytics
```

_**Constructor**_
```python
gen_an = GeneratedAnalytics("sqlite:///complexityweekend.sqlite")
```

## `GeneratedAnalytics` Properties ##

The properties of the `GeneratedAnalytics` class are maybe best to think of as "data reports." `GeneratedAnalytics` "reports" are refreshed by corresponding **[Methods](#generatedanalytics-methods)**.

### `user_list` ###

```python
GeneratedAnalytics.user_list = []
```

A `list` containing `string` elements that are the unique users in the database.

### `topic_list` ###

```
GeneratedAnalytics.topic_list = []
```

A `list` containing `string` elements that are the unique topics in the database.

### `topic_list` ###

```python
GeneratedAnalytics.characters_per_user = {"user": [], "characters_per_user": []}
```

A `dict` array with the total number of characters entered via messages to chat by element in [`user_list`](#user_list).

### `characters_per_topic` ###

```
GeneratedAnalytics.characters_per_user = {"user": [], "characters_per_topic": []}
```

A `dict` array with the total number of characters per element in [`topic_list`](#topic_list).

### `messages_per_user` ###

```python
GeneratedAnalytics.messages_per_user = {"user": [], "messages_per_user": []}
```

A `dict` array with the total number of messages to chat by element in [`user_list`](#user_list).

### `messages_per_topic` ###

```python
GeneratedAnalytics.messages_per_topic = {"topic": [], "messages_per_topic": []}
```

A `dict` array with the total number of messages per element in [`topic_list`](#topic_list).

### `number_users_per_topic` ###

```python
GeneratedAnalytics.number_users_per_topic = {"users_list": [], "topics_list": []}
```

A `dict` array with the number of users per element  in [`topic_list`](#topic_list).

### `reaction_per_message` ###

```python
GeneratedAnalytics.reaction_per_message = {"ordered_message_id":[], "num_reaction":[]}
```

A `dict` array with the message ID and corresponding number of reactions to that message.

### `reaction_sent_per_user` ###

```python
GeneratedAnalytics.reaction_sent_per_user = {"ordered_user":[], "user_to_reaction":[]}
```

#### TODO ####
> 
> Check that this is actually what we think it is. Looks like User gets sorted, but `user_to_reaction` is not?
{.is-danger}


### `reaction_popularity_map` ###

```python
GeneratedAnalytics.reaction_popularity_map = {"reactions":{}}
```

#### TODO ####

> Clarify what this is (or remove)?
{.is-warning}

### `reactions_per_user` ###

```python
GeneratedAnalytics.reactions_per_user = {"users_reactions":{}, "users_ordered":[]}
```

#### TODO ####

> Clarify what this is: lists of unique reactions per user?
{.is-warning}

### `received_most_reactions` ###

```python
GeneratedAnalytics.recieved_most_reactions = {"users_reactions":{}, "users_ordered":[]}
```

#### TODO ####
> Clarify what this is: list of messages with most reactions by user and what the reactions were?
{.is-warning}


### `edits_per_user` ###

```python
GeneratedAnalytics.edits_per_user = {
    "users":{}, 
    "ordered_users":[], 
    "ordered_num_edits":[]}
```

Which user had the most (raw) number of edits?

### `edits_per_topic` ###

```
GeneratedAnalytics.edits_per_topic = {
"topics":{}, 
"ordered_topics":[], 
"ordered_num_edits":[]}
```

Which topic had the most (raw) number of edits?

### `deletes_per_user` ###

```python
GeneratedAnalytics.deletes_per_user = {
"users":{}, 
"ordered_users":[], 
"ordered_num_deletes":[]}
```

Which users had the most (raw) number of deletes?

### `deletes_per_topic` ###

```python
GeneratedAnalytics.deletes_per_topic = {
"topics":{}, 
"ordered_topics":[], 
"ordered_num_deletes":[]}
```

Which topics had the most (raw) number of deletes?

### `who_edits_most_per_capita` ###

```python
GeneratedAnalytics.who_edits_most_per_capita = {
    "users":{}, 
    "ordered_users":[], 
    "ordered_edit_per_capita" : []}
```

Who edits most per message?

### `who_deletes_most_per_capita` ###

```python
GeneratedAnalytics.who_deletes_most_per_capita = {
    "users":{}, 
    "ordered_users":[], 
    "ordered_edit_per_capita" : []}
```

Who deletes the most per message?

### `topic_edits_per_capita` ###

```python
GeneratedAnalytics.topic_edits_per_capita = {
    "topics":{}, 
    "ordered_topics":[], 
    "ordered_edit_per_capita" : []}
```

Which topic channels had the most edits per message?

### `topic_deletes_per_capita` ###

```python
GeneratedAnalytics.topic_deletes_per_capita = {
    "topics":{}, 
    "ordered_topics":[], 
    "ordered_edit_per_capita" : []}
```

Which topic channels had the most deletes per message?

### `top_domains` ###

```python
GeneratedAnalytics.top_domains = {
    "URLs":{}, 
    "top_domains_sorted":[], 
    "num_times_repeated":[]}
```

What were the most-used top-level domains, what were the specific links, and how many times did they appear?

---

## `GeneratedAnalytics` Methods ##

Methods are mainly there to return "refreshed" versions of the data with respect to the database.

### `get_message` ###

```python
message = GeneratedAnalytics.get_message(MESSAGE_ID_NUM)
```

Returns a single row from Message object (SQL table) by ID.

### `get_reaction_per_message` ###

```python
GeneratedAnalytics.get_reaction_per_message()
```

Update the reactions to each message.

### `get_reaction_sent_per_user` ###

```python
GeneratedAnalytics.get_reaction_sent_per_user()
```

Update the reactions sent by each user.

### `get_num_messages_from_user` ###

```python
msg_changes = {"edit": INT, "text": INT, "delete": INT}
msg_changes.append(GeneratedAnalytics.get_num_messages_from_user("USER"))
```

Return object with number of times a text was edited or deleted for a given user.

### `get_num_messages_from_topic` ###

```python
messages = {"edit": INT, "text": INT, "delete": INT}
messages.append(GeneratedAnalytics.get_num_messages_from_topic("TOPIC"))
```

Return object with number of times a text was edited or deleted for a given topic.

### get_list_all_users ###

```python
GeneratedAnalytics.get_list_all_users()
```

Should be called from the object constructor; updates and returns list of all users in database.

###### TODO ######
> Set scoping for private/public methods?
{.is-warning}


### get_list_all_topics ###

```python
GeneratedAnalytics.get_list_all_topics()
```

Should be called from the object constructor; updates and returns list of all topics in database.

###### TODO ######
> Set scoping for private/public methods?
{.is-warning}

### `get_characters_per_user` ###

```python
characters_per_user = {"user": [], "characters_per_user": []}
characters_per_user.append(GeneratedAnalytics.get_characters_per_user())
```

Update and return total number of characters from messages for each user.

###### TODO ######
> Set scoping for private/public methods?
{.is-warning}

### `get_characters_per_topic` ###

```python
characters_per_topic = {"user": [], "characters_per_topic": []}
characters_per_topic.append(GeneratedAnalytics.get_characters_per_topic())
```

Update and return total number of characters from messages posted in each topic.

###### TODO ######
> Set scoping for private/public methods?
{.is-warning}

### `get_messages_per_user` ###

```python
messages_per_user = {"user": [], "messages_per_user": []}
messages_per_user.append(GeneratedAnalytics.get_messages_per_user())
```

Update and return total number of messages for each user.

###### TODO ######
> Set scoping for private/public methods?
{.is-warning}

### `get_messages_per_topic` ###

```python
messages_per_topic = {"user": [], "messages_per_topic": []}
messages_per_topic.append(GeneratedAnalytics.get_messages_per_topic())
```

Update and return total number of messages posted in each topic.

###### TODO ######
> Set scoping for private/public methods?
{.is-warning}

### get_number_users_per_topic ###

```python
number_users_per_topic = {"users_list": [], "topics_list": []}
number_users_per_topic.append(
    GeneratedAnalytics.get_number_users_per_topic)
```

Update and return the number of users for each topic.

###### TODO ######
> Set scoping for private/public methods?
{.is-warning}

### `get_reaction_popularity_topic` ###

```python
reactions = {"reactions":{}, "list":[]}
reactions.append(GeneratedAnalytics.get_reaction_popularity_topic("TOPIC"))
```

Get popularity of all reactions in a topic corresponding to a specific topic (`string`).

### `get_all_user_message_id` ###

```python
msgID = {"users_reactions":{}, "users_ordered":[]}
msgID.append(GeneratedAnalytics.get_all_user_message_id(user))
```

For a specific user (`user`, `string`), return all message IDs involving that user.

### `get_user_sent_most_reactions` ###

```python
GeneratedAnalytics.get_user_sent_most_reactions()
```

Return the sorted user by most number of reactions issued.

### `get_user_received_most_reactions` ###

```python
GeneratedAnalytics.get_user_received_most_reactions()
```

Update and return the sorted listing of users by number of reactions received.

### `get_edits_per_user` ###

```python
GeneratedAnalytics.get_edits_per_user()
```

Update and return the raw number of edited messages by user.

### `get_deletes_per_user` ###

```python
GeneratedAnalytics.get_deletes_per_user()
```

Update and return the raw number of deleted messages by user.

### `get_edits_per_topic` ###

```python
GeneratedAnalytics.get_edits_per_topic()
```

Update and return the raw number of edited messages by topic.

### `get_deletes_per_topic` ###

```python
GeneratedAnalytics.get_deletes_per_topic()
```

Update and return the raw number of deleted messages by topic.

### `get_who_edits_most_per_capita` ###

```python
GeneratedAnalytics.get_who_edits_most_per_capita()
```

Update and return the sorted per-capita message edits by user.

### `get_who_deletes_most_per_capita` ###

```python
GeneratedAnalytics.get_who_deletes_most_per_capita()
```

Update and return the sorted per-capita message deletions by user.

### `get_topic_edits_per_capita` ###

```python
GeneratedAnalytics.get_topic_edits_per_capita()
```

Update and return the per-capita edits by topic.

### `get_topic_deletes_per_capita` ###

```python
GeneratedAnalytics.get_topic_deletes_per_capita()
```

Update and return the per-capita deletes by topic.

### `get_top_domains` ###

```python
GeneratedAnalytics.get_top_domains()
```

Update list of most-popular top-level domains linked in text chat.

### `get_reaction_type_popularity_per_user` ###

```python
reaction_type_popularity_per_user = {
    "users_reactions":{},
    "reactions_ordered":[]
}
reaction_type_popularity_per_user.append(
    GeneratedAnalytics.get_reaction_type_popularity_per_user(
        "USERS USERNAME"))
```

Returns/updates the popularity of a given reaction type by their username.

### `get_message_data_frames` ###

```python
df = get_message_data_frames(self, offset_time=0)
```

Returns `df`, a [`Pandas`](https://pandas.pydata.org/getting_started.html) data frame with user, message ID, time, team name, topic, body text, and word count data for `"text"` type messages only.