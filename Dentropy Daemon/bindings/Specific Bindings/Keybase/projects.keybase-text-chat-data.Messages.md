---
uuid: f630787c-1f4e-4f3a-bfe7-a16601d661fc
share: false
---
---
title: Messages Class
description: Python3 class that uses sqlalchemy to interface with SQL database. Lives in database.py.
published: true
date: 2020-11-07T01:03:09.836Z
tags: 
editor: undefined
dateCreated: 2020-11-07T00:36:30.350Z
---

# `Messages` Class #
Python3 class that uses `sqlalchemy` to interface with `SQL` database. Lives in `database.py`.

## Basic Use ##

_**Import syntax**_
```python
from database import DB, Messages
```

_**Constructor**_
```python
db = DB("sqlite:///complexityweekend.sqlite") # in GeneratedAnalytics constructor
```
*Note: the constructor is in the `DB` object, which mediates the `sqlite` connection.*
*This is mediated in `database.py`, where the object returned by `declarative_base()` method from sqalchemy is modified by the `Messages` class.*


## `Messages` Properties ##

Each `Messages` property can be thought of like a table column. They correspond to parts of the `.json` object returned by querying `Keybase` that we want to retain about each message, which includes transactions like "reactions" to other text chat messages, or users entering and leaving a channel.

### `id` ###

```python
id = Column(Integer, primary_key=True)
```
Primary key identifier for each unique Message.

### `team` ###

```python
team = Column(String(1024))
```

Which `Keybase` team was this message sent in?

### `topic` ###

```python
topic = Column(String(128))
```

What Topic channel was this  message sent in?

### `msg_id` ###

```python
msg_id = Column(Integer)
```

What message does this instance reference?

### `msg_type` ###

```python
msg_type = Column(String(32))
```

What type of message (i.e. "text", "reaction", etc.) was this interaction?

### `from_user` ###

```python
from_user = Column(String(128))
```

From which user did this message originate?

### `sent_time` ### 

```python
sent_time = Column(Integer)
```

What time was this message sent?

* *Note: this is the number of seconds, in posixtime convention (UTC). That is, the number of seconds elapsed since 1970.*

### `txt_body` ###

```python
txt_body = Column(String(4096))
```

What text content was in the body of the message?

### `word_count` ### 

```python
word_count = Column(Integer)
```

How many words are in the body of the message?

####  TODO #### 
> Indicate how this was computed. Are stop words included? How were words tokenized?
{.is-warning}


### `num_urls` ###

```python
num_urls = Column(Integer)
```

How many URLs are referenced in this message?

### `urls` ###

```python
urls = Column(String(4096))
```

What URLs were identified in this message?

#### TODO ####

> Indicate how this was computed: are the URLs recognized on the Keybase end or on our side? From the code it looks like it is done on their end.
{.is-warning}


### `reaction_body` ###

```python
reaction_body = Column(String(1024))
```

If this is a reaction message, what emoji reaction was used in response to the message?

### `msg_reference` ###

```python
msg_reference = Column(Integer)
```

If this message replies to (for `"text"`) or reacts to (for `"reaction"`), which message identifier does it reference?

### `userMentions` ###

```python
userMentions = Column(String(1024))
```

What users were `@<user>` tagged in this message?