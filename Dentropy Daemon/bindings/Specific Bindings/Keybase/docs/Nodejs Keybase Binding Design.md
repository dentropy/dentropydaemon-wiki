---
uuid: 34f6a968-d72f-4a93-ac2b-092d25f02a33
share: false
---


* NodeJS Rewrite
  * Need to be able to specify network location
  * Export every team to a unique folder
  * Export every channel to a unique file in the team folder
  * Export attachments to a unique folder in the team folder
  * Export git repos to a unique folder in the team folder
  * Export shared files to a unique folder in the team folder
  * Export users to a unique folder
  * Export DM's
  * Get list of teams and export them to root of export location
* Export each team DM to a unique folder
