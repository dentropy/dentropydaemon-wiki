---
uuid: d08cb4f7-accf-4c4a-b672-dc3cb4a029f7
share: false
---
---
title: Requests  from Dentropy Daemon Keybase Team
description: 
published: true
date: 2020-10-24T19:00:46.454Z
tags: 
editor: undefined
dateCreated: 2020-10-23T21:23:38.351Z
---

# Requests from Dentropy Daemon Keybase Team

* Tooling reccomendations
  * Answer: Anaconda, Jupyer lab/notebook, vscodium
* Bot to transfer messages from one channel to another
* Python Text Extraction tool
* Tool to check many keybase account for team invites
* Login through keybase rest API
* Analytics Dashboard completed
* Mass keybase account manager