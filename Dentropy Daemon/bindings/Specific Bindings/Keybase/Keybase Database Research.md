---
uuid: c1b8d4a8-7732-4f2e-ab6d-b79254e2fa49
share: false
---
---
title: Databases
description: Descriptions of data structure, access schema, and documentation of associated code.
published: true
date: 2020-11-06T22:32:42.306Z
tags: 
editor: undefined
dateCreated: 2020-11-06T21:49:42.804Z
---

# Databases #
*A list of existing or in-progress databases*  

Databases are classified by their working state. They are sub-classified by the primary data-type.

## Automated ##
*Self-updating databases that are more-or-less complete go here.*
> Nothing falls under this category, *yet*.
{.is-danger}

### Text ###

### Image ###

### Measurement ###

### User ###

### Video ###

## In-Progress ##
*Databases that are currently under construction go here.*

### Text ###
* **[`Keybase` text chat](/Dashboards/Databases/Keybase-Text-Chat-Data)**: Text and user metadata scraped from the `Keybase` securely encrypted open-source social media platform for text communications.
  + ***Primary feature:** data in any message is posted in a channel with a given Topic; therefore, this provides a potentially powerful way to incorporate Topic Modeling into a tool that makes use of keyword linkage during inter-user interactions.*

### Image ###

### Measurement ###

### User ###

### Video ###

## TODO ##
*Ideas for future databases to create, curate, and maintain go here.*

- [ ] Empty
