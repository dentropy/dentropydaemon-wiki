---
share: true
uuid: 16cc922f-56ea-422e-95be-72f5f55e4111
---
---
## Links

* [[Discord Binding Logs]]
* [[Discord Binding Research]]
* [[Get Discord User Token]]
* [[Discord Elasticsearch Queries]]
* [[Discord Datapipeline]]
* [[Discord Binding V1]]
* [[Discord Binding V1 Schema]]
* [[Discord Bot]]
* [[Discord Bots]]
* [[Discord Datapipeline]]
* [[Discord Schema]]





---
title: Discord Binding
description: 
published: true
date: 2020-11-07T19:00:29.133Z
tags: 
editor: undefined
dateCreated: 2020-11-07T18:56:07.307Z
---

# Discord Binding


## Paul's thoughts and notes

* [Pyhton Package](https://discordpy.readthedocs.io/en/latest/)
* [Nodejs package](https://discord.js.org/)
* [DiscordChatExporter tool](https://github.com/Tyrrrz/DiscordChatExporter)
* For discord servers we probably want to develop a bot for exports rather than scrapeing using a user account whcih can get ones account banned