---
uuid: dc6a1ac7-60f0-452d-9536-9fed6d92bc51
share: false
---
* [[2023-05-18]]
	* Alright so why are we here?
		* This seems to be the vector my attention should be focused on
	* What do we want to do?
		* Index and queries discord communities to find high agency individuals
	* What do we want to do with high agency individuals
		* Make new friends
		* Understand their intent and goals
		* Form a team
	* What constitutes a high agency individuals
		* We are using [[Discord Elasticsearch Queries]] as our baseline and we will improve from there
	* What does MVP look like?
		* A [[cube js dashboard]] upgrade of [[Discord Binding V1]]
	* How do you get up an upgraded [[Discord Binding V1]] dashboard?
		* We need to have a lot of discord guilds indexed
		* We need a effective, well documented, indexing procedure
		* We need a well documented Schema
		* We need a well documented API
		* We need well implimented user stories
	* Paul why don't you do this for keybase first?
		* We will do both at the same time
		* You just increased the scope by an order of magnitude
		* No we already built them separately
		* We need a generalized social media schema
		* Didn't we do that already?
		* [[Social Media Singularity]]
	* What is the PLAN Paul?
		* Find discord data
		* Ask questions of Discord Data
		* Find list of communities we find interesting and want to index
		* Get Discord Nitro for one of our accounts, maybe two, it's worth it just do it
		* Develop generalised social media schema
			* Also look at other social media exports and document their data structure
		* Write import script
		* Develop [[cube js dashboard]]s
		* Develop social media migration tooling
		* SELL SELL SELL
	* I like this plan, snack, brush, walk
