---
uuid: 34bdb50e-6fc6-4c8d-8614-86365cb2f245
share: false
---
## [[How to add a Bot to a Discord Server]]

## To Research

* [Home | PluralKit](https://pluralkit.me/)
	* [PluralKit/Message.cs at 83af1f04a74819fbef35fc77e377c1295e17bd6e · PluralKit/PluralKit](https://github.com/PluralKit/PluralKit/blob/83af1f04a74819fbef35fc77e377c1295e17bd6e/PluralKit.Bot/Commands/Message.cs#L201)
	* Proof I need webhooks

## Other

* That ID is the ID of the application not the BOT secret key
* General Bot Stuff
  * [How to Make Your Own Discord Bot](https://www.howtogeek.com/364225/how-to-make-your-own-discord-bot/)
  * [Getting a Discord Guild id · Chickenpowerrr/RankSync Wiki](https://github.com/Chickenpowerrr/RankSync/wiki/Getting-a-Discord-Guild-id)
  * [How to get a token and channel ID for Discord · Chikachi/DiscordIntegration Wiki](https://github.com/Chikachi/DiscordIntegration/wiki/How-to-get-a-token-and-channel-ID-for-Discord)
* Bot Examples
  * [HaiderZaidiDev/Discord-Pin-Archiver-Bot: A discord bot that archives and fetches pinned messages.](https://github.com/HaiderZaidiDev/Discord-Pin-Archiver-Bot)
  * [jonas747/yagpdb: Yet another general purpose discord bot](https://github.com/jonas747/yagpdb)
* Tasks
	* List servers Bot has been added to
	* List Channels
	* Create a Channel
		* Create a Category Channel
		* Add a text channel under a category
		* Add a voice channel under a category 
	* Read old messages in channel
	* Send message in Channel
	* Create a role
	* Limit category to specific roles
	* Limit channel to specific roles


## Sources

* [discord.js v14 create channel - Stack Overflow](https://stackoverflow.com/questions/73840912/discord-js-v14-create-channel)
* [Discord Developer Portal — Documentation — Channel](https://discord.com/developers/docs/resources/channel#channel-object-channel-types)
* [discord.js | CategoryCreateChannelOptions](https://old.discordjs.dev/#/docs/discord.js/main/typedef/CategoryCreateChannelOptions)
* [discord.js | ClientUser](https://old.discordjs.dev/#/docs/discord.js/main/class/ClientUser?scrollTo=setUsername)