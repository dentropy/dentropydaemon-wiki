---
share: true
uuid: ce890113-147e-4515-8d16-b9e0abf81cd2
---
---
[ddaemon-monorepo/binding-discord at main · dentropy/ddaemon-monorepo](https://github.com/dentropy/ddaemon-monorepo/tree/main/binding-discord)

## README

## Requirements

* Unix Environment
  * [[dentropydaemon-wiki/Software/List/WSL|wiki.software.list.WSL]] for [[dentropydaemon-wiki/Software/List/Windows|wiki.software.List.Windows]]
  * [[dentropydaemon-wiki/Software/List/MacOS|wiki.software.List.MacOS]]
  * [[dentropydaemon-wiki/Software/List/linux|wiki.software.List.linux]]
* ddaemon-monorepo
* [[dentropydaemon-wiki/Software/List/DiscordChatExporter|wiki.software.List.DiscordChatExporter]]
  * [[dentropydaemon-wiki/Software/List/docker|wiki.software.List.docker]] (Optional, will have to change commands to export guilds)
* [[Discord User Token|wiki.ddaemon.monorepo.bindings.discord.Get Discord User Token]]
* [[dentropydaemon-wiki/Software/List/nodejs|wiki.software.list.nodejs]] or [[dentropydaemon-wiki/Software/List/nvm|wiki.software.List.nvm]]
* [[dentropydaemon-wiki/Software/List/Elasticsearch|wiki.software.List.Elasticsearch#elasticsearch]] Instance

## Scraping

``` bash
git clone keybase://team/dentropydaemon/ddaemon-monorepo
cd ddaemon-monorepo/discord-binding
mkdir inputs

# Export your guilds
docker run -v \
  `pwd`:/app/out tyrrrz/discordchatexporter:stable exportguild \
  -t "DISCORD_USER_TOKEN" -g GUILD_ID \
  --dateformat "yyyy-MM-dd H:mm:ss.ffff"\
   -f Json -p 80mb
```

## Indexing 

``` bash
cd ddaemon-monorepo
cp .env_sample .env

# Edit .env file with environment variables
vim .env

## RUN THESE ONE AT A TIME
cd discord-binding
npm install
node index.js
cd ..
bash ./discord-binding/test_connection.sh
bash ./discord-binding/set_mapping.sh
bash ./discord-binding/delete_index.sh
bash ./discord-binding/elastic_index.sh
bash ./discord-binding/set_mapping.sh
```
