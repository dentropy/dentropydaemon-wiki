---
share: true
uuid: 5598ab4c-9e1d-40b2-b63b-94b52fac2cc5
---
---

[[ENS Indexing]]

## Description

* The dashboard should support querying the the blockchain and tokens on it.

## Init

Track certain crypto wallets that we know are associated with these people

## Data Indexed

* Fungible token
  * Transactions
    * FROM
    * TO
    * Receipt
    * Block Num
    * Time
* NFT
* Supported Interface / 

## Queries

* Volume of token transferred per day
* 
