---
uuid: 54a6f3c8-e86e-42f4-b561-e8e10ab5591b
share: false
---

## Research

* [How to install and run bots for the Matrix network – /tmp/lab](https://www.tmplab.org/2020/04/01/how-to-install-and-run-bots-for-the-matrix-network/)
* [matrix-org/matrix-bot-sdk-tutorial: Repository of the bot we created in the matrix-bot-sdk-tutorial](https://github.com/matrix-org/matrix-bot-sdk-tutorial)