---
uuid: 73bea91f-ee73-4a95-a79d-e1b0c4140984
share: false
---
---
title: Dentropy Daemon Brain Goop
description: 
published: true
date: 2021-01-16T01:21:21.222Z
tags: 
editor: undefined
dateCreated: 2021-01-16T00:52:25.512Z
---

# Dentropy Daemon Brain Goop

A place for information and ideas that does not currently have a place or needs to be further refiend 

## Inital Plan

Step 1: Create an ontology of all the best tools for managing large sets of data and permissions

Step 2: Come up with ontologies on how we want to organize all the worlds information including the permission layer

Step 3: We have design meetings to come up with with parts and teams that will build the meta ontology that will make up the the spatial web

## Elevator Pitch

An integrated software suite that manages all data a user generates through an API. DentropyDaemon a name for the HPI (Human Programable Interface).