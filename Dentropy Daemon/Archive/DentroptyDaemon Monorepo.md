---
share: true
uuid: 65af23d6-ebd9-4f19-94a7-85fba48aeaff
---
## ddaemon-monorepo

A collection of ETL(Extract Transform Load) tools for taking social media and quantified self data and generating visualizations so people can better understand themselves and others.

## Motivation

Why can't Google index all my data allowing me to search myself? The vision for Dentropy Daemon is to take all ones data into a single extendable API that the user and only the user has access and control over.

## Guide to Bindings and Apps

* Bindings are tools to get and transform data from different sources into a format that can be queried.
* Apps are tools that can be used to interact with the data once the bindings have it in the correct format

## Bindings

* [[Keybase Binding Inital Docs|wiki.ddaemon.monorepo.bindings.keybase]]
  * Export data from keybase and put it in Elasticsearch
* [[Discord Binding|wiki.ddaemon.monorepo.bindings.discord]]
  * Take exported data from discord and put it into Elasticsearch
* [[Git Binding|wiki.ddaemon.monorepo.bindings.git]]
  * TODO

## Apps

* [[ddaemon-webapp|wiki.ddaemon.monorepo.app.web]]
  * Visualize the data from different bindings
