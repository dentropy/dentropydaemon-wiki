---
share: true
uuid: da8ee43f-5075-4547-a583-65a941185d4a
---
## Query Inspiration

* [[Git Queries|wiki.ddaemon.monorepo.bindings.keybase.queries]]
* [[dentropydaemon-wiki/Skills/Learning Pathways/SysAdmin/Basics|wiki.software.list.Elasticsearch.Examples.Basics]]
* [[dentropydaemon-wiki/Software/List/Elasticsearch/size length filter|wiki.software.list.Elasticsearch.size length filter]]

## Reminders

* We already indexed the character length as well as word length
* Should we also index number of reactions, YES it will make things much easier
* **I need to fix the indexing of topic names, entire string not there**
* **I should count the reactions as a int when initially indexing**
* **I should index the size of each message when initially indexing**
* **Skip Pagiation for now, just get 250**
* **Don't try and get everything working at once**
* We can look for the most of a specific character or word
*  [[dentropydaemon-wiki/Dentropy Daemon/Archive/Monorepo/User Stories - DDaemon]]

## Query Brainstorming
* Who sent the most messages
* Who sent the most message, within a certain time range
* Who sent reacted the most per message sent
* Who replied the most per message sent
* Who sent the more ( reactions
* Who was replied to the most
* Longest active user
* Shortest active user
* Average Half Life of a User

## Base Queries (Not in dashboard)

* [✅] List all users
  * [[KeybaseListAllUsers|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListAllUsers]]
* [✅]List all teams
  * [[KeybaseListAllTeams|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListAllTeams]]
* [✅] List all topics
  * [[KeybaseListAllTopics|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListAllTopics]]
* [✅] List all topics for specific team
  * [[KeybaseListAllTopicsForSpecificTeam|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListAllTopicsForSpecificTeam]]
* [✅] List all user on specific team
  * Built into left sidebar
  * [[KeybaseListAllUsersOnSpecificTeam|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListAllUsersOnSpecificTeam]]
* [✅] List search results
  * There is a separate general search module
  * [[KeybaseListSearchResults|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListSearchResults]]

## List

* [❌] List messages before a specific message
  * **NEED SOCIAL INTERFACE RECREATION MODULE**
* [❌] List messages after  a specific message
  * **NEED SOCIAL INTERFACE RECREATION MODULE**
* [❌] List most recent messages from team
* [❌] List most recent messages posted in topic
* [✅] List all users with the teams they are on
  * [[KeybaseListAllUsersWithTheTeamsTheyAreOn|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListAllUsersWithTheTeamsTheyAreOn]]
* [✅] List topics a user has posted in
  * [[KeybaseListTopicsAUserHasPostedIn|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListTopicsAUserHasPostedIn]]
  * [✅] List topics a user has NOT posted in
    * [[KeybaseListTopicsAUserHasNOTPostedIn|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListTopicsAUserHasNOTPostedIn]]
* [✅] List teams  a user has posted  in
  * [[KeybaseListTeamsAUserHasPostedIn|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListTeamsAUserHasPostedIn]]
  * [✅] List teams  a user has NOT posted in
    * [[KeybaseListTeamsAUserHasNOTPostedIn|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListTeamsAUserHasNOTPostedIn]]
* **ALL BUGS BELOW ARE BECAUSE ALL USERS ARE LISTED RATHER THAN USERS ON TEAM**
* [✅] List users  that have posted in team
  * [[KeybaseListUsersThatHavePostedInTeam|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListUsersThatHavePostedInTeam]]
  * [✅] List users  that have NOT posted in team
    * KeybaseListUsersThatHaveNOTPostedInTeam
      * [[KeybaseListAllTopicsForSpecificTeam|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListAllTopicsForSpecificTeam]]
      * [[KeybaseListTopicsAUserHasPostedIn|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListTopicsAUserHasPostedIn]]
* [✅] List users  that have posted in a specific topic
  * [[KeybaseListUsersThatHavePostedInASpecificTopic|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListUsersThatHavePostedInASpecificTopic]]
  * [✅] List users  that have NOT posted in a specific topic
    * KeybaseListUsersThatHaveNOTPostedInASpecificTopic
      * [[KeybaseListAllUsersOnSpecificTeam|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListAllUsersOnSpecificTeam]]
* [✅] List longest messages in specific topic(Characters)
  * [[KeybaseListLongestMessagesInSpecificTopicCharacters|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListLongestMessagesInSpecificTopicCharacters]]
* [✅] List longest messages on team (Characters)
  * [[KeybaseListLongestMessagesOnTeam|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListLongestMessagesOnTeam]]
* [✅] List longest messages in specific topic(Words)
  * [[KeybaseListLongestMessagesInSpecificTopicWords|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListLongestMessagesInSpecificTopicWords]]
* [✅] List longest messages on team (Words)
  * [[KeybaseListLongestMessagesOnWords|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListLongestMessagesOnWords]]
* [✅] List mentions of teams
  * [[KeybaseListMentionsOfTeams|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListMentionsOfTeams]]
* [✅] List mentions of topics
  * Do we want to list the most recent mention of topics or do we want to just be able to search channel mentions?
  * [[KeybaseListMentionsOfTopics|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListMentionsOfTopics]]
* [❌] List mentions of specific topic
  * **NEED TO UPDATE INDEX TOOL**
  * [[KeybaseListMentionsOfSpecificTopic|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListMentionsOfSpecificTopic]]
* [❌] List mentions of specific team
  * **NEED TO UPDATE INDEX TOOL**
  * [[KeybaseListMentionsOfSpecificTeam|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListMentionsOfSpecificTeam]]
* [❌] List mentions of specific user
  * **NEED TO UPDATE INDEX TOOL**
* [❌] List mentions of users
  * **NEED TO UPDATE INDEX TOOL**
* [❌] List messages reacted to most in specific topic
  * **NEED TO UPDATE INDEX TOOL**
  * [[KeybaseListMessagesReactedToMostInSpecificTopic|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListMessagesReactedToMostInSpecificTopic]]
* [❌] List messages reacted to most on team
  * **NEED TO UPDATE INDEX TOOL**
  * [[KeybaseListMessagesReactedToMostOnTeam|wiki.ddaemon.monorepo.User Stories.Lists.KeybaseListMessagesReactedToMostOnTeam]]
* [❌] List messages reacted to most on team
  * **NEED TO UPDATE INDEX TOOL**
  * KeybaseListMessagesReactedToMostOnTeam
* [❌] List most replied to messages in specific topic
  * **NEED TO UPDATE INDEX TOOL**
  * KeybaseListMostRepliedToMessagesInSpecificTopic
* [❌] List most replied to messages on team
  * **NEED TO UPDATE INDEX TOOL**
  * KeybaseListMostRepliedToMessagesOnTeam
* [❌] Most recent messages in [Everything, Team, Topic]
* Advanced (Requires new component set)
  * List messages from set of User,Topic, or Team chronologically

## Ways to measure things

* Existence of characteristic exists
  * User has or hos not posted in Topic or Team
  * User has or has not replied to or mentioned another user
* Longest
  * Message relative to User, Topic, and Team
* Most
  * Reactions to Message across topic, team, or specific user
  * Replies to message across topic, team, or specific user
  * Messages within or from a specific topic, team, or user
  * Mentions of user across topic or team
  * Mentions of topic, across team
* Time / Chronology
  * Oldest
  * Newest
  * Restricted time range
