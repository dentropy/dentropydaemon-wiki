---
share: true
uuid: 6bc689f8-26a9-4b6f-98fe-4a2d0f3ee37a
---


The goal of the Social Media Singularity is to create a single data structure for all social media a individual person consumes and produces. Each binding must also be paird with tools for analysis otherwise what is the point.

* [[DDaemon Bindings|Generalized Social Media Schemas]]
* [[ddaemon-webapp|ddaemon.monorepo.schemas]]

## Existing tools

* [Matterbridge](https://github.com/42wim/matterbridge)
  * Tool for deploying syncronized chat channels across multiple social media applications
* [All in one messenger](https://allinone.im/)
  * An application one can download to recieve and send messages from a vast list of social media platforms
  * [List of similar applications](https://alternativeto.net/software/all-in-one-messenger/)

## Brainstorming other Bindings

* Email
* Signal
* Keybase
* Discord
* Reddit
* Facebook
* Youtube
* Browsing History
* Dating Apps
  * Tinder
  * OKCupid
* Gaming Platforms
  * Steam 
  * Origin
  * Epic
  * BattleNet
* [[Holium/Research/Identity Protocols|wiki.software.Catagories.Identity]]

## TODO 

Befriend new members, moderators, and active members
Document organization scope and project descriptions (max 500 characters)
Setup personal account
Setup organization account
Test transaction and key creation and message
Setup org hierarchy
Publish org hierarchy
Publish public keybase and pgp signed message
