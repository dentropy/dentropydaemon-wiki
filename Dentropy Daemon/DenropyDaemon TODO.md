---
share: true
uuid: b9cd3e8b-1727-4a22-9332-90b42b5a7ffb
---
## TODO Where?

* [[Dentropy Daemon Design Doc]]
* [[Logs - DentropyCloud]]
* [[Dentropy Daemon Recruiting]]

## TODO

* [[Vision - DDaemon]]
* Document Technical Projects
* Docker Is Awesome! Blog post / Tutorial
  * Misskey setup

## Build a community

* Islander Advice
  * Befriend new members, moderators, and active members
  * Document organization scope and project descriptions (max 500 characters)
  * Setup personal account
  * Setup organization account
  * Test transaction and key creation and message
  * Setup org hierarchy
  * Publish org hierarchy
  * Publish public keybase and pgp signed message
