---
share: true
uuid: 488cb22c-91d3-4d1e-bd47-b1588e3fb899
---

## Intro

* [[Mission Statement - DDaemon]]
* [[Vision - DDaemon]]
* [[Motto - DDaemon]]
* [[Intro - DDaemon]]
* [[DDaemon Bindings]]

## Links for Paul

* [[Logs - DDaemon]]


## DDaemon Design

* [[Design Kit - DDaemon]]
* [[Catechism - DDaemon]]
	* [[Heilmeier Catechism -  DDaemon]]
* [[dentropydaemon-wiki/Dentropy Daemon/Design/User Stories - DDaemon]]
* [[Specific Aims - DDaemon]]
* [[Situation Scan - DDaemon]]
* [[Scoping - DDaemon]]
* [[Design Brief - DDaemon]]
* [[MVP - DDaemon]]
* [[DDaemon Features]]
* [[Business Case - DDaemon]]

## DDaemon Reminders

* [Copy of Digital Communications Protocols - Google Sheets](https://docs.google.com/spreadsheets/d/1moR2wTGfnuqf3x6neTOxMTHb33bewkiFnJLUR2BrGFM/edit#gid=0)
* [[Pure Functions of Human Computer Interaction]]
* [[dentropydaemon-wiki/Dentropy Daemon/Design/Semantic Forum]]
* [[Build a community]]
* [[Roadmap - DDaemon]]

## Brainstorming

* [[Brainstorming - DDaemon]]

