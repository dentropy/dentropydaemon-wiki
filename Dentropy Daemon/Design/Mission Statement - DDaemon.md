---
share: true
uuid: 49651cdb-2917-4c0e-b237-0ef9db099384
---
* Use algorithms to enhance human agency while building infrastructure [[Cypherpunk]] would admire.
* I want to build tools to reduce cognitive dissonance and force self awareness.
* Reduce Platform Fatigue
	* Whatsapp/Telegram/Signal = "Single Threaded Messengers"
	* Discord, Matrix, Keybase = "Multi Threaded Messengers"
	* Twitter, Email, Reddit = "Graph Based Messengers"
	* Amazon, Netflix, Youtube, IMDB = "Interactive Product Catalogs"

## Links

* [[Mission vs Vision Statements]]
* [Purpose, Mission, and Vision Statements - Management Tools | Bain & Company](https://www.bain.com/insights/management-tools-mission-and-vision-statements/#:~:text=A%20Mission%20Statement%20defines%20the,company's%20purposes%2C%20goals%20and%20values.)
