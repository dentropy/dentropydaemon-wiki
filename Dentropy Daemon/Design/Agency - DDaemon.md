---
share: true
uuid: 26b25984-ce01-4aa6-99cf-757eb46ffddd
---
## Ways to measure agency using social media

[[Serac|Relationships.People.Engerraund Serac]], a fictional character within [[dentropydaemon-wiki/Media/List/Westworld|media.list.Westworld]] season 3, uses this wonderful phrase, bubbles of agency, as a label for individuals in the truest form of the word people who obtain and wield power and yet can not be predicted. John Galt and his friends also have a similar phrase for the same concept, Prime Movers.
