---
share: true
uuid: c97c49f2-d064-4987-994b-62b4fdd918b2
---
[[snipe-it]]

* Apps
	* [[Trilium Notes]]
	* [[wikijs]]
	* [[Misskey]]
	* [[Monica CRM]]
	* [[Jellyfin]]
	* [[Syncthing]]
  * Torrent Client with VPN
    * [[qbittorrent]]
  * Static Blog
  * RSS Reader
    * [[Miniflux RSS]]
* TODO
  * Stop everything and restart script
  * Update Authelia file with domains
  * Add scripts to manage authelia
  * Storage Management
  * Backup and Restore
  * LDAP
  * Authelia Integration for SMTP, LDAP and 2FA
* Apps to do after TODO
  * Gitlab
  * Nextcloud with Office
  * Ghost
  * Searex
  * Wallabag
  * Matrix Server
  * gPodder
  * Jupyter Hub Server
  * GitPods
  * Code-Server

* Apps for Dentropy Cloud
  * Dynamic DNS
  * Better backup and restore
  * Keycloak Middleware
  * Docker Swarm
  * Separate network for every app
  * Nebula / Wireguard
  * SMTP
  * Onion Routing
* Apps
  * Seafile
  * Something for Bookmarks
  * Gitlab or Gitea
  * Guacamole
  * Nextcloud
  * Something Project Management
