---
share: true
uuid: 89182d9f-ab57-497c-96c6-0aff10c6724d
---

| Team Name                   | Dentropy Daemon                               |
| --------------------------- |:--------------------------------------------- |
| Project Callsign            | @dentropydaemon                               |
| Facilitator                 | Paul Mullins @dentropy                        |
| Contact Information         | @dentropy on [[keybaseSoftware.List.keybase]] |
| Date of Announcement        | ~October 2020                                 |
| Call for Collaboration Ends | When we enter the posthuman era               |
| Intended Date of Completion | When we enter the posthuman era               |

## Situation

[[Dentropy Daemon - Home]] is a different way of thinking about human computer interactions. Computers and the applications they run are supposed to augment our lives making us more productive, knowledgeable, and self aware so we can enjoy life. The vision for [[Dentropy Daemon - Home]] is to take all ones data into a single extendable API that the user and only the user has access and control over. Our computers function as our [[second and or outboard Brain]] yet our data is spread out all over the place making us all scatter brained. Once securely putting our [[outboard brain|wiki.concepts.list.Second Brain#eli5-second-brain]] back together we can give it the ability to run programs in the background monitoring the data coming in from you, the internet, and others. Unhooking our data from platforms and replacing said platforms with protocols will allow for more human centric computation.

[[dentropydaemon-wiki/Wiki/Concepts/List/Dentropy Cloud|wiki.ddaemon.design.Dentropy Cloud.Dentropy Cloud Description]] is a different way of running applications. The apps we all use on the web and on our phones don't really run on our phones they run or are dependent on someone's server. Back in the day one opened up spreadsheets that were store on their computer, now we open up spreadsheets stored on computers 1000's of miles away. The goal of [[dentropydaemon-wiki/Wiki/Concepts/List/Dentropy Cloud|wiki.ddaemon.design.Dentropy Cloud.Dentropy Cloud Description]] is to put the user not only in control apps they are running on the machines in front of them but the computer in the cloud truly running them.

## Potential Avenues of Approach

## Milestones

* [[Keybase Binding Inital Docs|wiki.ddaemon.monorepo.bindings.keybase]]
* [[DentroptyDaemon Monorepo|wiki.ddaemon.monorepo]]
* [[Heilmeier Catechism -  DDaemon|wiki.ddaemon.design.heilmeier catechism]]

## Implications of Outcome

These tools provide us a 
 