---
share: true
uuid: 18e9f505-d972-4314-83cd-2fa8e20e00da
---
* [[dentropydaemon-wiki/Software/Catagories/Mind Map|wiki.software.Catagories.Mind Map]]
* Drawing (Draw.io)
* [[dentropydaemon-wiki/Software/Catagories/Tier List|wiki.software.Catagories.Tier List]]
* Structured (Spreadsheet / SQL)
* Unstructured (JSON, XML, Key Value)
* [[dentropydaemon-wiki/Software/Catagories/Database/Graph Database Software]]
* Collection Higharchy / Namespace (DNS, Folder Structure, Bookmark collections)
* Project Management
	* Kan Board
	* Issue Tracker
	* Gantt Chart
* Thread (Reddit Comment, Hacker News Comment, Discourse, 4Chan)
* Content Feed (Facebook, Twitter, Hackernews)
* Message Feed (Texting, Direct Messages, Matrix)
* Logs
* [[dentropydaemon-wiki/Software/Catagories/Annotation Software]]
  * Tagging
* Time Management (Calendering), Time Table
* QUestionares / Forms
* *Permissions*
* Flash Cards
* Polls
* Checklists
* CRM

So if we are going to codify the human experience what parts of the world would not fit nicely into these model's

* System Modeling
* Supply Chain
* What about graphs and diagrams
* What about geospatial information
* How does the system understand natural language
* What about infographics
* What about images in general?
  * Image's in general are just a collection of metadata. They are tagged, they have descriptions, they each have a unique position in the meme pool of DID(Decentralized Identitie's)
  * There is Spatial Data and understanding which the above list does not cover
* What about Interactive Systems such as Games?
* What about the image softare that translates the text on images into other languages.
* What about stuff like the concept of quadratic funding?
