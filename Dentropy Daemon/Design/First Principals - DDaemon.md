---
uuid: af8e8c2c-4069-463c-ad18-fe29eccf4aab
share: false
---
---
title: First Principals
description: 
published: true
date: 2021-01-16T01:03:44.945Z
tags: 
editor: undefined
dateCreated: 2021-01-07T17:32:24.494Z
---

Dentropy Daemon First Principals Brainstorming
----------------------------------------------

Elon always says to solve problems from first principals. What is the most simple piece of information I should be logging before trying to create a meta social media platform? Keystrokes. Terminal Commands. DNS Traffic. Browsing History. Also gonna steal a design principal from the [Unix Philosophy](https://en.wikipedia.org/wiki/Unix_philosophy), store data as flat text files.

Syncing quantified self data
----------------------------

A shared folder across systems can be used to consolodate logged data across sytems. A series of python scripts managed useing cron and service files will gather the data and store it in the synced folder. Seperate files for each computer can be generated using a computers hostname. The directory of the synced file folder can be stored as an environment variable.

Keystroke logging
-----------------

    echo "export quantized_self_dir=/home/dentropy/Documents/Software/keylogger" >> ~/.bashrc 
    

    from pynput import keyboard
    import logging
    import os
    import socket
    
    #log_dir = os.environ['quantized_self_dir']
    log_dir = "/home/dentropy/Documents/Software/keylogger"
    os.makedirs(log_dir)
    logging.basicConfig(filename = log_dir + "/{}keyLog.txt".format(str(socket.gethostname())), 
        level=logging.DEBUG, format='%(asctime)s: %(message)s')
    
    
    def get_key_name(key):
        if isinstance(key, keyboard.KeyCode):
            return key.char
        else:
            return str(key)
    
    def on_press(key):
        key_name = get_key_name(key)
        logging.info('Key {} pressed.'.format(key_name))
        print(key_name)
    
    def on_release(key):
        key_name = get_key_name(key)
        logging.info('Key {} released.'.format(key_name))
    
    with keyboard.Listener(
        on_press = on_press,
        on_release = on_release) as listener:
        listener.join()

Kelogging Research Links:

[https://www.tutorialspoint.com/design-a-keylogger-in-python](https://www.tutorialspoint.com/design-a-keylogger-in-python) [https://realpython.com/python-logging/](https://realpython.com/python-logging/)

Terminal Command Logging
------------------------

On unix systems all commands are logged by default in order in which they were input. One can set an environment variable in bash to enable date and time data to also be logged.

    HISTTIMEFORMAT="%F %T "
    echo "export HISTTIMEFORMAT=\"%F %T \"" >> ~/.bashrc 
    

Terminal Command Logging Links:

[https://www.linuxuprising.com/2019/07/bash-history-how-to-show-timestamp-when.html](https://www.linuxuprising.com/2019/07/bash-history-how-to-show-timestamp-when.html) [https://awesomeopensource.com/project/wulfgarpro/history-sync?categoryPage=3](https://awesomeopensource.com/project/wulfgarpro/history-sync?categoryPage=3)

DNS Traffic
-----------

[https://github.com/jheise/dns\_analyze](https://github.com/jheise/dns_analyze)