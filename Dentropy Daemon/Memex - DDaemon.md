---
uuid: 75187815-0be9-48e2-8aef-056b93ca3fec
share: false
---
---
title: Memex
description: 
published: true
date: 2021-01-07T17:29:51.040Z
tags: 
editor: undefined
dateCreated: 2020-10-21T03:23:29.779Z
---

## Memex / Personal wiki / knowledge curation

“We are overwhelmed with information and we don’t have the tools to properly index and filter through it. [The development of these tools, which] will give society access to and command over the inherited knowledge of the ages [should] be the first objective of our scientist” - Vannevar Bush, 1945

The Memex was proposed as the ultimate personal library for a user’s books, correspondence, and records. In addition to just storing this data, users could navigate, organize, link, and share it with others.[S1]


## Memex Directory

* [Knowledge Media Curation](/dentropydaemon/memex/media-knowledge-curation) - Your own personal wiki knowledge graph that grows alongside you. Almost all knowledge is generated from media consumption so better track all that too
* [Behavior Tracking / Time Management](/dentropydaemon/memex/behavior-tracking) - Get to know yourself through quantified self then be able to use behavior changing tools to help you become the best version of yourself.
* [Memex Brainstorming](/dentropydaemon/memex/brainstorming)

## Important Background Reading

* <https://beepb00p.xyz/hpi.html>
  * [](https://beepb00p.xyz/myinfra.html#mypkg)
* <https://www.steveliu.co/memex>

## Cool tools

* <https://www.dailymap.app/>
* <https://github.com/GitJournal/GitJournal>

  [S1]: https://hyfen.net/memex/

