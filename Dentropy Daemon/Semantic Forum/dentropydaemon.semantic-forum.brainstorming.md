---
uuid: 0b020593-085f-4c80-88cc-7f856e79c7d5
share: false
---
---
title: Semantic Forum Brainstorming
description: 
published: true
date: 2020-11-15T16:20:40.804Z
tags: 
editor: undefined
dateCreated: 2020-11-15T16:20:36.618Z
---

The Semantic Forum
------------------

[Imported form here](https://dynalist.io/d/uudYrj0kbQlvghld8wmnlq3c)

* * Goals
    * Build an open-source forum and knowledge-building software
            by using a semantic graph as its core structure.
    * Define an ontology for productive conversation, which can
            also help the users to navigate and find information.
    * Provide tools for effective and meaningful knowledge
            building and sensemaking
    * Provide a client-side framework for writing components (or
            \"views\") that can display and change the graph.
  * Learn the core concepts
    * Semantic network - Wikipedia
    * Ontology (information science) - Wikipedia)
    * Linked Data - Wiki \| Tim Berners-Lee TED Talk \|
            non-technical introduction
    * Other related or similar projects:
        * DBpedia - DBpedia allows users to semantically query
                relationships and properties of Wikipedia resources,
                including links to other related datasets.
        * OntoWiki - Semantic Data Wiki and Linked Data Publishing
                Engine
        * Metacademy - A \"package manager\" for knowledge
  * Why a semantic graph?
    * By connecting data in a more flexible way, we give our users
            new ways to express their thoughts and report on their
            understanding.
    * Designing the knowledge-base as a graph will make it more
            easy for algorithms to aggregate the data, and answer
            complex queries.
    * What problems are we trying to solve and what needs are we
            trying to meet?
        * Stop discussions from getting lost in data flows.
        * Enable discussions to naturally branch/diverge in
                multiple directions
        * Filter away all that is irrelevant
        * Promote discussing complex subjects
        * Preserving knowledge
        * \"Channels\" for both slow and quick respondance
        * Ways to collect, curate and condense what we can
                collectively agree upon
        * Collaboration on information navigation.
        * Navigating the tension between openness and order
            * quote from PMWeng: \"You need to have a firm enough
                    grasp on what is reliably real: a grounding
                    structure or reference frame, and you must also be
                    flexible and under-determined enough to notice and
                    digest as yet unincorporated forms and material. Too
                    much of either fails.\"
  * How can I contribute?
    * We want to involve as many people as we can.
    * Take part of the current active discussions:
        * The Portal Forum threads:
            * How can we find Better methods to organize
                    information and create emergent internet structures?
        * \#semantic-forum-project on ThePortal\'s CoffeHouse
                Discord
            * (Invite to ThePortal\'s CoffeHouse Discord)
  * How can I get more involved?
    * Message Focusless or erezsh on Discord or on The Portal
            Forum
  * Todo
    * See the \#needs-work tag
    * Get the discussion going
        * Announce this project on the portal forum
        * Create a subreddit for this project.
        * Try to get a room in the forum for this project
    * refactor into a separate discussion:
        * <https://forum.theportal.dev/t/how-can-we-find-better-methods-to-organize-information-and-create-emergent-internet-structures/965/16?u=erezsh>
    * Backend

  [Imported form here]: https://dynalist.io/d/uudYrj0kbQlvghld8wmnlq3c
