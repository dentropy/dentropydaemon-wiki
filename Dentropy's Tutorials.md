---
share: true
uuid: b554fe38-0be3-4e5e-a817-41077f5f6e69
---
Before reading through the tutorials I have already documented you may want to check out [[Learning Pathways]] where I map out how different skills and projects connect to one another.

## List of Tutorials

* [[SQL]]
	* [[Why you should learn SQL]]
	* [[Recommended SQL Tutorials]]
	* [[Online SQL Consoles]]
	* [[Dentropys's SQL Alchemy Tutorial]]
	* [[JSON in sqlite]]
	* [[Ideas for SQL Projects]]
* [[Python]]
	* [[Why you should learn Python]]
	* [[Recommended Python Tutorials]]
	* [[Online Python Interpreters]]
* [[HTML]]
	* [[Why you should learn HTML]]
	* [[Recommended HTML Tutorials]]
	* [[Online HTML Editors]]
* [[JavaScript]]
	* [[Why you should learn JavaScript]]
	* [[Recommended JavaScript Tutorials]]
	* [[Online JavaScript Editor]]
* [[Homalab and SysAdmin Skills]]
	* [[How to mount a partition in Linux?]]
	* [[Create a Multi ISO USB Drive and document the process as a tutorial]]
* Project Tutorials
	* [[Web Scraping Tutorial]]
* [[Umbrel]]
	* [[Umbrel - Secure Install]]
	* [[Umbrel - Backup and Restore]]
	* [[Umbrel - Migrate App]]
* TODO
	* Database Backup and Restore
	* Docker Networking
	* CLI Torrent Client with VPN
	* S3 / Minio Tutorial
	* Pub Sub
		* Rabbit MQ, Nats, Kafka
	* Data Streaming
		* Kafka, Nats Jetstream
	* Data Lake
		* Presto
		* [How Discord Stores Trillions of Messages | Deep Dive - YouTube](https://www.youtube.com/watch?v=xynXjChKkJc)
	* Changing networking in live environment
	* Natural Language Search
		* [[Meilisearch]]
	* [[NLP]]
		* [Natural Language Processing Demystified](https://www.nlpdemystified.org/)
	* Deploying [[Funky Penguin]]'s Software
	* [[Dentropy's Ideal DevSecOps Stack]]