---
share: true
uuid: e76c8ac9-69f3-477f-8015-556e83738432
---
* [[DIY Keylogger]]
* [[DentropyCloud]]
* [[ENS Indexing]]
* [[ActivityWatch Experiments]]
* [[ActivityPub Utils]] - Work In Progress
* [[Docker VPN Router]]
* [[Python Compressor]]
* ddaemon related projects
	* [[Keybase Binding]]
	* [[Discord Binding]]
	* [[ddaemon-webapp]]
* Projects while learning to code
	* [dentropy/PythonicXsAndOs: Play X's and O's on the command line](https://github.com/dentropy/PythonicXsAndOs)
	* [dentropy/roomNavigator](https://github.com/dentropy/roomNavigator)

## Scatter Brained and Projects in Progress

* [[Question Engine]]
* [[KMS Analysis]]
* [[dentropydaemon-wiki/Projects/Robotics]]
* [[ActivityPub Utils]]
* [[obsidian-publish + hugo]]
* [[Let's Learn Web Scraping]]