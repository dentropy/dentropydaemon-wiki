---
share: true
uuid: 10708552-def9-4391-9126-8a4f53cb5e00
---
* [[Learn to Code]]
* [[Homalab and SysAdmin Skills|Homalab and SysAdmin Skills]]
* [[Devops Skills]]
* [[DataScientist Skills]]
* [[Robotics Skills|Robotics Skills]]
* [[Hacking Skills]]
* [[Developer]]
	* [[Frontend Skills|Frontend Skills]]
	* [[Backend Skills]]
* [[Troubleshooting]]
* [[MOOCs]]
* [[Certs]]

